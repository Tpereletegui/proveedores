import React, { createContext, useState, useMemo, useContext } from 'react';

const AuthContext = createContext();

export const useAuthContext = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
  /* const [user, setUser] = useState();

  const value = useMemo(function () {
    return { user, setUser }
  }, [user, setUser])

  return (
    <AuthContext.Provider value={value}>
      {children}
    </AuthContext.Provider>
  ) */
}

export default AuthContext;