import styled from 'styled-components'

export const FormWrapper = styled.form`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;
	width: 82%;
	margin-top: 40px;
	padding:0 1%;
	padding-top: 1%;
	height:55%;
`
export const Inputs = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-end;
	justify-content: flex-start;
	width: 100%;
	padding:0 1%;
	padding-top: 1%;
	overflow-y:auto;
	height:100%;
`

export const Label = styled.label`
	width: 100%;
	font-weight:600;
	margin-bottom:16px;
`

export const Small = styled.small`
	color:red;
	width: 100%;
    position: relative;
	bottom: 18px;
`

export const Input = styled.input`
	font-size: 14px;
	border: none;
	outline: none;
	width: 100%;
	padding: 8px;
	border-radius: 10px;
`
export const Option = styled.option`
	font-size: 14px;
	border: none;
	outline: none;
	width: 100%;
	padding: 8px;
`
export const Select = styled.select`
	width: 100%;
	padding: 8px;
	outline: none;
	border-radius:8px;
	background:#fff;
	margin-bottom: 20px;
	height: 40px;
`

export const InputWrapper = styled.div`
	background: #fff;
	width: 100%;
	height: 40px;
	margin-bottom: 20px;
	border-radius: 10px;
	box-shadow: 0 0px 2px rgba(0, 0, 0, 0.8);
	display: flex;
	justify-content: center;
	align-items: center;
`

export const Recovery = styled.a`
	color: #4182bd;
	font-size: 14px;
`

export const Icon = styled.img`
	width: 24px;
	height: 24px;
	margin: 10px;
`

export const SubmitBtn = styled.button`
	margin-top: 40px;
	border: none;
	font-size: 14px;
	background: #476ade;
	width: 100%;
	height: 40px;
	color: #fff;
	border-radius: 20px;
`

export const ExternalButtons = styled.div`
	display: flex;
	justify-content: space-between;
	width: 80%;
	margin-top: 20px;
`

export const ExternalLoginBtn = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	height: 40px;
	width: 48%;
	border: 1px solid #000;
	border-radius: 40px;
`

export const ExternalLogo = styled.img`
	margin-right: 10px;
	width: 24px;
`

export const ExternalText = styled.p`
	font-size: 14px;
	color: #000;
`
