import styled from 'styled-components'

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
`

export const Title = styled.h1`
	font-size: 24px;
	font-weight: 700;
	color: #343434;
	margin-bottom: 0;
`

export const Subtitle = styled.p`
	font-size: 14px;
	color: #828282;
`
