import { Subtitle, Title, Wrapper } from './styles';

const Info = ({ title, subtitle }) => {
  return (
    <Wrapper>
      <Title>{title}</Title>
      <Subtitle>{subtitle}</Subtitle>
    </Wrapper>
  )
}

export default Info;