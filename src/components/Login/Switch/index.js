import React from 'react'
import { Button, Text, Wrapper } from './styles'

const Switch = ({ mode }) => {
  return (
    <Wrapper>
      {mode === 'login' ? <Text>No tenés cuenta aún?</Text> : <Text>Ya tenés una cuenta?</Text>}
      {mode === 'login' ? <Button style={{cursor: 'pointer'}} onClick={()=>{ window.location = '/signup' }}>Registrarse</Button> : <Button onClick={()=>{ window.location = '/login' }}>Ingresar</Button>}
    </Wrapper>
  )
}

export default Switch;