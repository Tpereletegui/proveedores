import styled from "styled-components";

export const Wrapper = styled.div`
  margin-top: 40px;
  width: 80%;
`

export const Text = styled.p`
  font-size: 14px;
  color: #828282;
  width: 100%;
  text-align: center;
`

export const Button = styled.button`
  font-size: 14px;
  color: #4182BD;
  border-radius: 40px;
  border: 2px solid #4182BD;
  height: 40px;
  width: 100%;
`