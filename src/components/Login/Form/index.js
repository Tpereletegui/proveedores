import React, { useState, useEffect,useContext } from 'react';
import { FormWrapper, Small, Input, Recovery, Icon, InputWrapper, SubmitBtn, ExternalLoginBtn, ExternalLogo, ExternalText, ExternalButtons } from './styles';
import { useForm } from "react-hook-form";
import Switch from '../../common/switchSeller';
import userLogin from '../../../controllers/user/login';
import userSignup from '../../../controllers/user/signup';
import { Router, useRouter } from 'next/router';
import jwt from 'jsonwebtoken'
import { useAuthContext } from '../../../context/AuthContext'


const Form = ({ mode }) => {
  let router = useRouter();
  const { setUser, user } = useAuthContext()
  const [isToggledUserType, setToggledUserType] = useState(false);
  const [userType, setUserType] = useState("Proveedor")
  const { register, handleSubmit, watch, formState: { errors } } = useForm({mode:"all"});

  const onSubmit = async (data) => {
    let inputs = {
      ...data,
      type: userType
    }
    if (mode === "login") {
      const response = await userLogin(inputs);
      if (response) {
        localStorage.setItem('IdToken', response);
        const decoded = jwt.decode(response);
        console.log('decoded', decoded);
        setUser(decoded);
        console.log(decoded);
        localStorage.setItem('loggedUser', JSON.stringify(decoded))
        router.replace('/');
      }
    } else {
      const signupresponse = await userSignup(inputs);
      console.log(signupresponse)
      if(signupresponse){
        const { email, password } = inputs;
        const response = await userLogin({email, password});
        console.log(response)
        if (response) {
          localStorage.setItem('IdToken', response);
          const decoded = jwt.decode(response);
          console.log('decoded', decoded);
          setUser(decoded);
          console.log(decoded);
          localStorage.setItem('loggedUser', JSON.stringify(decoded))
          if (userType === "Proveedor") {
            window.location = "/sellerSignup"
          }
          else if (userType === "Comprador") {
            window.location = "/"
          }
        }
      }
    }
  }
  function onToggleUserType() {
    setToggledUserType(!isToggledUserType);
    if(isToggledUserType === true);
    setUserType("Proveedor");
    if(isToggledUserType ===false){
      setUserType("Comprador");
    }
  }
  return (
    <>
      <FormWrapper onSubmit={handleSubmit(onSubmit)}>
        {mode === 'signup' && <>
          <InputWrapper>
            <Icon src="/img/nameIcon.png" />
            <Input placeholder="Nombre" name="username"
              {...register("username",
                {
                  required: {
                    value: true,
                    message: "Ingresar un nombre de usuario es obligatorio"
                  },
                  minLength: {
                    value: 2,
                    message: "El nombre debe tener al menos 2 caracteres"
                  },
                  maxLength: {
                    value: 60,
                    message: "El nombre no puede superar los 60 caracteres"
                  }
                })
              }
            />
          </InputWrapper>
          <Small>
            {errors?.username?.message}
          </Small>
          <InputWrapper >
            <Icon src="/img/phoneIcon.svg" />
            <Input placeholder="Phone" name="phone" {...register("phone", {
              required: {
                value: true,
                message: "El numero de teléfono es obligatorio"
              },
              pattern: {
                value: /^[0-9\s]*$/,
                message: "El telefono ingresado no cumple con un formato valido"
              }
            })} />
          </InputWrapper>
          <Small>
            {errors?.phone?.message}
          </Small>


        </>}
        <InputWrapper  >
          <Icon src="/img/emailIcon.png" />
          <Input name="email" type='text' placeholder="Email" {...register("email", {
            required: {
              value: true,
              message: "Ingresar un email es obligatorio"
            },
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
              message: "Ingrese un email valido"
            },
          }
          )}
          />
        </InputWrapper>
        <Small>
          {errors?.email?.message}
        </Small>

        <InputWrapper>
          <Icon src="/img/passwordIcon.png" />
          <Input placeholder="Contraseña" name="password" type="password" {...register("password",{
            required:{
            value:true,
            message:"Ingresar una contraseña es obligatorio"
          },
          // pattern:{
          //   value:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
          //   message:"La contaseña debe tener al menos 8 caracteres, 1 mayúscula, 1 minúscula y un número"
          // }
          }
          )} />

        </InputWrapper>
        <Small>
          {errors?.password?.message}
        </Small>
        {
          mode === 'signup' && <Switch isToggled={isToggledUserType} onToggle={() => onToggleUserType()} on={'Proveedor'} off={'Comprador'} />
        }
        {mode === 'login' && <Recovery href="/recovery">Olvidaste tu contraseña?</Recovery>}
        <SubmitBtn type="submit">{mode === 'login' ? 'Ingresar' : 'Registrarse'}</SubmitBtn>
      </FormWrapper>


      <ExternalButtons>
        <ExternalLoginBtn>
          <ExternalLogo src="/img/googleLogo.png" />
          <ExternalText>Google</ExternalText>
        </ExternalLoginBtn>
        <ExternalLoginBtn>
          <ExternalLogo src="/img/facebookLogo.png" />
          <ExternalText>Facebook</ExternalText>
        </ExternalLoginBtn>
      </ExternalButtons>
    </>
  )
}

export default Form;