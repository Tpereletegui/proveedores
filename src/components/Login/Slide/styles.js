import styled from 'styled-components'

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`

export const SlideItem = styled.img`
	width: 200px;
	height: 150px;
	z-index: 2;
	position: absolute;
	top: calc(50% - 50px);
`

export const SlideBG = styled.img`
	position: absolute;
	width: 200px;
	height: 200px;
	top: calc(50% - 75px);
`

export const SlideTitle = styled.h2`
	margin-top: 35vh;
	font-size: 24px;
	font-weight: 700;
	color: #fff;
	text-align: center;
	margin-bottom: 0;
	z-index: 3;
`

export const SlideSubtitle = styled.p`
	font-size: 14px;
	font-weight: 400;
	color: #fff;
	text-align: center;
	z-index: 3;
`
