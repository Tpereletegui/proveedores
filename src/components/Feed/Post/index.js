import { Container, PictureContainer, Picture, Text, InfoContainer, ProfileInfoContainer, ProfilePicture, Price, Msg, LocationInfoContainer, Rating, Star, Location, Marker, Time } from './styles';

const Post = ({ imgUrl1, imgUrl2, text, profileUrl, profileImgUrl, price, rating, location, time }) => {
  return (
    <Container>

      <PictureContainer>
        <Picture src={imgUrl1}/>
        <Picture src={imgUrl2}/>
      </PictureContainer>

      <Text>{text}</Text>

      <InfoContainer>

        <ProfileInfoContainer>
          <ProfilePicture href={profileUrl} target="_blank">
            <img src={profileImgUrl} alt="Profile Picture"/>
          </ProfilePicture>
          <Price>AR$ {price}</Price>
          <Msg src="img/msg.png" alt="MSG"/>
        </ProfileInfoContainer>

        <LocationInfoContainer>
          <Rating>{rating} <Star src="img/star.png"/></Rating>
          <Location><Marker src="img/location.png"/> {location}</Location>
          <Time>Hace {time >= 60 ? `${Math.floor(time/60)} Horas` : `${time} Mins`}</Time>
        </LocationInfoContainer>

      </InfoContainer>
      
    </Container>
  )
}

export default Post
