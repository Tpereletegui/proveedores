import styled from "styled-components";

export const Container = styled.div`
  max-width: 400px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  border-radius: 15px;
  box-shadow: 0 0 4px rgba(0, 0, 0, .5);
  padding: 20px;
  overflow: hidden;
  margin: 20px 15px 10px 15px;
`

export const PictureContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`

export const Picture = styled.img`
  width: 176px;
  height: 132px;
  max-width: 48%;
`

export const Text = styled.p`
  width: 100%;
  font-size: 17px;
  text-align: center;
  color: #000;
`

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`

export const ProfileInfoContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  position: relative;
`

export const ProfilePicture = styled.a`

`

export const Price = styled.p`
  margin-left: 10px;
  color: #4182BD;
`

export const Msg = styled.img`
  position: absolute;
  right: 0;
  padding: 5px 8% 5px 8%;
  border-radius: 12px;
  background: #E8E8E8;
`

export const LocationInfoContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`

export const Rating = styled.p`

`

export const Star = styled.img`

`

export const Location = styled.p`

`

export const Marker = styled.img`

`

export const Time = styled.p`

`