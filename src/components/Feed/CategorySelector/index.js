import React from 'react';
import useEmblaCarousel from 'embla-carousel-react';
import { Slide } from './styles';
import styles from '../../../styles/Embla.module.css';

const CategorySelector = () => {
  const [emblaRef] = useEmblaCarousel()

  return (
    <div className={styles.embla} ref={emblaRef}>
      <div className={styles.embla__container}>
        <Slide>Tecnologia</Slide>
        <Slide>Ropa</Slide>
        <Slide>Comida</Slide>
        <Slide>Tecnologia</Slide>
        <Slide>Ropa</Slide>
        <Slide>Comida</Slide>
        <Slide>Tecnologia</Slide>
        <Slide>Ropa</Slide>
        <Slide>Comida</Slide>
      </div>
    </div>
  )
}

export default CategorySelector;