import styled from "styled-components"

export const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	height: 350px;
	width: 800px;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
	overflow-y: scroll;
	background: white;
	@media (max-width: 823px) {
		width: 100%;
	}
	@media (max-width: 400px) {
		width: 400px;
		overflow-x: auto;
	}
`

export const User = styled.div`
	display: flex;
	justify-content: space-evenly;
	align-items: center;
	flex-direction: row;
	width: 100%;
	margin-top: 20px;
	height: 40px;
`

export const Username = styled.p`
	width: 30%;
`
export const Email = styled.p`
	width: 50%;
`
export const Edit = styled.button`
	width: 10%;
	background-color: transparent;
	border: none;
	cursor: pointer;
	border-radius: 6px;
	img {
		transition: 0.3s;
	}
	img {
		&:hover {
			box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
			padding: 4px;
		}
	}
`

export const Users = styled.div`
	width: 100%;
	height: 100%;
`

export const ModalOpen = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: rgba(220, 220, 220, 0.7);
`

export const ButtonSaveModal = styled.button`
	background: #ffffff;
	border: 1px solid #476ade;
	box-sizing: border-box;
	border-radius: 8px;
	width: 45%;
	height: 30px;
	margin-top: 30px;
	color: #476ade;
	cursor: pointer;
	border-radius: 8px;
	transition: 0.6s;
	&:hover {
		background-color: #476ade;
		color: #ffffff;
		transform: scale(1.1);
	}
`

export const Span = styled.span`
	font-weight: bold;
	color: black;
`

export const ModalParagraph = styled.p`
	font-weight: normal;
	color: #414245;
`

export const ModalContainer = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-evenly;
	flex-direction: column;
	height: 330px;
`

export const BlockBtn = styled.button`
	background: none;
	border: none;
	margin: 0 6px;
	transition: 0.6s;

	&:hover {
		transform: scale(1.1);
	}
`
