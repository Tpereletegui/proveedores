import { useEffect, useState } from "react";
import axios from "axios";
import {
  ButtonSaveModal,
  Edit,
  Email,
  ModalOpen,
  User,
  Username,
  Users,
  Wrapper,
  ModalParagraph,
  Span,
  ModalContainer,
  BlockBtn
} from "./styles.jsx";
import Modal from "../../components/common/Modal/index";

const users = () => {
 /*  const [users, setUsers] = useState([]);
  const [user, setUser] = useState({});
  console.log("useer", user);
  const [open, setOpen] = useState(false);
  useEffect(() => {
    async function getUsers() {
      try {
        let data = await axios.get(`${process.env.apiURI}/user`);
        console.log("users data", data);
        setUsers(data.data);
      } catch (error) {
        console.log(error);
      }
    }
    getUsers();
  }, []); */

  function openModal(user) {
    setOpen(true);
    setUser(user);
  }

  function closeModal() {
    setOpen(false);
    setUser({});
  }
  return (
    <>
      {open && <ModalOpen></ModalOpen>}
      <Wrapper>

        <Users>
          <User style={{ marginBottom: "40px" }}>
            <Username style={{ fontWeight: "bold" }}>USUARIO</Username>
            <Email style={{ fontWeight: "bold" }}>EMAIL</Email>
            <Username style={{ fontWeight: "bold", width: "10%" }}>
              DETALLES
            </Username>
          </User>
          {users.length &&
            users.map((user, index) => (
              <User key={index}>
                <Username>{user.username}</Username>
                <Email>{user.email}</Email>
                <Edit onClick={() => openModal(user)}>
                  <img src={"/img/details.png"} />
                </Edit>
              </User>
            ))}
        </Users>
      </Wrapper>
      {open && (
        <Modal>
          <ModalContainer>
            <img
              style={{ width: "60px", height: "60px", borderRadius: "50%" }}
              src={user.image}
            />
            <ModalParagraph>{user.username}</ModalParagraph>
            <ModalParagraph style={{ fontWeight: 'bold' }}>{user.code}</ModalParagraph>
            <ModalParagraph>
              <Span>Email: </Span> {user.email}
            </ModalParagraph>
            <ModalParagraph>
              <Span>Telefono: </Span>
              {user.phone}
            </ModalParagraph>
            <ModalParagraph>
              <Span>ID: </Span>
              {user._id}
            </ModalParagraph>
            <ModalParagraph>
              <Span>Tipo:  </Span>
              {user.code && user.code[0] === 'P' ? 'Vendedor' : 'Comprador'}
            </ModalParagraph>
            <div>
              <BlockBtn>
                <img src={"/img/warning.png"} />
              </BlockBtn>
              <BlockBtn>
                <img src={"/img/block.png"} />
              </BlockBtn>
            </div>
            <ButtonSaveModal onClick={() => closeModal()}>
              Cerrar
            </ButtonSaveModal>
          </ModalContainer>
        </Modal>
      )}
    </>
  );
};

export default users;
