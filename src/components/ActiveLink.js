import { useRouter } from 'next/router';
import Link from 'next/link';
import styled from 'styled-components';

const Enlace = styled.a` 
    border-bottom: ${({ isHere }) => isHere ? '1px solid #000000' : 'none'};
    color: #000;
	transition: all 300ms ease-out;
    cursor: pointer;
    height: 100%;
	&:hover {
		transform: scale(1.1);
	}
`

function ActiveLink({ href, children }) {
    const router = useRouter();

    const isHere = router.asPath === href;

    const handleClick = (e) => {
        e.preventDefault();
        router.push(href);
    };

    return (
        <Link href={href}>
            <Enlace isHere={isHere} onClick={handleClick}>
                {children}
            </Enlace>
        </Link>

    );
};
export default ActiveLink;