import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { io } from 'socket.io-client'
import {
	Date,
	Wrapper,
	Box1,
	Box2,
	Box3,
	NameCompany,
	Category,
	BoxContainer2,
	BoxContainer3,
	Info,
	Main,
	Subtitle,
	BoxContent,
	BoxColumn,
	BoxColumnTitle,
	BoxContainer,
	BoxColumnText,
	Title,
	ModalOpen,
	Footer,
	Products,
	Buttons,
	Options,
	OptionBox,
	Input,
	OptionText,
	OptionTitle,
	OptionP,
	InputContra
} from './styles'

const PostOffer = ({ datos, sellerId, socket, buyerId, categories }) => {
	const [seller, setSeller] = useState({})
	const [names, setNames] = useState([]);
	const [token, setToken] = useState(null)
	const [sellerData, setSellerData] = useState(undefined);
	const [reload, setReload] = useState(false)
	const [active, setActive] = useState('')
	const [selected, setSelected] = useState({
		products: [],
	})
	const [contraoferta, setcontraoferta] = useState(false);

	const [user, setUser] = useState(null)


	function onClick() {
		if (active !== 'active') setActive('active')
		else setActive('')
	}

	useEffect(() => {
		axios.get(`http://localhost:3000/api/user/${buyerId}`).then(res => setUser(res.data)).catch(err => console.log(err))
	}, [])

	console.log('USER', user)

	useEffect(() => {
		const a = async () => {
			if (!sellerId) { setReload(!reload) }
			try {
				const response = await axios.get(`http://localhost:3000/api/seller/${sellerId}`);
				setSeller(response.data.data)
				console.log('FIL', response.data.data)
				if (response.data.data && response.data.data.user_id) {
					const response2 = await axios.get(`http://localhost:3000/api/user/${response.data.data.user_id}`);
					setSellerData(response2?.data?.data);
				}
			}
			catch (error) {
				console.log(error)
			}

		}
		a();
	}, [reload])

	useEffect(() => {
		const getCategory = () => {
			if (!categories) { setReload(!reload) }
			categories?.map(async (cat) => {
				const response = await axios.get(`http://localhost:3000/api/category/${cat}`)
				setNames(prev => [...prev, response.data?.data?.name])
			})
		}
		getCategory()
	}, [reload])



	useEffect(() => {
		setToken(localStorage.getItem('IdToken'))
		console.log('TOKEN', localStorage.getItem('IdToken'))
	}, [])


	/*
	useEffect(() => {
		if (token) {

			const newSocket = io('http://localhost:3001', {
				auth: { token },
				autoConnect: true,
			})
			setSocket(newSocket)
			return () => newSocket.close()
		}
	}, [token])
	*/

	const handleSelect = (e, product) => {
		let filtered = selected.products.filter(x => x.id === e.target.value)
		if (filtered.length)
			setSelected({
				...selected,
				products: selected.products.filter(x => x.id !== e.target.value),
			})
		else
			setSelected({
				...selected,
				products: [...selected.products, product],
			})
	}

	const createChat = () => {
		socket.emit('private message', {
			receiver: { id: buyerId, name: user.data.username },

			text: 'Hola. Gracias por aceptar mi producto!',
		})
		window.location = '/chat'
	}

	return (
		<>

			<Wrapper>
				<Title>
					<NameCompany>
						{seller && (seller?.fantasyName || seller?.businessName)}
						{names.map((category, i) => (
							<Category key={i}>{category}</Category>
						))}

					</NameCompany>
					<Date>Valido hasta {datos.validation?.slice(0, 10)}</Date>
				</Title>
				<Main>
					<Info>
						<BoxContainer>
							<Subtitle>Cotización</Subtitle>
							<Box1>
								<BoxContent>
									<BoxColumn>
										<BoxColumnTitle>
											Tipo de PCD
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.pcd}
										</BoxColumnText>
										<BoxColumnTitle>
											Terminos y Condiciones
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.conditions}
										</BoxColumnText>
									</BoxColumn>
									<BoxColumn>
										<BoxColumnTitle>
											Forma de Pago
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.payment}
										</BoxColumnText>
										<BoxColumnTitle>
											Observaciones
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.observations1}
										</BoxColumnText>
									</BoxColumn>
								</BoxContent>
							</Box1>
						</BoxContainer>
						<BoxContainer2>
							<Subtitle>Propuesta</Subtitle>
							<Box2>
								<BoxContent>
									<BoxColumn>
										<BoxColumnTitle>
											Facturación mínima
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.billing}
										</BoxColumnText>
										<BoxColumnTitle>
											Validez de la propuesta
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.validation?.split("T")[0]}
										</BoxColumnText>
										<BoxColumnTitle>
											Incluye flete
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.includesTransportation ? 'Si' : 'No'}
										</BoxColumnText>
									</BoxColumn>
									<BoxColumn>
										<BoxColumnTitle>
											Plazo de entrega
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.deliveryDate}
										</BoxColumnText>
										<BoxColumnTitle>
											Condiciones de pago
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.condition}
										</BoxColumnText>
										<BoxColumnTitle>
											Observaciones
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.observations2}
										</BoxColumnText>
									</BoxColumn>
								</BoxContent>
							</Box2>
						</BoxContainer2>
						<BoxContainer3>
							<Subtitle>Contacto</Subtitle>
							<Box3>
								<BoxContent>
									<BoxColumn style={{ width: '90%' }}>
										<BoxColumnTitle>
											Dirección de entrega
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.deliveryAdress}
										</BoxColumnText>
										<BoxColumnTitle>
											Dirección de facturación
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.billingAdress}
										</BoxColumnText>
										<BoxColumnTitle>
											Contacto del responsable
										</BoxColumnTitle>
										<BoxColumnText>
											{datos.contact}
										</BoxColumnText>
									</BoxColumn>
								</BoxContent>
							</Box3>
						</BoxContainer3>
					</Info>
				</Main>
				<Footer open={active}>
					<Products onClick={() => onClick()}>
						Ver Productos
						<button
							onClick={() => onClick()}
							style={{
								backgroundColor: 'transparent',
								border: 'none',
								cursor: 'pointer',
							}}
						>
							<img
								src={
									active.length
										? '/img/arrowUp.png'
										: '/img/arrowDown.png'
								}
								style={{ marginLeft: '6px' }}
							></img>
						</button>
					</Products>
					<Buttons onClick={() => createChat()}>Contactar comprador </Buttons>
				</Footer>
				<Options active={active}>
					{datos.variants && !contraoferta
						?
						datos.variants.map((va) => (

							<OptionBox key={va._id}>
								<Input
									value={datos.variants._id}
									onClick={e => handleSelect(e, va)}
									type='checkbox'
								></Input>
								<OptionText>
									<OptionTitle>Tipo:</OptionTitle>
									<OptionP style={{ marginLeft: '7px' }}>
										{va.variantType}
									</OptionP>
								</OptionText>
								<OptionText>
									<OptionTitle>Cantidad:</OptionTitle>
									<OptionP style={{ marginLeft: '7px' }}>
										{va.quantity} {va.unit}
									</OptionP>
									{/* 	<button
									style={{
										backgroundColor: 'transparent',
										border: 'none',
										cursor: 'pointer',
									}}
								>
									<img src='img/arrowDown.png'></img>
								</button> */}
								</OptionText>
								<OptionText>
									<OptionTitle>Precio: </OptionTitle>
									<OptionP style={{ marginLeft: '2px' }}>
										{' '}
										$ {va.price}
									</OptionP>
								</OptionText>
								<OptionText>
									<OptionTitle>Descripción: </OptionTitle>
									<OptionP> {va.description}</OptionP>
								</OptionText>
								<OptionText>
									<OptionTitle>Marca: </OptionTitle>
									<OptionP style={{ marginLeft: '0px' }}>
										{' '}
										{va.brand}
									</OptionP>
								</OptionText>
								<OptionText>
									<OptionTitle>Color: </OptionTitle>
									<OptionP style={{ marginLeft: '0px' }}>
										{' '}
										{va.color}
									</OptionP>
								</OptionText>
								<button style={{ background: 'none', border: 'none', cursor: 'pointer' }} onClick={(e) => onModal(e, va.image)}><img src={'/img/ModalImage.png'} alt='' /></button>
							</OptionBox>
						))



						: contraoferta ?
							datos.variants.map((va) => (

								<OptionBox key={va._id}>
									<Input
										value={datos.variants._id}
										onClick={e => handleSelectContra(e, va)}
										type='checkbox'
									></Input>
									<OptionText>
										<OptionTitle>Tipo:</OptionTitle>
										<OptionP style={{ marginLeft: '7px' }}>
											{va.variantType}
										</OptionP>
									</OptionText>
									<OptionText>
										<OptionTitle>Cantidad:</OptionTitle>
										<InputContra value={''} placeholder={va.quantity + ' /u'} disabled={!selectedcontra.products.length} style={{ marginLeft: '7px' }}>

										</InputContra>
										{/* 	<button
									style={{
										backgroundColor: 'transparent',
										border: 'none',
										cursor: 'pointer',
									}}
								>
									<img src='img/arrowDown.png'></img>
								</button> */}
									</OptionText>
									<OptionText>
										<OptionTitle>Precio: </OptionTitle>
										<InputContra disabled={!selectedcontra.products.length} placeholder={'$' + va.price} style={{ marginLeft: '2px' }}>
										</InputContra>
									</OptionText>
									<OptionText>
										<OptionTitle>Descripción: </OptionTitle>
										<OptionP> {va.description}</OptionP>
									</OptionText>
									<OptionText>
										<OptionTitle>Marca: </OptionTitle>
										<OptionP style={{ marginLeft: '0px' }}>
											{' '}
											{va.brand}
										</OptionP>
									</OptionText>
									<OptionText>
										<OptionTitle>Color: </OptionTitle>
										<OptionP style={{ marginLeft: '0px' }}>
											{' '}
											{va.color}
										</OptionP>
									</OptionText>
									<button style={{ background: 'none', border: 'none', cursor: 'pointer' }} onClick={(e) => onModal(e, va.image)}><img src={'/img/ModalImage.png'} alt='' /></button>
								</OptionBox>
							))
							: null
					}
				</Options>

			</Wrapper>

		</>
	)
}
export default PostOffer
