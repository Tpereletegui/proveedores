import styled from "styled-components"

export const Container = styled.div`
	display: flex;
	width: 100%;
	height: calc(100vh - 77px);
	scroll-snap-align: start;
	justify-content: flex-start;
	flex-direction: column;
	align-items: center;
	position: relative;
	padding: 1% 0;
`
export const Wrapper = styled.div`
	width: min(100%, 1200px);
	padding-bottom: 15px;
	height: calc(100vh - 77px);
	background-color: white;
	margin: auto;
	margin-bottom: 0;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	@media (max-width: 1024px) {
		overflow-y: scroll;
	}
`

export const Main = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	width: 100%;
	height: 60%;
	@media (max-width: 978px) {
		height: 50%;
	}
`

export const SubtitleWrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: row;
	width: 100%;
	margin: auto;
	margin-left: 12%;
`

export const Subtitle = styled.p`
	font-style: normal;
	font-weight: bold;
	font-size: 22px;
	line-height: 22px;
	color: #476ade;
`

export const Info = styled.div`
	display: flex;
	justify-content: center;
	align-items: flex-start;
	flex-direction: row;
	width: 100%;
	height: fit-content;
	@media (max-width: 978px) {
		flex-direction: column;
		justify-content: flex-start;
		max-height: 100%;
		overflow-y: auto;
		width: 95%;
		border-radius: 8px;
		box-shadow: 0 0 4px rgba(0, 0, 0, 0.4);
		padding: 10px;
	}
`

export const BoxContainer = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	width: 33%;
	height: 100%;
	@media (max-width: 978px) {
		width: 100%;
		height: 400px;
	}
`
export const BoxContainer2 = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	width: 33%;
	height: 100%;
	@media (max-width: 978px) {
		width: 100%;
		height: 300px;
	}
`
export const BoxContainer3 = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	width: 33%;
	height: 100%;
	@media (max-width: 978px) {
		width: 100%;
		height: 300px;
	}
`

export const Box1 = styled.div`
	min-height: 350px;
	max-height: max-content;
	padding: 30px;
	@media (max-width: 1024px) {
		height: max-content;
		min-height: auto;
		margin-bottom: 24px;
	}
`
export const Box2 = styled.div`
	max-height: max-content;
	width: 100%;
	border-right: 2px solid black;
	border-left: 2px solid black;
	padding: 30px;
	min-height: 350px;
	@media (max-width: 1024px) {
		height: max-content;
		min-height: auto;
		margin-bottom: 40px;
		border: none;
	}
`
export const Box3 = styled.div`
	min-height: 350px;
	max-height: max-content;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	flex-direction: column;
	padding: 30px;
`

export const BoxContent = styled.div`
	display: flex;
	justify-content: space-evenly;
	align-items: flex-start;
	flex-direction: row;
	width: 100%;
	height: 100%;
	@media (max-width: 1024px) {
		margin: auto;
		align-items: center;
		justify-content: center;
	}
`
export const BoxColumnContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	justify-content: flex-start;
	text-align: center;
`
export const BoxColumn = styled.div`
	display: flex;
	justify-content: space-start;
	align-items: center;
	flex-direction: column;
	text-align: left;
	height: 100%;
	width: 100%;
	padding: 0 8px;
	@media (max-width: 1024px) {
		margin: auto;
	}
`
export const BoxColumnTitle = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 18px;
	line-height: 25px;
	margin-bottom: 15px;
	width: 100%;
	color: black;
	text-align: left;
`
export const BoxColumnText = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 17px;
	margin-bottom: 18px;
	width: 100%;
	text-align: left;

	color: rgba(0, 0, 0, 0.6);
`
export const Date = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 16px;
	color: rgba(0, 0, 0, 0.6);
	margin-right: 3%;
	@media (max-width: 1024px) {
		margin-right: 0%;
	}
`

export const Title = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	flex-direction: row;
	margin-bottom: 3%;
	padding: 15px;
	@media (max-width: 1024px) {
		flex-direction: column;
		margin-bottom: 35px;
	}
`

export const NameCompany = styled.p`
	display: flex;
	align-items: center;
	color: black;
	font-size: 20px;
	gap: 12px;
	margin-bottom: 10px;
`

export const Category = styled.span`
	color: white;
	font-size: 20px;
	background-color: #476ade;
	padding: 5px;
	border-radius: 8px;
`

export const Footer = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
	flex-direction: row;
	position: absolute;
	background-color: ${props => (props.open ? "white" : "transparent")};
	bottom: ${props => (props.open ? "180px" : "25px")};
	padding-top: 0px;
	transition: 0.7s;
	transition-timing-function: ease-in-out;
	@media (max-width: 1024px) {
		flex-direction: column-reverse;
		margin-top: 30px;
		width: 100%;
		display: none;
	}
`
export const ButtonsContainer = styled.div`
	display: flex;
	margin-top: 15px;
	width: 100%;
	justify-content: flex-start;
	padding-left: 20px;
	@media (max-width: 1024px) {
		display: none;
	}
`
export const ButtonsContainerMobile = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	background-color: white;
`
export const ButtonsSubContainer = styled.div`
	display: flex;
	width: 100%;
	justify-content: space-around;
`

export const Buttons = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 14px;
	color: #333333;
	background-color: transparent;
	border: 1px solid #476ade;
	box-sizing: border-box;
	border-radius: 8px;
	width: max-content;
	height: 28px;
	margin-right: 22px;
	text-align: center;
	cursor: pointer;
	padding: 5px;
	@media (max-width: 1024px) {
		margin: auto;
		margin-bottom: 15px;
		width: 168px;
		margin-top: 10px;
		font-size: 13px;
	}
`

export const Products = styled.p`
	width: 145px;
	height: 29px;
	font-style: normal;
	font-weight: 500;
	font-size: 14px;
	color: #333333;
	text-align: center;
	padding: 5px;
	transition: 0.7s;
	@media (max-width: 1024px) {
		margin: auto;
		/* ${props => (props.active ? ";bottom:375px;left:0px;" : "bottom:0px;")}; */
	}
`

export const Options = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
	flex-direction: column;
	position: absolute;
	bottom: 0px;
	width: min(100%, 1200px);
	overflow-y: auto;
	height: ${props => (props.active ? "180px" : "0px")};
	padding: ${props => (props.active ? "12px 0" : "0px")};
	background-color: ${props => (props.active ? "white" : "transparent")};
	transition: 0.7s;
	transition-timing-function: ease-in-out;
	@media (max-width: 1024px) {
		height: ${props => (props.active ? "89%" : "100px")};
		width: 100%;
		background-color: ${props => (props.active ? "white" : "none")};
	}
`
export const OptionBox = styled.div`
	display: flex;
	padding: 10px;
	align-items: center;
	justify-content: space-evenly;
	flex-direction: row;
	background: #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
	width: 97%;
	height: 70px;
	margin: 4px 0;

	@media (max-width: 1024px) {
		flex-wrap: nowrap;
		justify-content: center;
		align-items: flex-start;
		flex-direction: column;
		height: max-content;
		width: 90%;
		margin-bottom: 10px;
		margin-top: 10px;
		padding: 50px;
	}
	@media (max-width: 412px) {
		flex-wrap: nowrap;
		justify-content: center;
		align-items: flex-start;
		flex-direction: column;
		height: max-content;
		width: 90%;
		margin-bottom: 10px;
		margin-top: 10px;
		padding: 10px;
	}
`

export const OptionText = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
	flex-direction: row;
	margin-right: 20px;
	width: 15%;

	@media (max-width: 1024px) {
		margin-bottom: 15px;
		margin-left: 20%;
		&:nth-child(2) {
			width: 50%;
		}
		&:nth-child(5) {
			width: 90%;
		}
		&:nth-child(3) {
			width: 70%;
		}
		&:nth-child(4) {
			width: 70%;
		}
		&:nth-child(6) {
			width: 70%;
		}
		&:nth-child(7) {
			width: 70%;
		}
	}
`
export const OptionP = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 16px;
	width: auto;
	color: rgba(0, 0, 0, 0.6);
	padding: 5px;
	margin: 0px;
`
export const OptionTitle = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 16px;
	line-height: 25px;
	color: black;
	padding: 5px;
`

export const Input = styled.input`
	-webkit-appearance: none;
	appearance: none;
	background: #ffffff;
	border: 1px solid #828282;
	border-radius: 6px;
	width: 16px;
	height: 16px;

	cursor: pointer;
	margin-right: 15px;

	&:checked {
		border: 5px solid #476ade;
	}
	@media (max-width: 1024px) {
		margin-right: 90%;
		margin-bottom: 15px;
	}
`

export const OptionImg = styled.img`
	margin-top: 10px;
	@media (max-width: 1024px) {
		margin-top: 20px;
		margin-left: 45%;
	}
`

export const ModalImageContainer = styled.div`
	max-width: 300px;
	max-height: 600px;
	margin: auto;
`

export const ModalImage = styled.img`
	max-width: 100%;
	max-height: 100%;
`
export const ModalTitle = styled.p`
	font-size: 24px;
	color: #476ade;
	text-align: center;
	margin-bottom: 70px;
`

export const ButtonSaveModal = styled.button`
	background: #ffffff;
	border: 1px solid #476ade;
	box-sizing: border-box;
	border-radius: 8px;
	width: 45%;
	height: 30px;

	color: #476ade;
	cursor: pointer;
	&:hover {
		background-color: ${props => (props.props ? "transparent" : "#476ADE")};
		color: ${props => (props.props ? "#476ADE" : "#FFFFFF")};
	}
`
export const ModalButtonsContainer = styled.div`
	width: 80%;
	display: flex;
	align-items: center;

	justify-content: center;
	margin: auto;
`
export const ModalOpen = styled.div`
	position: absolute;
	width: 100%;
	height: 100%;
	background-color: rgba(220, 220, 220, 0.7);
`

export const InputContra = styled.input`
	font-style: normal;
	font-weight: 500;
	font-size: 16px;
	text-align: center;
	width: auto;
	color: rgba(0, 0, 0, 0.6);
	width: 90px;
	border: none;
	background-color: transparent;
	border-bottom: 1px solid #476ade;
	margin: 0px;
`
export const selectAll = styled.button`
	background: #ffffff;
	border: 1px solid #476ade;
	box-sizing: border-box;
	border-radius: 8px;
	width: 45%;
	height: 30px;
	color: #476ade;
	cursor: pointer;
	&:hover {
		background-color: #476ade;
		color: #ffffff;
	}
`
