import React, { useState, useEffect } from "react";
import axios from "axios";
import Modal from "../Modal/index";
import { io } from "socket.io-client";
import Link from "next/link";
import {
  Date,
  Wrapper,
  Box1,
  Box2,
  Box3,
  NameCompany,
  Container,
  OptionP,
  Category,
  OptionText,
  OptionTitle,
  BoxContainer2,
  BoxContainer3,
  Info,
  Main,
  Input,
  OptionBox,
  Options,
  OptionImg,
  Subtitle,
  BoxContent,
  BoxColumn,
  BoxColumnTitle,
  BoxContainer,
  BoxColumnText,
  Title,
  Footer,
  Buttons,
  Products,
  ModalTitle,
  ModalImage,
  ModalImageContainer,
  ButtonSaveModal,
  ModalButtonsContainer,
  ModalOpen,
  InputContra,
	ButtonsContainerMobile,
	ButtonsSubContainer,
  selectAll
} from "./styles";

const Posts = ({ datos, edit, sellerId, categories, socket }) => {
  const [seller, setSeller] = useState({});
  const [names, setNames] = useState([]);
  const [all, setall] = useState(false)
  const [modal, setModal] = useState({
    open: false,
    image: "",
  });
  const [selected, setSelected] = useState({
    products: [],
  });

  // console.log('selected', selected)

  const [active, setActive] = useState("");
  const [sellerData, setSellerData] = useState(undefined);
  const [contraoferta, setcontraoferta] = useState(false);
  const [selectedcontra, setselectedcontra] = useState({
    products: [],
  });

  function useWindowSize() {
    const [size, setSize] = useState([window.innerHeight, window.innerWidth]);
    useEffect(() => {
      const handleResize = () => {
        setSize([window.innerHeight, window.innerWidth])
      };
      window.addEventListener("resize", handleResize)
      return () => {
        window.removeEventListener("resize", handleResize)
      }
    }, [])
    return size;
  }
  const [height, width] = useWindowSize()



  
useEffect(() => {
		const getCategory = () => {
			categories?.map(async (cat) => {
				const response = await axios.get(`${process.env.VERCEL_URL}/api/category/${cat}`)
				setNames(prev => [...prev, response.data?.data?.name])
			})
		}
		getCategory()
	}, [])


  useEffect(() => {
    const a = async () => {
      if(!edit) {
        console.log('entre aqui')
        const response = await axios.get(
          `${process.env.VERCEL_URL}/api/seller/${sellerId}`
        );
        setSeller(response.data.data);
        if (response.data.data && response.data.data.user_id) {
          const response2 = await axios.get(
            `${process.env.VERCEL_URL}/api/user/${response.data.data.user_id}`
          );
          setSellerData(response2?.data?.data);
        }
      }else {
        console.log('no entre')
        return
      }
      }
    a();
  }, []);



  const handleSelect = ( product) => {
    
    handleSelectContraCopy( product);

    let filtered = selected.products.find((x) => x._id === product._id);
    console.log('filtered', filtered)
    if (filtered)
      setSelected({
        products: selected.products.filter((x) => x._id !== product._id),
      });
    else
      setSelected({
        products: [...selected.products, product],
      });
  };

  const handleSelectCopy = ( product) => {
    let filtered = selected.products.find((x) => x._id === product._id);

    if (filtered)
      setSelected({
        products: selected.products.filter((x) => x._id !== product._id),
      });
    else
      setSelected({
        products: [...selected.products, product],
      });
  };

  const handleSelectContra = ( product) => {
    handleSelectCopy( product);

    let producto = { ...product };
    let filtered = selectedcontra.products.filter(
      (x) => x._id === producto._id
    );


    if (filtered.length)
      setselectedcontra({
        ...selectedcontra,
        products: selectedcontra.products.filter((x) => x._id !== producto._id),
      });
    else
      setselectedcontra({
        ...selectedcontra,
        products: [...selectedcontra.products, producto],
      });
  };


  const handleSelectContraCopy = ( product) => {
    let producto = { ...product };
    let filtered = selectedcontra.products.filter(
      (x) => x._id === producto._id
    );

    if (filtered.length)
      setselectedcontra({
        ...selectedcontra,
        products: selectedcontra.products.filter((x) => x._id !== producto._id),
      });
    else
      setselectedcontra({
        ...selectedcontra,
        products: [...selectedcontra.products, producto],
      });
  };
  function onClick() {
    if (active !== "active") setActive("active");
    else setActive("");
  }

  function onModal(e, image) {
    e.preventDefault();
    setModal({
      open: true,
      image: !image ? image : "",
    });
  }

  const accept = async () => {
    const userId = JSON.parse(localStorage.getItem('loggedUser'))
    if (userId && selected.products.length && seller._id) {
      const response = await axios.post(`${process.env.VERCEL_URL}/api/offer`, {
        variants: selected.products,
        type: 'Aceptado',
        post_id: datos.id,
        buyer_id: userId.id,
        seller_id: seller._id
      })
    }
  }
  const createChat = () => {
    socket.emit('private message', { receiver: { id: sellerData._id, name: sellerData.username }, text: "Hola me interesa tu producto..." });
    window.location = '/chat';
  }

  //CONTRAOFERTA
  function onValueQuantity(id) {
    let filtered = selectedcontra.products.find((x) => x._id === id);
    let filteredVariants = datos.variants.find((x) => x._id === id);
    return filtered?.quantity ? filtered.quantity : filteredVariants.quantity;
  }
  function onValuePrice(id) {
    let filtered = selectedcontra.products.find((x) => x._id === id);
    let filteredVariants = datos.variants.find((x) => x._id === id);
    return filtered?.price ? filtered.price : filteredVariants.price;
  }

  function onChange(e, id) {
    e.preventDefault();
    let filter = selectedcontra.products.find((x) => x._id === id);
    let name = e.target.name;
    let value = e.target.value.length ? parseInt(e.target.value) : e.target.value;
    filter[name] = value;


    let filtered = selectedcontra.products.filter((x) => x._id !== id);
    setselectedcontra({
      products: [...filtered, filter],
    });
  }


  async function submitContra() {
    let obj = {
      variants: [...selectedcontra.products],
      buyer_id: JSON.parse(localStorage.getItem("loggedUser")).id,
      seller_id: sellerId,
      post_id: datos.id,
      type: 'Contraoferta'
    }
    let response = await axios.post(`${process.env.VERCEL_URL}/api/offer/`, obj);
  }


  function selectAll(){
   if( all === false || selected.products.length < datos.variants.length) {
     setSelected({
       products: datos.variants
     })
     setselectedcontra({
       products: datos.variants
     })
     datos.variants.map(box => {
       let check= document.getElementById(`${box._id}`);
       check.checked = true
     })
     setall(true)
   }else {
    setSelected({
      products: []
    })
    setselectedcontra({
      products: []
    })

    datos.variants.map(box => {
      let check= document.getElementById(`${box._id}`);
      check.checked = false
    })
    setall(false);
   }
  }

  //FIN CONTRAOFERTA

  return (
    <Container>
      {modal.open && <ModalOpen></ModalOpen>}
      <Wrapper>
        <Title>
          <NameCompany>
            {seller && (seller?.fantasyName || seller?.businessName)}
            {names.map((category, i) => (
              <Category key={i}>{category}</Category>
            ))}
          </NameCompany>
          <Date>Valido hasta {datos.validation.slice(0, 10)}</Date>
        </Title>

        <Main>
          <Info>
            <BoxContainer>
              <Subtitle>Cotización</Subtitle>
              <Box1>
                <BoxContent>
                  <BoxColumn>
                    <BoxColumnTitle>Tipo de publicación</BoxColumnTitle>
                    <BoxColumnText>{datos.pcd}</BoxColumnText>
                    <BoxColumnTitle>Observaciones</BoxColumnTitle>
                    <BoxColumnText>{datos.observations1}</BoxColumnText>
                  </BoxColumn>
                  <BoxColumn>
                    <BoxColumnTitle>Forma de Pago</BoxColumnTitle>
                    <BoxColumnText>{datos.payment}</BoxColumnText>
                  </BoxColumn>
                </BoxContent>
              </Box1>
            </BoxContainer>
            <BoxContainer2>
              <Subtitle>Propuesta</Subtitle>
              <Box2>
                <BoxContent>
                  <BoxColumn>
                    <BoxColumnTitle>Monto mínimo</BoxColumnTitle>
                    <BoxColumnText>{datos.billing}</BoxColumnText>
                    {/* <BoxColumnTitle>Validez de la propuesta</BoxColumnTitle>
                    <BoxColumnText>
                      {datos.validation.split("T")[0]}
                    </BoxColumnText> */}
                    <BoxColumnTitle>Incluye flete</BoxColumnTitle>
                    <BoxColumnText>
                      {datos.includesTransportation ? "Si" : "No"}
                    </BoxColumnText>
                  </BoxColumn>
                  <BoxColumn>
                    <BoxColumnTitle>Plazo de entrega</BoxColumnTitle>
                    <BoxColumnText>{datos.deliveryDate}</BoxColumnText>
                    {/* <BoxColumnTitle>Condiciones de pago</BoxColumnTitle>
                    <BoxColumnText>{datos.condition}</BoxColumnText> */}
                    <BoxColumnTitle>Observaciones</BoxColumnTitle>
                    <BoxColumnText>{datos.observations2}</BoxColumnText>
                  </BoxColumn>
                </BoxContent>
              </Box2>
            </BoxContainer2>
            <BoxContainer3>
              <Subtitle>Contacto</Subtitle>
              <Box3>
                <BoxContent>
                  <BoxColumn style={{ width: "90%" }}>
                    <BoxColumnTitle>Dirección de entrega</BoxColumnTitle>
                    <BoxColumnText>{datos.deliveryAdress}</BoxColumnText>
                
                    <BoxColumnTitle>Contacto del responsable</BoxColumnTitle>
                    <BoxColumnText>{datos.contactName}</BoxColumnText>
                  </BoxColumn>
                </BoxContent>
              </Box3>
            </BoxContainer3>
          </Info>
        </Main>
        {edit === true ? (
          <Footer open={active}>
            <Products>
              Ver Productos
              <button
                onClick={() => onClick()}
                style={{
                  backgroundColor: "transparent",
                  border: "none",
                  cursor: "pointer",
                }}
              >
                <img
                  src={active.length ? "img/arrowUp.png" : "img/arrowDown.png"}
                ></img>
              </button>
            </Products>
            <Link href={"/interactions"}>
              <Buttons style={{ cursor: "pointer" }}>Interacciones</Buttons>
            </Link>
            <Link style={{ cursor: "pointer" }} href={`/edit/${datos.id}`}>
              <button
                onClick={() => (window.location.href = `/edit/${datos.id}`)}
                style={{
                  backgroundColor: "transparent",
                  border: "none",
                  cursor: "pointer",
                }}
              >
                <img
                  style={{
                    cursor: "pointer",
                  }}
                  src={"/img/pencilModal.png"}
                />
              </button>
            </Link>
          </Footer>
        ) : (
          <Footer open={active}>
            
            <Products onClick={() => onClick()}>
              Ver Productos
              <button
                onClick={() => onClick()}
                style={{
                  backgroundColor: "transparent",
                  border: "none",
                  cursor: "pointer",
                }}
              >
                <img
                  src={active.length ? "img/arrowUp.png" : "img/arrowDown.png"}
                  style={{ marginLeft: "6px" }}
                ></img>
              </button>
            </Products>

            <Buttons onClick={accept}>Aceptar Productos</Buttons>


            <Buttons
              onClick={() => setcontraoferta(!contraoferta)}
              style={
                contraoferta
                  ? { backgroundColor: "#476ADE", color: "white" }
                  : {}
              }
            >
              Realizar una contraoferta
            </Buttons>
            <Buttons onClick={() => createChat()}>Contactar vendedor </Buttons>
            {contraoferta && <Buttons onClick={() => submitContra()} style={{ background: '#476ade', color: 'white' }}> Enviar Contraoferta</Buttons>}

          </Footer>
        )}
      </Wrapper>
      <Options active={active}>
			{
					width<1024?
					<ButtonsContainerMobile>
						<ButtonsSubContainer>
						<Buttons onClick={accept}>Aceptar</Buttons> 
						

            <Buttons
              onClick={() => setcontraoferta(!contraoferta)}
              style={
                contraoferta
                  ? { backgroundColor: "#476ADE", color: "white" }
                  : {}
              }
            >
              Contraoferta
            </Buttons>
            <Buttons onClick={() => createChat()}>Contactar </Buttons>
						{contraoferta && <Buttons onClick={()=>submitContra()} style={{background: '#476ade', color: 'white'}}> Aceptar</Buttons>}
						</ButtonsSubContainer>
						<ButtonsSubContainer>
						<Products active={active} onClick={() => onClick()}>
							Ver Productos
							<button
								onClick={() => onClick()}
								style={{
									backgroundColor: 'transparent',
									border: 'none',
									cursor: 'pointer',
								}}
							>
								<img
									src={
										active.length
											? 'img/arrowUp.png'
											: 'img/arrowDown.png'
									}
									style={{marginLeft: '6px'}}
								></img>
							</button>
						</Products>
						</ButtonsSubContainer>				
					</ButtonsContainerMobile>
					:null
				}
        {datos.variants && !contraoferta ?
        <>
        {
           datos.variants.map((va) => (
              <OptionBox key={va._id}>
                <Input
                  id={va._id}
                  value={datos.variants._id}
                  onClick={() => handleSelect( va)}
                  type="checkbox"
                ></Input>
                <OptionText>
                  <OptionTitle>Tipo:</OptionTitle>
                  <OptionP style={{ marginLeft: "7px" }}>
                    {va.variantType}
                  </OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Cantidad:</OptionTitle>
                  <OptionP style={{ marginLeft: "7px" }}>{va.quantity} {va?.unit}</OptionP>
                  {/* 	<button
									style={{
										backgroundColor: 'transparent',
										border: 'none',
										cursor: 'pointer',
									}}
								>
									<img src='img/arrowDown.png'></img>
								</button> */}
                </OptionText>
                <OptionText>
                  <OptionTitle>Precio: </OptionTitle>
                  <OptionP style={{ marginLeft: "2px" }}> $ {va.price}</OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Descripción: </OptionTitle>
                  <OptionP> {va.description}</OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Marca: </OptionTitle>
                  <OptionP style={{ marginLeft: "0px" }}> {va.brand}</OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Color: </OptionTitle>
                  <OptionP style={{ marginLeft: "0px" }}> {va.color}</OptionP>
                </OptionText>
                <button
                  style={{
                    background: "none",
                    border: "none",
                    cursor: "pointer",
                  }}
                  onClick={(e) => onModal(e, va.image)}
                >
                  <img src={"/img/ModalImage.png"} alt="" />
                </button>
              </OptionBox>
            ))}
            <ButtonSaveModal props={active} style={active ? {display: 'block', zIndex: '9', width: '200px', border: 'none' , fontWeight: '500', fontSize: '16px', alignSelf: 'flex-start',} : {display:'none'} } onClick={()=>selectAll()}>Seleccionar todos</ButtonSaveModal> 
            </>
          : contraoferta
            ? datos.variants.map((va) => (
              <OptionBox key={va._id}>
                <Input
                  onClick={(e) => handleSelectContra(e, va)}
                  type="checkbox"
                ></Input>
                <OptionText>
                  <OptionTitle>Tipo:</OptionTitle>
                  <OptionP style={{ marginLeft: "7px" }}>
                    {va.variantType}
                  </OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Cantidad:</OptionTitle>
                  <InputContra
                    name="quantity"
                    value={onValueQuantity(va._id)}
                    onChange={(e) => onChange(e, va._id)}
                    placeholder={va.quantity + " /u"}
                    disabled={!selectedcontra.products.length}
                    style={{ marginLeft: "7px" }}
                  ></InputContra>
                  {/* 	<button
									style={{
										backgroundColor: 'transparent',
										border: 'none',
										cursor: 'pointer',
									}}
								>
									<img src='img/arrowDown.png'></img>
								</button> */}
                </OptionText>
                <OptionText>
                  <OptionTitle>Precio: </OptionTitle>
                  <InputContra
                    name="price"
                    value={onValuePrice(va._id)}
                    onChange={(e) => onChange(e, va._id)}
                    disabled={!selectedcontra.products.length}
                    placeholder={"$" + va.price}
                    style={{ marginLeft: "2px" }}
                  ></InputContra>
                </OptionText>
                <OptionText>
                  <OptionTitle>Descripción: </OptionTitle>
                  <OptionP> {va.description}</OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Marca: </OptionTitle>
                  <OptionP style={{ marginLeft: "0px" }}> {va.brand}</OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Color: </OptionTitle>
                  <OptionP style={{ marginLeft: "0px" }}> {va.color}</OptionP>
                </OptionText>
                <button
                  style={{
                    background: "none",
                    border: "none",
                    cursor: "pointer",
                  }}
                  onClick={(e) => onModal(e, va.image)}
                >
                  <img src={"/img/ModalImage.png"} alt="" />
                </button>
              </OptionBox>
            ))
            : null}
      </Options>
       
      {modal.open && (
        <Modal>
          {/* 	<ModalTitle>Imagen</ModalTitle> 
          	<ModalImageContainer>
             <ModalImage src={datos?.variants?.image ? datos.variants.image  :''}/>
         		 </ModalImageContainer>
         		<ModalButtonsContainer style={{justifyContent: 'center'}}>
            <ButtonSaveModal onClick={()=>setModal(!modal)}>Salir</ButtonSaveModal>  
          	</ModalButtonsContainer>   */}
          <ModalTitle>Imagen</ModalTitle>
          <ModalImageContainer>
            <ModalImage
              src={datos?.variants[0].image ? datos.variants[0].image : ""}
            />
          </ModalImageContainer>
          <ModalButtonsContainer>
            <ButtonSaveModal onClick={() => setModal(!modal)}>
              Salir
            </ButtonSaveModal>
          </ModalButtonsContainer>
        </Modal>
      )}
    </Container>
  );
};
export default Posts;
