
import React ,{useState, useEffect} from 'react';
import editPost from '../../../../controllers/post/editPost';
import {
  Date,
  Wrapper,
  Box1,
  Box2,
  Box3,
  NameCompany, 
  OptionP,
  Category,
  OptionText, 
  OptionTitle, 
  BoxContainer2 , 
  BoxContainer3, 
  Info, 
  Main,
  Input,
  OptionInfo,
  Options, 
  Subtitle, 
  BoxContent, 
  BoxColumn, 
  BoxColumnTitle,
  BoxContainer, 
  BoxColumnInput, 
  OptionBox,
  Title, 
  Footer, 
  Products,
  BtnDelete,
  
    } from './styles';




const PostUpdate2 = ({props, id}) => {
  const[active, setActive] = useState('');
  const [updateData , setUpdateData] = useState(props);
  console.log('props',id);
  console.log(updateData);
 
function onChangeQuote (e) {
    e.preventDefault();
    setUpdateData({
      ...updateData,
      quote: {
        ...updateData.quote,
        [e.target.name] : e.target.value
      }
    })
  
} 

 async function onSubmit () {
  let body = JSON.stringify(updateData) 
  const edit = await editPost(body, id);
  window.location.reload();
}

function onChangeVariants (e) {
  e.preventDefault();
    setUpdateData({
      ...updateData,
      variants: {
        ...updateData.variants,
        [e.target.name] : e.target.value
      }
    })
  
}  

function onChangeContact (e) {
  e.preventDefault();
    setUpdateData({
      ...updateData,
      contact: {
        ...updateData.contact,
        [e.target.name] : e.target.value
      }
    })
  
}  

function onChangeProposal (e) {
  e.preventDefault();
    setUpdateData({
      ...updateData,
      proposal: {
        ...updateData.proposal,
        [e.target.name] : e.target.value
      }
    })
  
}  
  

function onActive(){
      if(active!=='active')setActive('active');
      else setActive('');
    }
  

  return (
    <>
   
      <Wrapper>
        <Title>
            <NameCompany><Category></Category></NameCompany>
            <Date>Desde 17/12/2021 10:00 hasta 14/12/2021</Date>
        </Title>
      
      
      
     
      <Main>
          <Info>
            <BoxContainer>
                  <Subtitle>Cotización</Subtitle>
                  <Box1>
                      <BoxContent>
                          <BoxColumn>
                              <BoxColumnTitle>Tipo de PCD</BoxColumnTitle>
                                <BoxColumnInput onChange={(e)=> onChangeQuote(e)} name='pdc' type={'text'} value={updateData.quote?.pdc} />
                              <BoxColumnTitle>Terminos y Condiciones</BoxColumnTitle>
                              <BoxColumnInput type={'text'} />
                          </BoxColumn>
                          <BoxColumn>
                              <BoxColumnTitle>Forma de Pago</BoxColumnTitle>
                              <BoxColumnInput type={'text'} />
                              <BoxColumnTitle>Observaciones</BoxColumnTitle>
                              <BoxColumnInput type={'text'} />
                          </BoxColumn>
                        </BoxContent> 
                    </Box1>
                </BoxContainer>
             <BoxContainer2>  
                  <Subtitle>Propuesta</Subtitle>
                  <Box2>
                      <BoxContent>
                          <BoxColumn>
                                <BoxColumnTitle>Facturación mínima</BoxColumnTitle>
                                <BoxColumnInput type={'text'} onChange={(e)=> onChangeProposal(e)} name='minimum' value={updateData.proposal?.minimum}/>
                                <BoxColumnTitle>Validez de la propuesta</BoxColumnTitle>
                                <BoxColumnInput style={{width: '110px'}} type={'text'}  onChange={(e)=> onChangeProposal(e)} name='endDate' value={updateData.proposal?.endDate}/>
                                <BoxColumnTitle>Incluye flete</BoxColumnTitle>
                                <BoxColumnInput style={{width: '25px'}} type={'text'}  onChange={(e)=> onChangeProposal(e)} name='includesTransport' value={updateData.proposal?.includesTransport}/>
                          </BoxColumn>
                          <BoxColumn >
                                <BoxColumnTitle>Plazo de entrega</BoxColumnTitle>
                                <BoxColumnInput type={'textarea'}  onChange={(e)=> onChangeProposal(e)} name='shippingInterval' value={updateData.proposal?.shippingInterval}/>
                                <BoxColumnTitle>Condiciones de pago</BoxColumnTitle>
                                <BoxColumnInput type={'text'}/>
                                <BoxColumnTitle>Observaciones</BoxColumnTitle>
                                <BoxColumnInput type={'text'}  onChange={(e)=> onChangeProposal(e)} name='notes' value={updateData.proposal?.notes}/>
                          </BoxColumn>
                          </BoxContent> 
                      </Box2>
                  </BoxContainer2>
              <BoxContainer3>
                  <Subtitle>Contacto</Subtitle>
                  <Box3>
                      <BoxContent>
                          <BoxColumn style={{width: '250px'}}>
                            <BoxColumnTitle>Dirección de entrega</BoxColumnTitle>
                            <BoxColumnInput type={'text'}  onChange={(e)=> onChangeContact(e)} name='sellingAddress' value={updateData.contact?.sellingAddress}/>
                            <BoxColumnTitle>Dirección de facturación</BoxColumnTitle>
                            <BoxColumnInput type={'text'} onChange={(e)=> onChangeContact(e)}  name='shippingAddress' value={updateData.contact?.shippingAddress}/>
                            <BoxColumnTitle>Contacto del responsable</BoxColumnTitle>
                            <BoxColumnInput type={'text'} onChange={(e)=> onChangeContact(e)}  name='contactName' value={updateData.contact?.contactName}/>
                          </BoxColumn>
                        
                          </BoxContent> 
                      </Box3> 
                  </BoxContainer3> 
          </Info>
      </Main>
          
            <Footer>
                <Products>Ver Productos <button onClick={()=>onActive()} style={{backgroundColor: 'transparent', border: 'none', cursor: 'pointer'}}><img src={active.length ? 'img/arrowUp.png':'img/arrowDown.png'}></img></button></Products>
               
                
            </Footer> 
           
    </Wrapper>
     <Options>
        {
          active==='active'  ?
           
            
             
                <OptionBox>
                <OptionInfo>
                    <Input type='checkbox' ></Input>
                        <OptionText style={{textAlign: 'left'}}>
                            <OptionTitle>Tipo:</OptionTitle>
                            <OptionP type={'text'} onChange={(e)=> onChangeVariants(e)}  name='variantType' value={updateData.variants?.variantType}  />
                        </OptionText>
                        <OptionText>
                          <OptionTitle>Cantidad:</OptionTitle>
                          <OptionP type={'text'} />
                          <p style={{fontSize: '16px', fontWeight :400, color: 'rgba(0, 0, 0, 0.6)',padding:'10px'}}>unidades</p>
                            <button style={{backgroundColor: 'transparent', border: 'none', cursor: 'pointer'}}><img src='img/arrowDown.png'></img></button>
                        </OptionText>
                        <OptionText>
                            <OptionTitle>Precio: </OptionTitle>
                            <OptionP type={'text'} />
                            <p style={{fontSize: '16px', fontWeight :400, color: 'rgba(0, 0, 0, 0.6)'}}>/u.</p>
                        </OptionText>
                        <OptionText>
                            <OptionTitle>Descripción: </OptionTitle>
                            <OptionP type={'text'} />
                        </OptionText>
                        <OptionText>
                            <OptionTitle>Marca: </OptionTitle>
                            <OptionP type={'text'}  />
                        </OptionText>
                        <OptionText>
                            <OptionTitle>Color: </OptionTitle>
                            <OptionP type={'text'} />
                        </OptionText>
                     <img  style={{margin: 'auto'}} src='img/Vector (1).png'/> 
              </OptionInfo>
              <BtnDelete  ><img style={{ width: '16px' }} src='img/delete.png' /></BtnDelete>
              </OptionBox>
             
            
            
          
        
             : null
        }
        </Options>
        
    
       <button onClick={()=>onSubmit()}>test</button>
    </>
  )
}

export default PostUpdate2;