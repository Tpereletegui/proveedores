import React ,{useState, useEffect} from 'react';
import Modal from '../../Modal';
import useFetch from '../../../../hooks/useFetch.jsx';
import editPost from '../../../../controllers/post/editPost';
import Switch from '../../switch';
import axios from 'axios';
import deletePost from '../../../../controllers/post/deletePost';
import {
  Wrapper,
  Subtitle,
  InputWrapper,
  Input,
  InputContainer,
  InputTitle,
  OptionBox,
  Options,
  OptionInput,
  ButtonContainers,
  ButtonSave,
  ButtonDelete,
  TitleContainer,
  Company,
  Category,
	SelectCategories,
	SelectedContainer,
	Selected,
	SelectedDelete,
	CategoriesContainer,
  ModalButtonsContainer,
  ModalTitle,
  ModalParagraph,
  ButtonSaveModal,
  ButtonDeleteModal,
  ModalOpen,
  ProductsContainer,
  ProductsWrapper,
  ParagraphProducts,
  AddButton,
  AddNew,
  ModalImage,
  ModalImageContainer,
  InputDate,
  SelectData,
  SelectOption,
  OptionSelect,
  CategoryOption
  
    } from './styles';




const PostUpdate = ({props, id, categories}) => {
  console.log('props', id)
  const[active, setActive] = useState('');
  const [modalImage, setModalImage] =useState(false);
  const [modal, setModal] =useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const[ modalSubmit, setModalSubmit] = useState(false);
  const [deleted, setDeleted]=useState(false);
  const [image, setImage] =useState('');
  const [error, setError] =useState(false);
  const [isToggledPdc, setToggledPdc] =useState(false);
  const [isToggledFlete, setToggledFlete] =useState(false);
  const [renderCategories, setrenderCategories] = useState(false)
  const [finished, setFinished] =useState(false);
  console.log('image', image);
  const [updateData , setUpdateData] = useState({
    categories: [],
    contact : {
      contactName : props.data.contact.contactName ?? '',
      sellingAddress : props.data.contact.sellingAddress,
      _id : props.data.contact._id
    },
    createdAt : props.data.createdAt ?? '',
    proposal: {
      minimum : '',
      notes: props.data.proposal.notes ?? '',
      shippingInterval : props.data.proposal.shippingInterval ?? '',
      endDate: '',
      includesTransport : false,
      _id: props.data.proposal._id ?? '',
     },
    quote : {
      pdc : props.data.quote.pdc ?? '',
      paymentMethod : '',
      notes: props.data.quote.notes ?? '',
      _id : props.data.quote._id ?? ''
    },
    seller_id: props.data.seller_id ?? '',
    updatedAt: props.data.updatedAt ?? '' ,
    _id: props.data._id,
    variants : props.data.variants
  });
  const [ newVariant, setNewVariant] = useState({
    _id : updateData.variants?.length + 1 ?? 0,
    variantType: "",
		quantity: 0,
		price: 0,
		description: "",
		brand: "",
		color: "",
    image: "",
  })
  console.log('newvarian', newVariant)
  const [add, setAdd] =useState(false);
	const [categoriesSelected, setCategoriesSelected] = useState([]);
  console.log('props',id);
  console.log('updateddata',updateData);

  function checkEmpty() {
    let quote = checkProperties(updateData.quote);
    let proposal = checkProperties(updateData.proposal);
    let contact = checkProperties(updateData.contact);
    
    if(quote || proposal || contact || updateData.variants.length === 0 || updateData.categories.length === 0){
      setError(true);
    }else{
      setError(false);
    }
  }

  function checkProperties(obj) {
    for (var key in obj) {
        if (obj[key] !== null && obj[key] != "")
            return false;
    }
    return true;
}
 
function onChangeQuote (e) {
    e.preventDefault();
    setUpdateData({
      ...updateData,
      quote: {
        ...updateData.quote,
        [e.target.name] : e.target.value
      }
    })
  
} 

//TOOGLES 

function onTogglePdc(){
  setToggledPdc(!isToggledPdc);
  if(isToggledPdc === true);
  setUpdateData({
    ...updateData,
    quote : {
      ...updateData.quote,
      pdc: 'Comprar'
    }
  });
  if(isToggledPdc ===false){
    setUpdateData({
      ...updateData,
      quote : {
        ...updateData.quote,
        pdc: 'Vender'
      }
    });
  }
}
function onToggleFlete(){
  setToggledFlete(!isToggledFlete);
  if(isToggledFlete){

    setUpdateData({
      ...updateData,
      proposal: {
        ...updateData.proposal,
        includesTransport: false
      }
    })
  }else {
    setUpdateData({
      ...updateData,
      proposal: {
        ...updateData.proposal,
        includesTransport: true
      }
    })
  }
  
}

//FINISH TOOGLES

function showImage(image){
  setImage(image);
  setModalImage(!modalImage);
}


 function onSubmit () {
  function deleteId(){
    updateData.variants.map( x=> {
      delete x._id
    })
  } 
  deleteId();
  console.log('upadatevariants', updateData.variants)
  let body = JSON.stringify(updateData); 
   editPost(body, updateData._id )
   .then( response => {
    console.log('responseedit', response)
    setFinished(!finished);
   })
 
}

function onDelete() {
  deletePost(id)
  .then(
    response => setDeleted(true)
  )
}

function openModalSubmit(){
  checkEmpty();
  setModalSubmit(!modalSubmit);
}

async function onChangeVariants (e) {
  e.preventDefault();
	if(e.target.name === 'quantity' || e.target.name === 'price'){
	
		console.log('number', e.target.value)
		setNewVariant({
      ...newVariant,
        [e.target.name] : e.target.value.length ?  parseInt(e.target.value) : 0
    })
	}else{
		setNewVariant({
      ...newVariant, 
        [e.target.name] : e.target.value
    })
	}
   
  
}  

function AddNewVariant() {
  setUpdateData({
    ...updateData,
    variants : [...updateData.variants, newVariant]
  })
  setNewVariant({
    _id : updateData.variants.length + 1,
    variantType: "",
    quantity: 0,
    price: 0,
    description: "",
    brand: "",
    color: ""
  })
  setAdd(!add);
}




function deleteVariant(id) {
  let filtered = updateData.variants.filter( variant => variant._id !== id);
  console.log('filter', filtered);
  console.log('id', id)
  setUpdateData({
    ...updateData, 
    variants: filtered
  })
}

function onChangeContact (e) {
  e.preventDefault();
    setUpdateData({
      ...updateData,
      contact: {
        ...updateData.contact,
        [e.target.name] : e.target.value
      }
    })
  
}  

function onChangeProposal (e) {
  e.preventDefault();
    setUpdateData({
      ...updateData,
      proposal: {
        ...updateData.proposal,
        [e.target.name] : e.target.value
      }
    })
  
}  
function onSelectCategory(e) {
  console.log("name", e.target.name);
  console.log("value", e.target.value);
  /* 	const index = e.target.selectedIndex;
  const el = e.target.childNodes[index] */
  //const option =  el.getAttribute('label');
  let check = updateData.categories.find((x) => e.target.value === x);
  console.log("check", check);
  if (check) {
    return;
  } else {
    setUpdateData({
      ...updateData,
      categories: [...updateData.categories, e.target.value],
    });
    setCategoriesSelected([
      ...categoriesSelected,
      { name: e.target.name, value: e.target.value },
    ]);
  }
}

function onDeleteSelected (e) {
  console.log('delete',e.target.value);
  setUpdateData({
    ...updateData,
    categories : updateData.categories.filter(x => x !== e.target.value)
  })
  let filter = categoriesSelected.filter(x => x.value !== e.target.value)
  console.log('filter', filter)
  setCategoriesSelected(filter)
}

function uploadImage(files) {
  console.log('files',files[0]);
  const formData = new FormData();
  formData.append('file', files[0]);
  formData.append('upload_preset','azihkt5g');
  axios.post('https://api.cloudinary.com/v1_1/proveedores/image/upload', formData)
  .then(response => {
    console.log('response', response)
    setNewVariant({
    ...newVariant,
    image: response.data.url
  })});
}

function onActive(){
      if(active!=='active')setActive('active');
      else setActive('');
    }
  

  return (
    <>
    <Wrapper>
      <TitleContainer>
        <Company>Zara</Company>
        <Category>Indumentaria</Category>
      </TitleContainer>
      <Subtitle>Cotizacion</Subtitle>
      <InputWrapper>
        <InputContainer>
              <InputTitle>Tipo de PDC</InputTitle>
              <Switch  isToggled={isToggledPdc} onToggle={()=>onTogglePdc()} on={'Comprar'} off={'Vender'}/>
          </InputContainer>
          <InputContainer>
        <InputTitle>Forma de Pago *</InputTitle>
            <SelectData name="paymentMethod" onChange={(e)=> onChangeQuote(e)}>
                  <SelectOption>Seleccionar</SelectOption>
                  <SelectOption>Efectivo</SelectOption>
                  <SelectOption>Cheque</SelectOption>
                  <SelectOption>Transferencia Bancaria</SelectOption>
                </SelectData>
          </InputContainer>
          
        </InputWrapper>
        <InputWrapper>
       
          <InputContainer>
              <InputTitle>Observaciones</InputTitle>
              <Input onChange={(e)=> onChangeQuote(e)} name='notes' type={'text'} value={updateData.quote?.notes} />
          </InputContainer>
        </InputWrapper>
        <Subtitle>Propuesta</Subtitle>
      <InputWrapper>
        <InputContainer>
        <InputTitle>Facturación mínima *</InputTitle>
                <SelectData name="minimum" onChange={(e)=>onChangeProposal(e)}>
                  <SelectOption value={'Sin monto minimo'}>Sin monto mínimo</SelectOption>
                  <SelectOption value={'50000'}>$ 50.000</SelectOption>
                  <SelectOption value={'75000'}>$ 75.000</SelectOption>
                  <SelectOption value={'100000'}>$ 100.000</SelectOption>
                  <SelectOption value={'125000'}>$ 125.000</SelectOption>
                  <SelectOption value={'150000'}>$ 150.000</SelectOption>
                  <SelectOption value={'175000'}>$ 175.000</SelectOption>
                  <SelectOption value={'200000'}>$ 200.000 o más</SelectOption>
                </SelectData>
          </InputContainer>
          <InputContainer>
              <InputTitle>Incluye flete</InputTitle>
              <Switch  isToggled={isToggledFlete} onToggle={()=>onToggleFlete()} on={'No incluye'} off={'Incluye'}/>
          </InputContainer>
        </InputWrapper>
        <InputWrapper>
        
          <InputContainer>
              <InputTitle>Plazo de entrega</InputTitle>
              <Input  onChange={(e)=> onChangeProposal(e)} name='shippingInterval' value={updateData.proposal?.shippingInterval} />
          </InputContainer>
          <InputContainer>
              <InputTitle>Condiciones de pago</InputTitle>
              <Input />
          </InputContainer>
          </InputWrapper>
          <InputWrapper>
          
          <InputContainer>
              <InputTitle>Observaciones</InputTitle>
              <Input  onChange={(e)=> onChangeProposal(e)} name='notes' value={updateData.proposal?.notes} />
          </InputContainer>
          <InputContainer >
                <InputTitle>Validez de la propuesta *</InputTitle>
                <Input type='date' name='endDate' value={updateData.proposal?.endDate} onChange={(e)=>onChangeProposal(e)}/>
              </InputContainer>
        </InputWrapper>
        <Subtitle>Contacto</Subtitle>
      <InputWrapper>
        <InputContainer>
              <InputTitle>Dirección de entrega</InputTitle>
              <Input onChange={(e)=> onChangeContact(e)} name='sellingAddress' value={updateData.contact?.sellingAddress} />
          </InputContainer>
          
        
        <InputContainer>
              <InputTitle>Contacto del responsable</InputTitle>
              <Input  onChange={(e)=> onChangeContact(e)}  name='contactName' value={updateData.contact?.contactName} />
          </InputContainer>
        </InputWrapper>
        <Subtitle>Productos</Subtitle>
        {
         updateData.variants?.length ? 
         updateData.variants.map(x => {
           return(
             <>
             <ProductsWrapper>
               <button onClick={()=>deleteVariant(x._id)} style={{background: 'none', border: 'none', cursor: 'pointer', marginBottom: '20px'}}><img src={'/img/delete.png'}></img></button>
              <ProductsContainer>
                <InputContainer style={{width: '50%'}}>
                  <ParagraphProducts>Nombre: {x.variantType}</ParagraphProducts>
                </InputContainer>
                <InputContainer style={{width: '50%'}}>
                  <ParagraphProducts>Cantidad: {x.quantity}</ParagraphProducts>
                </InputContainer>
                <InputContainer style={{width: '50%'}}>
                  <ParagraphProducts>Precio: {x.price}</ParagraphProducts>
                </InputContainer>
                <InputContainer style={{width: '50%'}}>
                  <ParagraphProducts>Marca: {x.brand}</ParagraphProducts>
                </InputContainer>
                <InputContainer style={{width: '50%'}}>
                  <ParagraphProducts>Color: {x.color}</ParagraphProducts>
                </InputContainer>
                <InputContainer style={{width: '70%', overflowX: 'hidden', height: 'max-content'}} >
                  <ParagraphProducts>Descripción: {x.description}</ParagraphProducts>
                </InputContainer>

              </ProductsContainer>
              <button onClick={()=>showImage(x.image)}><img  src={'/img/imageUpload.png'}/></button>
              </ProductsWrapper>
              
              </>
           )
         }) : null
       
       }


          {!add && <AddNew onClick={()=> setAdd(!add)}>Añadir producto +</AddNew>}
         { 
          add &&
         <Options>
            <OptionBox>
            <InputWrapper>
                <InputContainer>
                      <InputTitle>Tipo </InputTitle>
                      <OptionInput onChange={(e)=> onChangeVariants(e)}  name='variantType' value={newVariant.variantType} />
                    </InputContainer>
                   <InputContainer>
                      <InputTitle>Cantidad</InputTitle>
                      <OptionInput onChange={(e)=> onChangeVariants(e)} name='quantity' type={'text'} value={newVariant.quantity} />
                   </InputContainer>
              </InputWrapper>
              <InputWrapper>
                <InputContainer>
                      <InputTitle>Precio (unidad) </InputTitle>
                      <OptionInput onChange={(e)=> onChangeVariants(e)} name='price' type={'text'} value={newVariant.price} />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Unidad </InputTitle>
                      <OptionSelect
                        onChange={(e) => onChangeVariants(e)}
                        name="unit"
                        type={"text"}
                      >
                        <option value="kg">Kilogramos</option>
                        <option value="m">Metros</option>
                        <option value="u">Unidades</option>
                      </OptionSelect>
                      </InputContainer>
                   <InputContainer>
                      <InputTitle>Marca</InputTitle>
                      <OptionInput onChange={(e)=> onChangeVariants(e)} name='brand' type={'text'} value={newVariant.brand} />
                   </InputContainer>
              </InputWrapper>
              <InputWrapper>
              <InputContainer style={{width: '90%', margin: 'auto'}}>
                      <InputTitle>Descripción</InputTitle>
                      <OptionInput onChange={(e)=> onChangeVariants(e)} name='description' type={'text'} value={newVariant.description} />
                   </InputContainer>
              </InputWrapper>
              <InputWrapper>
                <InputContainer>
                      <InputTitle>Color </InputTitle>
                      <OptionInput onChange={(e)=> onChangeVariants(e)} name='color' type={'text'} value={newVariant.color} />
                    </InputContainer>
                   <InputContainer>
                      <InputTitle>Imagen</InputTitle>
                      <OptionInput type='file' onChange={(e)=>uploadImage(e.target.files)} />
                   </InputContainer>
              </InputWrapper>
              <AddButton onClick={()=>AddNewVariant()}>add</AddButton>
            </OptionBox>
          </Options>}
          <div id="categorias" style={{ height: "50px" }}></div>
            <Subtitle data-aos="fade-right" data-aos-duration="1500">
              Categorias
            </Subtitle>
            <CategoriesContainer data-aos="flip-up" data-aos-duration="1500">
              <p
                style={{ display: "flex", gap: "10px", alignItems: "center" }}
                onClick={() => {
                  setrenderCategories(!renderCategories);
                }}
              >
                Seleccionar
                {renderCategories ? (
                  <img src="/img/arrowUp.png" />
                ) : (
                  <img src="/img/arrowDown.png" />
                )}
              </p>

              <SelectCategories renderCategories={renderCategories}>
                {categories.map((x, i) => {
                  return (
                    <div key={i}>
                      <CategoryOption
                        onClick={(e) => onSelectCategory(e)}
                        name={x.name}
                        key={x._id}
                        value={x._id}
                      >
                        {x.name}
                      </CategoryOption>
                    </div>
                  );
                })}
              </SelectCategories>
						<SelectedContainer>
							{
								categoriesSelected.length ? 
								categoriesSelected.map( cat=> {
									
									return(
										<Selected key={cat.value}>{cat.name} <SelectedDelete onClick={(e)=> onDeleteSelected(e)} value={cat.value}>x</SelectedDelete></Selected>
									)
								})
								:
								null
							}
						</SelectedContainer>
					</CategoriesContainer> 
          <ButtonContainers>
            <ButtonDelete onClick={()=>setModalDelete(!modalDelete)}>Eliminar Publicacion</ButtonDelete>
            <ButtonSave onClick={()=>openModalSubmit()}>Guardar Publicacion</ButtonSave>
          </ButtonContainers>
    </Wrapper>
    {
        modalImage && 
        <Modal>
          
          <ModalTitle>Imagen</ModalTitle> 
          <ModalImageContainer>
             <ModalImage src={image ? image : ''}/>
          </ModalImageContainer>
          <ModalButtonsContainer style={{justifyContent: 'center'}}>
            <ButtonSaveModal onClick={()=>{setModalImage(!modalImage)}}>Salir</ButtonSaveModal>  
          </ModalButtonsContainer>

        </Modal>
      }

{
        modal && 
        <Modal>
          <ModalTitle>Cancelar</ModalTitle> 
          <ModalParagraph>Seguro que desea salir? Sus cambios no se guardarán</ModalParagraph>
          <ModalButtonsContainer>
            <ButtonSaveModal onClick={()=>window.location.href = '/'}>Salir</ButtonSaveModal>
            <ButtonDeleteModal onClick={()=>setModal(!modal)} >Cancelar</ButtonDeleteModal>
          </ModalButtonsContainer>

        </Modal>
      }
        {
        modalSubmit && 
        <Modal>
        { 
        error ?       
        <>
        <ModalTitle>Editar publicación</ModalTitle> 
         <ModalParagraph>Faltan datos por completar. Por favor, revise el formulario. </ModalParagraph>
         <ModalButtonsContainer style={{justifyContent: 'center'}}>
           <ButtonSaveModal  onClick={()=>setModalSubmit(!modalSubmit)}>Volver</ButtonSaveModal>
           
         </ModalButtonsContainer>
         </>
         :
        !finished ?  
        <>
         <ModalTitle>Editar publicación</ModalTitle> 
          <ModalParagraph>Se editará la publicación con los datos ingresados. Desea continuar ? </ModalParagraph>
          <ModalButtonsContainer>
            <ButtonSaveModal disabled={error}  onClick={()=>onSubmit()}>Continuar</ButtonSaveModal>
            <ButtonDeleteModal onClick={()=>setModalSubmit(!modalSubmit)} >Cancelar</ButtonDeleteModal>
          </ModalButtonsContainer>
          </>
          : finished  &&
             <>
             <ModalTitle>Editar publicación</ModalTitle> 
              <ModalParagraph>Cambios aplicados con éxito! </ModalParagraph>
                    <ModalButtonsContainer style={{justifyContent: 'center'}}>
                      <ButtonSaveModal onClick={()=>window.location.href ='/'}>Continuar</ButtonSaveModal> 
                    </ModalButtonsContainer>
             </>
           
        }

        </Modal>
      } 
      {
        modalDelete && 
        <Modal>
          {!deleted ?
            <>
            <ModalTitle>Eliminar Publicacion</ModalTitle> 
            <ModalParagraph>Seguro que desea eliminar?</ModalParagraph>
            <ModalButtonsContainer>
              <ButtonSaveModal onClick={()=>onDelete()}>Eliminar</ButtonSaveModal>
              <ButtonDeleteModal onClick={()=>setModalDelete(!modalDelete)} >Cancelar</ButtonDeleteModal>
            </ModalButtonsContainer>
            </>
             :

             <>
             <ModalTitle>Eliminar Publicacion</ModalTitle> 
             <ModalParagraph>Publicación Eliminada con éxito</ModalParagraph>
             <ModalButtonsContainer>
               <ButtonSaveModal onClick={()=>window.location.href ='/'}>Continuar</ButtonSaveModal>
             </ModalButtonsContainer>
             </>
              
          }
         

        </Modal>
      }      
  
   
    </>
  )
}

export default PostUpdate;