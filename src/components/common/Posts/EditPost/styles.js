import styled from "styled-components";



export const Wrapper = styled.div`
  width: 700px;
  height: max-content;
  background-color: white;
  margin: auto; 
  margin-bottom: 0;
  margin-top: 5%;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
  border-radius: 14px;
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 30px;
  margin-bottom: 100px;
  @media (max-width: 714px) {
  width: 100%;
  }
  @media  (min-width: 1624px) {
  width: 700px;
  }
  

`

export const TitleContainer =styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  margin-bottom: 20px;
  @media (max-width: 714px) {
  flex-direction: column;
  }
`

export const Company =styled.p`
  color: black;
  font-size: 20px;
  margin-right: 15px;
`

export const Category = styled.p`
 color: white;
  font-size: 20px;
  background-color: #476ADE;
  padding: 5px;
  border-radius: 8px;

`


export const Subtitle=styled.p`
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 22px;
  color: #476ADE;
  margin-bottom: 50px;
  margin-top: 50px;
 
`


export const InputWrapper =styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  width: 100%;
  @media (max-width: 700px) {
    flex-direction: column;
  }
  
`

export const InputContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 300px;
  flex-direction: column;
`

export const InputTitle =styled.p`
   font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 25px;
  margin-bottom: 15px;
  text-align: center;
  width: 100%;
  color: black;
`

export const Input = styled.input`
   font-style: normal;
    font-weight: 500;
    font-size: 16px;
    resize: none;
    margin-bottom: 15px;
    background-color: #F8F8F8;
    border: none;
    border-radius: 5px;
    text-align: center;
    padding: 10px;
    width: 100%;
    height: 60px;
    color: rgba(0, 0, 0, 0.6);
    &:focus {
      outline: none;
      border-bottom: 1.5px solid #476ADE;
    }
`

export const InputDate =styled.input`
   font-style: normal;
    font-weight: 500;
    font-size: 16px;
    resize: none;
    margin-bottom: 15px;
    background-color: #F8F8F8;
    border: none;
    border-radius: 5px;
    text-align: center;
    padding: 10px;
    width: 100%;
    height: 60px;
    color: rgba(0, 0, 0, 0.6);
    &:focus {
      outline: none;
      border-bottom: 1.5px solid #476ADE;
    }
`

export const Options =styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 80%;
`

export const OptionBox =styled.div`
   display: flex;
  justify-content: space-between;
  padding: 10px;
  align-items: center;
  flex-direction: column;
  background: #FFFFFF;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
  border-radius: 14px;
  width: 100%;
  height: max-content;
  height: max-content;
  margin-top: 1%; 
  
`


export const OptionInput =styled.input`
   font-style: normal;
    font-weight: 500;
    font-size: 16px;
    resize: none;
    margin-bottom: 15px;
    background-color: #F8F8F8;
    border: none;
    border-radius: 5px;
    text-align: center;
    padding: 5px;
    width: 90%;
    height: 40px;
    color: rgba(0, 0, 0, 0.6);
    &:focus {
      outline: none;
      border-bottom: 1.5px solid #476ADE;
    }
    @media (max-width: 714px) {
        width: 80%;
  }

`


export const ButtonContainers =styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;
    width: 80%;
    margin-top: 50px;
    @media (max-width: 714px) {
      flex-direction: column;
  }

`

export const ButtonSave =styled.button `
  background: #FFFFFF;
  border: 1px solid #476ADE;
  box-sizing: border-box;
  border-radius: 8px;
  width: 220px;
  height: 30px;
  
  color: #476ADE;
  cursor: pointer;

`

export const ButtonDelete =styled.button `
  background: #FFFFFF;
  border: 1px solid #DB2C2C;
  box-sizing: border-box;
  border-radius: 8px;
  width: 220px;
  height: 30px;
  color: #DB2C2C;
  cursor: pointer;

`

export const CategoriesContainer =styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`

export const SelectCategories = styled.div`
  position: relative;
	background-color: red;
	width: 300px;
	padding: ${({ renderCategories }) => renderCategories ? '20px 0vw' : '0px'};
	opacity: ${({ renderCategories }) => renderCategories ? '1' : '0'};
	overflow: hidden;
	transition: 0.6s all ease-out;
	background: #ffffff;
	
	border-radius: 0px 0px 8px 8px;

	display: flex;
	flex-wrap: nowrap;
	align-items: center;
	justify-content: center;
  flex-direction: column;
	gap: 20px;
`


export const SelectedContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  flex-wrap: wrap;
  width: 70%;
  margin-top: 40px;
  margin-bottom: 100px;
`

export const Selected = styled.p`
  width: max-content;
  padding: 10px;
  height: max-content;
  border-radius: 20px;
  background-color:#476ADE ;
  color: white;
  text-align: center;
  margin-right: 10px;
  margin-top: 10px;
  
`

export const SelectedDelete =styled.button`
  border: none;
  width: 16px;
  height: 16px;
  background-color: white;
  color: black;
  border-radius: 50%;
`


export const ModalTitle = styled.p`
  font-size: 24px;
  color: #476ADE;
  text-align: center;
  margin-bottom: 70px;


`

export const ModalParagraph =styled.p`
  font-size: 16px;
  color: #000000;
  text-align: center;
  margin-bottom: 50px;
`

export const ModalButtonsContainer =styled.div`
  width: 80%;
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  margin: auto;
`

export const ButtonSaveModal =styled.button `
  background: #FFFFFF;
  border: 1px solid #476ADE;
  box-sizing: border-box;
  border-radius: 8px;
  width: 45%;
  height: 30px;
  
  color: #476ADE;
  cursor: pointer;

`

export const ButtonDeleteModal =styled.button `
  background: #FFFFFF;
  border: 1px solid #DB2C2C;
  box-sizing: border-box;
  border-radius: 8px;
  width: 45%;
  height: 30px;
  color: #DB2C2C;
  cursor: pointer;

`

export const ModalOpen = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: rgba(220,220,220,0.7);
`


export const ProductsContainer =styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  height: max-content;
  justify-content: flex-start;
  padding-left: 20%;

`

export const ProductsWrapper = styled.div`
  width: 80%;
  display: flex;
  flex-direction: column;
  box-shadow:  0px 0px 5px rgba(0, 0, 0, 0.13);
  margin-top: 50px;
  padding: 20px;
  align-items: center;
  border-radius: 10px;
  margin-bottom: 20px;
`

export const ParagraphProducts  =styled.p`
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 25px;
    margin-bottom: 15px;
    text-align: left;
    width: 100%;
    color: black;

`

export const AddButton = styled.button`
  background: #FFFFFF;
  border: 1px solid #476ADE;
  box-sizing: border-box;
  border-radius: 8px;
  width: 50px;
  height: 30px;
  color: #476ADE;
  cursor: pointer;
  margin-top: 20px;

`

export const AddNew = styled.button`
  background: none;
  border: none;
  border-radius: 8px;
  width: max-content;
  height: 30px;
  color: #476ADE;
  font-size: 16px;
  font-weight: 300;
  cursor: pointer;
  margin-top: 20px;
  margin-bottom: 30px;

`

export const ModalImageContainer= styled.div`
  
  max-width: 300px;
  max-height: 600px;
  margin: auto;
  margin-bottom: 30px;
`

export const ModalImage =styled.img`
  max-width: 300px;
  max-height: 550px;
  object-fit: cover;
`

export const SelectData = styled.select`
   font-style: normal;
    font-weight: 500;
    font-size: 16px;
    resize: none;
    margin-bottom: 15px;
    background-color: #F9F9F9;
    border: none;
    /* border-bottom: 1.5px solid #476ADE; */
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
    border-radius: 5px;
    text-align: center;
    padding: 10px;
    padding-bottom: 0;
    width: 100%;
    height: 50px;
    color: rgba(0, 0, 0, 0.6);
    &:focus {
      outline: none;
      border-bottom: 2px solid #476ADE;
    }
`

export const SelectOption = styled.option`
   display: block;
	color: black;
	font-size: 15px;
  border: none;
  background: none;
  cursor: pointer;
	&:hover {
		transition: 0.3s all ease-out;
		transform: scale(1.1, 1.1);
	}
	cursor: pointer;
`

export const OptionSelect = styled.select`
  background: white;
  border: none;
  color: #333;
  box-shadow: 0 0 4px rgba(0, 0, 0, .3);
  border-radius: 6px;
  padding: 4px 10px;
  background-color: #F9F9F9;
`


export const CategoryOption = styled.button`
  display: block;
	color: black;
	font-size: 15px;
  border: none;
  background: none;
  cursor: pointer;
	&:hover {
		transition: 0.3s all ease-out;
		transform: scale(1.1, 1.1);
	}
	cursor: pointer;
`