import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Modal from '../Modal/index'
import { io } from 'socket.io-client'
import Link from 'next/link'
import {
	Date,
	Wrapper,
	Box1,
	Box2,
	Box3,
	NameCompany,
	OptionP,
	Category,
	OptionText,
	OptionTitle,
	BoxContainer2,
	BoxContainer3,
	Info,
	Main,
	Input,
	OptionBox,
	Options,
	OptionImg,
	Subtitle,
	BoxContent,
	BoxColumn,
	BoxColumnTitle,
	BoxContainer,
	BoxColumnText,
	Title,
	Footer,
	Buttons,
	Products,
	ModalTitle,
	ModalImage,
	ModalImageContainer,
	ButtonSaveModal,
	ModalButtonsContainer,
	ModalOpen,
} from './styles'

const Posts = ({ datos, edit, sellerId, categories }) => {

	const [seller, setSeller] = useState({})
	const [names, setNames] = useState([])
	const [modal, setModal] = useState({
		open: false,
		image: '',
	})
	const [selected, setSelected] = useState({
		products: [],
	})
	const [active, setActive] = useState('')
	const [token, setToken] = useState(null)
	const [socket, setSocket] = useState(null)
	const [sellerData, setSellerData] = useState(undefined);
	const [contraoferta, setcontraoferta] = useState(false);
	const [selectedcontra, setselectedcontra] = useState({});

	useEffect(() => {
		const getCategory = () => {
			categories?.map(async cat => {
				const response = await axios.get(`http://localhost:3000/api/category/${cat}`)
				setNames(prev => [...prev, response.data?.data?.name])
			})
		}
		getCategory()
	}, [])

	/* useEffect(() => {
		const a = async () => {
			const response = await axios.get(`http://localhost:3000/api/seller/${sellerId}`)
			if (response.data.data && response.data.data.user_id) {
				const response2 = await axios.get(`http://localhost:3000/api/user/${response.data.data.user_id}`)
				setSellerData(response2?.data?.data)
			}
		}
		a()
	}, []) */

	useEffect(() => {
		setToken(localStorage.getItem('IdToken'))
		console.log('TOKEN', localStorage.getItem('IdToken'))
	}, [])

	useEffect(() => {
		// after authentication if a valid jwt token is saved in the state, instantiate
		// a socket instance without connecting it, can enable autoConnect to connect automatically
		// without having to press the connect button
		if (token) {
			// socket instance with the url of the server in the backend, and an options object
			// the auth field send the jwt token to authenticate the connection with the io middleware in the back
			// and a sessionID if there is any
			const newSocket = io('http://localhost:3001', {
				auth: { token },
				autoConnect: true,
			})
			// saves the socket instance to state
			setSocket(newSocket)
			return () => newSocket.close()
		}
	}, [token])

	/* 	useEffect(() => {
			const getSeller = async () => {
				const response = await axios.get(`http://localhost:3000/api/seller/${sellerId}`)
				setSeller(response.data)
			}
	
			getSeller()
		}, [])
	 */
	const handleSelect = (e, product) => {
		let filtered = selected.products.filter(x => x.id === e.target.value)
		if (filtered.length)
			setSelected({
				...selected,
				products: selected.products.filter(x => x.id !== e.target.value),
			})
		else
			setSelected({
				...selected,
				products: [...selected.products, product],
			})
	}

	function onClick() {
		if (active !== 'active') setActive('active')
		else setActive('')
	}

	function onModal(e, image) {
		e.preventDefault()
		setModal({
			open: true,
			image: !image ? image : '',
		})
	}



	const createChat = () => {
		socket.emit('private message', {
			receiver: { id: sellerData._id, name: sellerData.username },
			text: 'Hola me interesa tu producto...',
		})
		window.location = '/chat'
	}

	return (
		<>
			{modal.open && <ModalOpen></ModalOpen>}
			<Wrapper>
				<Title>
					<NameCompany>
						{seller && seller.data?.businessName}
						{names.map((category, i) => (
							<Category key={i}>{category}</Category>
						))}
					</NameCompany>
					<Date>{datos.validation.slice(0, 10)}</Date>
				</Title>

				<Main>
					<Info>
						<BoxContainer>
							<Subtitle>Cotización</Subtitle>
							<Box1>
								<BoxContent>
									<BoxColumn>
										<BoxColumnTitle>Tipo de PCD</BoxColumnTitle>
										<BoxColumnText>{datos.pcd}</BoxColumnText>
										<BoxColumnTitle>Terminos y Condiciones</BoxColumnTitle>
										<BoxColumnText>{datos.conditions}</BoxColumnText>
									</BoxColumn>
									<BoxColumn>
										<BoxColumnTitle>Forma de Pago</BoxColumnTitle>
										<BoxColumnText>{datos.payment}</BoxColumnText>
										<BoxColumnTitle>Observaciones</BoxColumnTitle>
										<BoxColumnText>{datos.observations1}</BoxColumnText>
									</BoxColumn>
								</BoxContent>
							</Box1>
						</BoxContainer>
						<BoxContainer2>
							<Subtitle>Propuesta</Subtitle>
							<Box2>
								<BoxContent>
									<BoxColumn>
										<BoxColumnTitle>Facturación mínima</BoxColumnTitle>
										<BoxColumnText>{datos.billing}</BoxColumnText>
										<BoxColumnTitle>Validez de la propuesta</BoxColumnTitle>
										<BoxColumnText>{datos.validation.split('T')[0]}</BoxColumnText>
										<BoxColumnTitle>Incluye flete</BoxColumnTitle>
										<BoxColumnText>{datos.includesTransportation}</BoxColumnText>
									</BoxColumn>
									<BoxColumn>
										<BoxColumnTitle>Plazo de entrega</BoxColumnTitle>
										<BoxColumnText>{datos.deliveryDate}</BoxColumnText>
										<BoxColumnTitle>Condiciones de pago</BoxColumnTitle>
										<BoxColumnText>{datos.paymentConditions}</BoxColumnText>
										<BoxColumnTitle>Observaciones</BoxColumnTitle>
										<BoxColumnText>{datos.observations2}</BoxColumnText>
									</BoxColumn>
								</BoxContent>
							</Box2>
						</BoxContainer2>
						<BoxContainer3>
							<Subtitle>Contacto</Subtitle>
							<Box3>
								<BoxContent>
									<BoxColumn style={{ width: '90%' }}>
										<BoxColumnTitle>Dirección de entrega</BoxColumnTitle>
										<BoxColumnText>{datos.deliveryAdress}</BoxColumnText>
										<BoxColumnTitle>Dirección de facturación</BoxColumnTitle>
										<BoxColumnText>{datos.billingAdress}</BoxColumnText>
										<BoxColumnTitle>Contacto del responsable</BoxColumnTitle>
										<BoxColumnText>{datos.contact}</BoxColumnText>
									</BoxColumn>
								</BoxContent>
							</Box3>
						</BoxContainer3>
					</Info>
				</Main>
				{edit === true ? (
					<Footer>
						<Products>
							Ver Productos{' '}
							<button
								onClick={() => onClick()}
								style={{
									backgroundColor: 'transparent',
									border: 'none',
									cursor: 'pointer',
								}}
							>
								<img src={active.length ? 'img/arrowUp.png' : 'img/arrowDown.png'}></img>
							</button>
						</Products>
						<Link href={'/interactions'}>
							<Buttons style={{ cursor: 'pointer' }}>Interacciones</Buttons>
						</Link>
						<Link style={{ cursor: 'pointer' }} href={`/edit/${datos.id}`}>
							<button
								onClick={() => (window.location.href = `/edit/${datos.id}`)}
								style={{
									backgroundColor: 'transparent',
									border: 'none',
									cursor: 'pointer',
								}}
							>
								<img
									style={{
										cursor: 'pointer',
									}}
									src={'/img/pencilModal.png'}
								/>
							</button>
						</Link>
					</Footer>
				) : (
					<Footer>
						<Products>
							Ver Productos{' '}
							<button
								onClick={() => onClick()}
								style={{
									backgroundColor: 'transparent',
									border: 'none',
									cursor: 'pointer',
								}}
							>
								<img src={active.length ? 'img/arrowUp.png' : 'img/arrowDown.png'}></img>
							</button>
						</Products>
						<Buttons>Aceptar Productos</Buttons>
						<Buttons onClick={() => setcontraoferta(!contraoferta)}>Realizar una contraoferta</Buttons>
						<Buttons onClick={() => createChat()}>Contactar vendedor </Buttons>
					</Footer>
				)}
			</Wrapper>
			<Options>
				{active === 'active' && datos.variants
					? datos.variants.map(va => (
						<OptionBox key={va._id}>
							<Input
								value={datos.variants._id}
								onClick={e => handleSelect(e, x)}
								type='checkbox'
							></Input>
							<OptionText>
								<OptionTitle>Tipo:</OptionTitle>
								<OptionP style={{ marginLeft: '7px' }}>{va.variantType}</OptionP>
							</OptionText>
							<OptionText>
								<OptionTitle>Cantidad:</OptionTitle>
								<OptionP style={{ marginLeft: '7px' }}>{va.quantity}</OptionP>
								{/* 	<button
									style={{
										backgroundColor: 'transparent',
										border: 'none',
										cursor: 'pointer',
									}}
								>
									<img src='img/arrowDown.png'></img>
								</button> */}
							</OptionText>
							<OptionText>
								<OptionTitle>Precio: </OptionTitle>
								<OptionP style={{ marginLeft: '2px' }}> $ {va.price}</OptionP>
							</OptionText>
							<OptionText>
								<OptionTitle>Descripción: </OptionTitle>
								<OptionP> {va.description}</OptionP>
							</OptionText>
							<OptionText>
								<OptionTitle>Marca: </OptionTitle>
								<OptionP style={{ marginLeft: '0px' }}> {va.brand}</OptionP>
							</OptionText>
							<OptionText>
								<OptionTitle>Color: </OptionTitle>
								<OptionP style={{ marginLeft: '0px' }}> {va.color}</OptionP>
							</OptionText>
							<button
								style={{ background: 'none', border: 'none', cursor: 'pointer' }}
								onClick={e => onModal(e, va.image)}
							>
								<img src={'/img/ModalImage.png'} alt='' />
							</button>
						</OptionBox>
					))
					: null
				}
			</Options>

			{modal.open && (
				<Modal>
					{/* 	<ModalTitle>Imagen</ModalTitle> 
          	<ModalImageContainer>
             <ModalImage src={datos?.variants?.image ? datos.variants.image  :''}/>
         		 </ModalImageContainer>
         		<ModalButtonsContainer style={{justifyContent: 'center'}}>
            <ButtonSaveModal onClick={()=>setModal(!modal)}>Salir</ButtonSaveModal>  
          	</ModalButtonsContainer>   */}
					<ModalTitle>Imagen</ModalTitle>
					<ModalImageContainer>
						<ModalImage src={datos?.variants[0].image ? datos.variants[0].image : ''} />
					</ModalImageContainer>
					<ModalButtonsContainer>
						<ButtonSaveModal onClick={() => setModal(!modal)}>Salir</ButtonSaveModal>
					</ModalButtonsContainer>
				</Modal>
			)}
		</>
	)
}
export default Posts
