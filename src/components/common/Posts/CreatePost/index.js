import React, { useState, useEffect } from 'react';
import getPosts from '../../../../controllers/post/createPost';
import Modal from '../../Modal';
import axios from 'axios';
import { Image } from 'cloudinary-react';
import Switch from '../../switch';
import Accordion from '../../Accordion';
import AOS from 'aos';
import "aos/dist/aos.css";
import {
  Wrapper,
  Subtitle,
  InputWrapper,
  Input,
  InputContainer,
  InputTitle,
  OptionBox,
  Options,
  OptionInput,
  ButtonContainers,
  ButtonSave,
  ButtonDelete,
  TitleContainer,
  Company,
  Category,
  SelectCategories,
  SelectedContainer,
  Selected,
  SelectedDelete,
  CategoriesContainer,
  ModalButtonsContainer,
  ModalTitle,
  ModalParagraph,
  ButtonSaveModal,
  ButtonDeleteModal,
  ModalOpen,
  ProductsContainer,
  ProductsWrapper,
  ParagraphProducts,
  AddButton,
  AddNew,
  ModalImage,
  ModalImageContainer,
  InputDate,
  Products,
  ParagraphColumn,
  Next,
  CategoryOption,
  OptionSelect,
  SelectData,
  SelectDataContainer,
  SelectOption,
} from "./styles";


const CreatePost = ({ categories }) => {
  //STATES
  const [modal, setModal] = useState(false);
  const [modalSubmit, setModalSubmit] = useState(false);
  const [modalImage, setModalImage] = useState(false);
  const [image, setImage] = useState("");
  console.log("image", image);
  const [processing, setProcessing] = useState(false);
  const [finished, setFinished] = useState(false);
  const [isToggledPdc, setToggledPdc] = useState(false);
  const [isToggledFlete, setToggledFlete] = useState(false);
  const [isToggledValidate, setToggledValidate] = useState(false);
  const [add, setAdd] = useState(false);
  const [categoriesSelected, setCategoriesSelected] = useState([]);
  const [renderCategories, setrenderCategories] = useState(false);
  const [renderBilling, setRenderBilling] = useState(false);

  //buttons form

  const [first, setfirst] = useState(false);
  const [second, setsecond] = useState(false);
  const [third, setthird] = useState(false);
  const [four, setfour] = useState(false);
  const [fifth, setfifth] = useState(false);
  //

  const [data, setData] = useState({
    seller_id: "",
    quote: {
      pdc: "Comprar",
      paymentMethod: "",
      notes: "",
    },
    proposal: {
      minimum: "",
      shippingInterval: "",
      endDate: "5",
      includesTransport: false,
      notes: "",
    },
    contact: {
      sellingAddress: "",
      contactName: "",
    },
    variants: [],
    categories: [],
  });

  const [error, setError] = useState(false);
  console.log("data", data);

  const [newVariant, setNewVariant] = useState({
    id: data.variants.length + 1,
    variantType: "",
    quantity: 0,
    price: 0,
    description: "",
    brand: "",
    color: "",
    image: "",
    unit: "",
  });
  console.log('new', newVariant)
  //ONTESTERROR

  function checkProperties(obj) {
    for (var key in obj) {
      if (obj[key] !== null && obj[key] != "") return false;
    }
    return true;
  }

  function checkEmpty() {
    let quote = checkProperties(data.quote);
    let proposal = checkProperties(data.proposal);
    let contact = checkProperties(data.contact);

    if (
      quote ||
      proposal ||
      contact ||
      data.variants.length === 0 ||
      data.categories.length === 0
    ) {
      setError(true);
    } else {
      setError(false);
    }
  }

  //ON CHANGES
  function onChangeQuote(e) {
    e.preventDefault();
    setData({
      ...data,
      quote: {
        ...data.quote,
        [e.target.name]: e.target.value,
      },
    });
  }

  // ONCHANGES --> TOGGLES
  function onTogglePdc() {
    setToggledPdc(!isToggledPdc);
    if (isToggledPdc === true);
    setData({
      ...data,
      quote: {
        ...data.quote,
        pdc: "Comprar",
      },
    });
    if (isToggledPdc === false) {
      setData({
        ...data,
        quote: {
          ...data.quote,
          pdc: "Vender",
        },
      });
    }
  }
  function onToggleFlete() {
    setToggledFlete(!isToggledFlete);
    if (isToggledFlete) {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          includesTransport: false,
        },
      });
    } else {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          includesTransport: true,
        },
      });
    }
  }

  function onToggleValidate() {
    setToggledValidate(!isToggledValidate);
    if (isToggledValidate) {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          endDate: "5",
        },
      });
    } else {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          endDate: "10",
        },
      });
    }
  }

  useEffect(() => {
    async function getId() {
      let local = await localStorage.getItem("loggedUser");
      console.log("local", local);
      let localParse = JSON.parse(local);
      console.log("localParse", localParse);
      let seller = await axios.get(
        `http://localhost:3000/api/user/getseller?_id=${localParse.id}`
      );
      console.log("tomas", seller);
      setData({
        ...data,
        seller_id: seller.data._id,
      });
    }
    getId();
  }, []);

  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);

  //FINISH TOGGLES


  //ONCHANGE

  function onSelect(e){
    console.log('entre select')
    e.preventDefault();
    setData({
      ...data,
      proposal: {
        ...data.proposal,
        minimum: e.target.value === 'Sin monto mínimo' ? e.target.value : parseInt(e.target.value)
      }
    })
  }


  async function onChangeVariants(e) {
    e.preventDefault();
    if (e.target.name === "quantity" || e.target.name === "price") {
      console.log("number", e.target.value);
      setNewVariant({
        ...newVariant,
        [e.target.name]: e.target.value.length ? parseInt(e.target.value) : 0,
      });
    } else {
      setNewVariant({
        ...newVariant,
        [e.target.name]: e.target.value,
      });
    }
  }

  function onChangeContact(e) {
    e.preventDefault();
    setData({
      ...data,
      contact: {
        ...data.contact,
        [e.target.name]: e.target.value,
      },
    });
  }

  function onChangeProposal(e) {
    e.preventDefault();
    if (e.target.value === "true") {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          [e.target.name]: !data.proposal.includesTransport,
        },
      });
    } else {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          [e.target.name]: e.target.value,
        },
      });
    }
  }

  function onSelectCategory(e) {
    console.log("name", e.target.name);
    console.log("value", e.target.value);
    /* 	const index = e.target.selectedIndex;
  	const el = e.target.childNodes[index] */
    //const option =  el.getAttribute('label');
    let check = data.categories.find((x) => e.target.value === x);
    console.log("check", check);
    if (check) {
      return;
    } else {
      setData({
        ...data,
        categories: [...data.categories, e.target.value],
      });
      setCategoriesSelected([
        ...categoriesSelected,
        { name: e.target.name, value: e.target.value },
      ]);
    }
  }

  function onDeleteSelected(e) {
    console.log("delete", e.target.value);
    setData({
      ...data,
      categories: data.categories.filter((x) => x !== e.target.value),
    });
    let filter = categoriesSelected.filter((x) => x.value !== e.target.value);
    console.log("filter", filter);
    setCategoriesSelected(filter);
  }

  function uploadImage(files) {
    console.log("files", files[0]);
    const formData = new FormData();
    formData.append("file", files[0]);
    formData.append("upload_preset", "azihkt5g");
    axios
      .post(
        "https://api.cloudinary.com/v1_1/proveedores/image/upload",
        formData
      )
      .then((response) => {
        console.log("response", response);
        setNewVariant({
          ...newVariant,
          image: response.data.url,
        });
      });
  }

  //ON SUBMIT

  function AddNewVariant() {
    setData({
      ...data,
      variants: [...data.variants, newVariant],
    });
    setNewVariant({
      id: data.variants.length + 1,
      variantType: "",
      quantity: 0,
      price: 0,
      description: "",
      brand: "",
      color: "",
    });
    setAdd(!add);
  }

  function deleteVariant(id) {
    let filtered = data.variants.filter((variant) => variant.id !== id);
    setData({
      ...data,
      variants: filtered,
    });
  }

  function showImage(image) {
    setImage(image);
    setModalImage(!modalImage);
  }

  async function onSubmit() {
    setProcessing(!processing);
    let body = JSON.stringify(data);
    let response = await getPosts(body);

    console.log("create", response);
    setProcessing(!processing);
    setFinished(!finished);
  }

  function openModalSubmit() {
    checkEmpty();
    setModalSubmit(!modalSubmit);
  }

  return (
    <>
      {(modal || modalSubmit || modalImage) && <ModalOpen></ModalOpen>}
      <Wrapper>
        <TitleContainer>
          <Company>Crear Publicación</Company>
        </TitleContainer>
        <Subtitle data-aos="zoom-in-up" data-aos-duration="1500">
          Cotizacion
        </Subtitle>
        <InputWrapper>
          <InputContainer data-aos="fade-right" data-aos-duration="1500">
            <InputTitle>Tipo de Publicacion *</InputTitle>
            <Switch
              isToggled={isToggledPdc}
              onToggle={() => onTogglePdc()}
              on={"Comprar"}
              off={"Vender"}
            />
          </InputContainer>

          <InputContainer data-aos="fade-left" data-aos-duration="1500">
            <InputTitle>Forma de Pago *</InputTitle>
            <SelectData name="paymentMethod" onChange={(e)=> onChangeQuote(e)}>
                  <SelectOption>Seleccionar</SelectOption>
                  <SelectOption>Efectivo</SelectOption>
                  <SelectOption>Cheque</SelectOption>
                  <SelectOption>Transferencia Bancaria</SelectOption>
                </SelectData>
          </InputContainer>
          <InputContainer data-aos="fade-left" data-aos-duration="1500">
            <InputTitle>Observaciones</InputTitle>
            <Input
              onChange={(e) => onChangeQuote(e)}
              name="notes"
              type={"text"}
              value={data.quote?.notes}
            />
          </InputContainer>
        </InputWrapper>
        {!first && (
          <Next
            
            onClick={() => setfirst(true)}
          >
            <a href="#propuesta">
              <img
                style={{ width: "38px", height: "38px", cursor: "pointer" }}
                src="/img/next.png"
              />
            </a>
          </Next>
        )}
        {first && (
          <div
            style={{
              marginBottom: "200px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div id="propuesta" style={{ height: "50px" }}></div>
            <Subtitle data-aos="zoom-in-up" data-aos-duration="1500">
              Propuesta
            </Subtitle>
            <InputWrapper>
              <InputContainer data-aos="fade-right" data-aos-duration="1500">
                <InputTitle>Incluye flete *</InputTitle>
                <Switch
                  isToggled={isToggledFlete}
                  onToggle={() => onToggleFlete()}
                  on={"No incluye"}
                  off={"Incluye"}
                />
              </InputContainer>
              <InputContainer data-aos="fade-right" data-aos-duration="1500">
                <InputTitle>Facturación mínima *</InputTitle>
                <SelectData name="minimum" onChange={(e)=>onChangeProposal(e)}>
                  <SelectOption value={'Sin monto minimo'}>Sin monto mínimo</SelectOption>
                  <SelectOption value={'50000'}>$ 50.000</SelectOption>
                  <SelectOption value={'75000'}>$ 75.000</SelectOption>
                  <SelectOption value={'100000'}>$ 100.000</SelectOption>
                  <SelectOption value={'125000'}>$ 125.000</SelectOption>
                  <SelectOption value={'150000'}>$ 150.000</SelectOption>
                  <SelectOption value={'175000'}>$ 175.000</SelectOption>
                  <SelectOption value={'200000'}>$ 200.000 o más</SelectOption>
                </SelectData>
              </InputContainer>
              <InputContainer data-aos="fade-left" data-aos-duration="1500">
                <InputTitle>Validez de la propuesta *</InputTitle>
                <Input type={'date'} name='endDate' value={data.proposal?.endDate} onChange={(e)=>onChangeProposal(e)}/>
              </InputContainer>

              <InputContainer data-aos="fade-left" data-aos-duration="1500">
                <InputTitle>Plazo de entrega *</InputTitle>
                <Input
                  onChange={(e) => onChangeProposal(e)}
                  name="shippingInterval"
                  value={data.proposal?.shippingInterval}
                />
              </InputContainer>

              <InputContainer data-aos="fade-right" data-aos-duration="1500">
                <InputTitle>Condiciones de pago *</InputTitle>
                <Input />
              </InputContainer>
              <InputContainer data-aos="fade-left" data-aos-duration="1500">
                <InputTitle>Observaciones</InputTitle>
                <Input
                  onChange={(e) => onChangeProposal(e)}
                  name="notes"
                  value={data.proposal?.notes}
                />
              </InputContainer>
            </InputWrapper>
            {!second && (
              <Next
                data-aos="fade-right"
                data-aos-duration="1500"
                onClick={() => setsecond(true)}
              >
                <a href="#contacto">
                  <img
                    style={{ width: "38px", height: "38px", cursor: "pointer" }}
                    src="/img/next.png"
                  />
                </a>
              </Next>
            )}
          </div>
        )}
        {second && (
          <div
            style={{
              marginBottom: "50px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              width: "100%",
            }}
          >
            <div id="contacto" style={{ height: "90px" }}></div>
            <Subtitle data-aos="zoom-in-up" data-aos-duration="1500">
              Contacto
            </Subtitle>
            <InputWrapper>
              <InputContainer data-aos="fade-right" data-aos-duration="1500">
                <InputTitle>Dirección de entrega *</InputTitle>
                <Input
                  onChange={(e) => onChangeContact(e)}
                  name="sellingAddress"
                  value={data.contact?.sellingAddress}
                />
              </InputContainer>

              <InputContainer data-aos="fade-left" data-aos-duration="1500">
                <InputTitle>Contacto del responsable *</InputTitle>
                <Input
                  onChange={(e) => onChangeContact(e)}
                  name="contactName"
                  value={data.contact?.contactName}
                />
              </InputContainer>
            </InputWrapper>
            {/* {
          !third &&
        <Next data-aos='fade-right' data-aos-duration='1500' onClick={()=>setthird(true)}><a href='#productos'><img  style={{width: '38px', height: '38px', cursor: 'pointer'}} src='/img/next.png'/></a></Next>
        } */}
          </div>
        )}

        {second && (
          <>
            <div style={{ height: "0px" }}></div>
            <Subtitle
              id="productos"
              data-aos="fade-right"
              data-aos-duration="1500"
              style={{ marginTop: "50px" }}
            >
              {" "}
              Productos
            </Subtitle>
            <Products>
              {data.variants.length
                ? data.variants.map((x) => {
                    return (
                      <>
                        <ProductsWrapper>
                          <button
                            onClick={() => deleteVariant(x.id)}
                            style={{
                              background: "none",
                              border: "none",
                              cursor: "pointer",
                              marginBottom: "20px",
                            }}
                          >
                            <img src={"/img/delete.png"}></img>
                          </button>
                          <ProductsContainer>
                            <ParagraphColumn>
                              <ParagraphProducts>
                                Nombre: {x.variantType}
                              </ParagraphProducts>

                              <ParagraphProducts>
                                Cantidad: {x.quantity}
                              </ParagraphProducts>

                              <ParagraphProducts>
                                Precio: {x.price}
                              </ParagraphProducts>
                            </ParagraphColumn>
                            <ParagraphColumn>
                              <ParagraphProducts>
                                Marca: {x.brand}
                              </ParagraphProducts>

                              <ParagraphProducts>
                                Color: {x.color}
                              </ParagraphProducts>

                              <ParagraphProducts>
                                Descripción: {x.description}
                              </ParagraphProducts>
                            </ParagraphColumn>
                          </ProductsContainer>
                          <button onClick={() => showImage(x.image)}>
                            <img src={"/img/imageUpload.png"} />
                          </button>
                        </ProductsWrapper>
                      </>
                    );
                  })
                : null}
            </Products>
            {!add && (
              <AddNew
                data-aos="fade-left"
                data-aos-duration="1500"
                onClick={() => setAdd(!add)}
              >
                Añadir producto +
              </AddNew>
            )}
            {add && (
              <Options>
                <OptionBox>
                  <InputWrapper>
                    <InputContainer>
                      <InputTitle>Tipo *</InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="variantType"
                        value={newVariant.variantType}
                      />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Cantidad *</InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="quantity"
                        type={"text"}
                        value={newVariant.quantity}
                      />
                    </InputContainer>

                    <InputContainer>
                      <InputTitle>Precio * </InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="price"
                        type={"text"}
                        value={newVariant.price}
                      />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Unidad </InputTitle>
                      <OptionSelect
                        onChange={(e) => onChangeVariants(e)}
                        name="unit"
                        type={"text"}
                      >
                        <option value="kg">Kilogramos</option>
                        <option value="m">Metros</option>
                        <option value="u">Unidades</option>
                      </OptionSelect>
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Marca </InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="brand"
                        type={"text"}
                        value={newVariant.brand}
                      />
                    </InputContainer>

                    <InputContainer style={{ width: "90%", margin: "auto" }}>
                      <InputTitle>Descripción </InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="description"
                        type={"text"}
                        value={newVariant.description}
                      />
                    </InputContainer>

                    <InputContainer>
                      <InputTitle>Color</InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="color"
                        type={"text"}
                        value={newVariant.color}
                      />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Imagen *</InputTitle>
                      <OptionInput
                        type="file"
                        onChange={(e) => uploadImage(e.target.files)}
                      />
                    </InputContainer>
                  </InputWrapper>
                  <AddButton
                    disabled={newVariant.image?.length <= 0}
                    onClick={() => AddNewVariant()}
                  >
                    Agregar
                  </AddButton>
                </OptionBox>
              </Options>
            )}
            {!four && (
              <Next
                onClick={() => setfour(true)}
              >
                <a href="#categorias">
                  <img
                    style={{ width: "38px", height: "38px", cursor: "pointer" }}
                    src="/img/next.png"
                  />
                </a>
              </Next>
            )}
          </>
        )}

        {four && (
          <>
            <div id="categorias" style={{ height: "50px" }}></div>
            <Subtitle data-aos="fade-right" data-aos-duration="1500">
              Categorias
            </Subtitle>
            <CategoriesContainer data-aos="flip-up" data-aos-duration="1500">
              <p
                style={{ display: "flex", gap: "10px", alignItems: "center" }}
                onClick={() => {
                  setrenderCategories(!renderCategories);
                }}
              >
                Seleccionar
                {renderCategories ? (
                  <img src="/img/arrowUp.png" />
                ) : (
                  <img src="/img/arrowDown.png" />
                )}
              </p>

              <SelectCategories renderCategories={renderCategories}>
                {categories.map((x, i) => {
                  return (
                    <div key={i}>
                      <CategoryOption
                        onClick={(e) => onSelectCategory(e)}
                        name={x.name}
                        key={x._id}
                        value={x._id}
                      >
                        {x.name}
                      </CategoryOption>
                    </div>
                  );
                })}
              </SelectCategories>
              <SelectedContainer>
                {categoriesSelected.length
                  ? categoriesSelected.map((cat) => {
                      return (
                        <Selected key={cat.name}>
                          {cat.name}{" "}
                          <SelectedDelete
                            onClick={(e) => onDeleteSelected(e)}
                            value={cat.value}
                          >
                            x
                          </SelectedDelete>
                        </Selected>
                      );
                    })
                  : null}
              </SelectedContainer>
            </CategoriesContainer>
          </>
        )}

        {four && (
          <ButtonContainers>
            <div >
              <ButtonDelete onClick={() => setModal(!modal)}>
                Cancelar
              </ButtonDelete>
            </div>
            <div >
              <ButtonSave onClick={() => openModalSubmit()}>
                Crear Publicacion
              </ButtonSave>
            </div>
          </ButtonContainers>
        )}
      </Wrapper>
      {modal && (
        <Modal>
          <ModalTitle>Cancelar</ModalTitle>
          <ModalParagraph>
            Seguro que desea salir? Sus cambios no se guardarán
          </ModalParagraph>
          <ModalButtonsContainer>
            <ButtonDeleteModal onClick={() => setModal(!modal)}>
              Cancelar
            </ButtonDeleteModal>
            <ButtonSaveModal onClick={() => (window.location.href = "/")}>
              Salir
            </ButtonSaveModal>
          </ModalButtonsContainer>
        </Modal>
      )}
      {modalSubmit && (
        <Modal>
          {error ? (
            <>
              <ModalTitle>Crear publicación</ModalTitle>
              <ModalParagraph>
                Faltan datos por completar. Por favor, revise el formulario.{" "}
              </ModalParagraph>
              <ModalButtonsContainer style={{ justifyContent: "center" }}>
                <ButtonSaveModal onClick={() => setModalSubmit(!modalSubmit)}>
                  Volver
                </ButtonSaveModal>
              </ModalButtonsContainer>
            </>
          ) : !finished ? (
            <>
              <ModalTitle>Crear publicación</ModalTitle>
              <ModalParagraph>
                Se creará la publicación con los datos ingresados. Desea
                continuar ?{" "}
              </ModalParagraph>
              <ModalButtonsContainer>
                <ButtonDeleteModal onClick={() => setModalSubmit(!modalSubmit)}>
                  Cancelar
                </ButtonDeleteModal>
                <ButtonSaveModal disabled={error} onClick={() => onSubmit()}>
                  Continuar
                </ButtonSaveModal>
              </ModalButtonsContainer>
            </>
          ) : (
            finished && (
              <>
                <ModalTitle>Crear publicación</ModalTitle>
                <ModalParagraph>Publicacion creada con éxito! </ModalParagraph>
                <ModalButtonsContainer style={{ justifyContent: "center" }}>
                  <ButtonSaveModal onClick={() => (window.location.href = "/")}>
                    Continuar
                  </ButtonSaveModal>
                </ModalButtonsContainer>
              </>
            )
          )}
        </Modal>
      )}

      {modalImage && (
        <Modal>
          <ModalTitle>Imagen</ModalTitle>
          <ModalImageContainer>
            <ModalImage src={image ? image : ""} />
          </ModalImageContainer>
          <ModalButtonsContainer style={{ justifyContent: "center" }}>
            <ButtonSaveModal
              onClick={() => {
                setModalImage(!modalImage);
              }}
            >
              Salir
            </ButtonSaveModal>
          </ModalButtonsContainer>
        </Modal>
      )}
    </>
  );
};

export default CreatePost;
