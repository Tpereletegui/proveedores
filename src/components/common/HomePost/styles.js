import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  scroll-snap-align: start;
`
export const Wrapper = styled.div`
  width: 90%;
  height: max-content;
  background-color: white;
  scroll-snap-align: start;

  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
  border-radius: 14px;
  @media (max-width: 1024px) {
  width: 90%;
  }
  @media  (min-width: 1624px) {
  width: 1400px;
  }
  

`

export const Main = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 60%;
 `

export const SubtitleWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  width: 100%;
  margin: auto;
  margin-left: 12%;
`

export const Subtitle = styled.p`
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 22px;
  color: #476ADE;
 
`

export const Info = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-direction: row;
  width: 100%;
  height: max-content;
  
 
  
  @media (max-width: 1024px) {
  flex-direction: column;
  justify-content: center;
  }
`

export const BoxContainer = styled.div`
  display: flex;
  justify-content: center ;
  align-items: center;
  flex-direction: column;
  width: 36%;
  height: max-content;
  @media (max-width: 1024px) {
  width: 100%;
  
  }
  
`
export const BoxContainer2 = styled.div`
  display: flex;
  justify-content: center ;
  align-items: center;
  flex-direction: column;
  width: 36%;
  height: max-content;
  @media (max-width: 1024px) {
  width: 100%;
  margin-bottom: 100px;
  }
  
`
export const BoxContainer3 = styled.div`
  display: flex;
  justify-content: center ;
  align-items: center;
  flex-direction: column;
  width: 24%;
  height: max-content;
  @media (max-width: 1024px) {
  width: 100%;
  }
  
`

export const Box1 = styled.div`
  min-height: 350px;
  max-height: max-content;
  padding: 30px;
 
 
  
  
  
`
export const Box2 = styled.div`
  
  max-height: max-content;
  border-right: 2px solid black;
  border-left: 2px solid black;
  padding: 30px;
  
  padding-bottom: 0px;
  padding-right: 0px;
  @media (max-width: 1024px) {
  border: none;
  }
  
`
export const Box3 = styled.div`
   min-height: 350px;
   max-height: max-content;
   display: flex;
   justify-content: flex-start;
   align-items: center;
   flex-direction: column;
   padding: 30px;
   
   

 
`





export const BoxContent = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: row;
  height: 100%;
  margin-left: 3%;
  width: 100%;
  @media (max-width: 1024px) {
  margin: auto;
  align-items: center;
  justify-content: center;
  
  }
`

export const BoxColumn = styled.div`
display: flex;
justify-content: flex-start;
align-items: flex-start;
flex-direction: column;
height: 100%;
width: 50%;
margin-right: 7%;
@media (max-width: 1024px) {
  margin: auto;
  
  
  }


`
export const BoxColumnTitle = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 25px;
  margin-bottom: 15px;
  width: 90%;
  color: black;
`
export const BoxColumnText = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  margin-bottom: 15px;
  width: 100%;
  
  color: rgba(0, 0, 0, 0.6);
`
export const Date = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: rgba(0, 0, 0, 0.6);
  margin-right: 3%;
  @media (max-width: 1024px) {
  
  margin-right: 0%;
  }
`

export const Title = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  margin-bottom: 3%;
  padding: 15px;
  @media (max-width: 1024px) {
  flex-direction: column;
  margin-bottom: 35px;
  
  }
`


export const NameCompany = styled.p`
  color: black;
  font-size: 20px;
  margin-left: 3%;
  @media (max-width: 1024px) {
  margin-left: 0px;
  margin-bottom: 15px;
  text-align: center;
  
  }
  
 
  

`

export const Category = styled.span`
  color: white;
  font-size: 20px;
  background-color: #476ADE;
  padding: 5px;
  border-radius: 8px;
  @media (max-width: 1024px) {
  display: block;
  margin-top: 15px;
  
  }
  
`


export const Footer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: row;
  margin-left: 1%;
  padding: 20px;
  padding-top: 0px;
  @media (max-width: 1024px) {
  flex-direction: column-reverse;
  margin-top: 30px;
  }

`

export const Buttons = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #333333;
  background-color: transparent;
  border: 1px solid #476ADE;
  box-sizing: border-box;
  border-radius: 8px;
  width: max-content;
  height: 29px;
  margin-right: 3%;
  text-align: center;
  padding: 5px;
  @media (max-width: 1024px) {
  margin: auto;
  margin-bottom: 15px;
  width: 190px;
  margin-top: 10px;
  }
`


export const Products = styled.p`
  width: 175px;
  height: 29px;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #333333;
  text-align: center;
  padding: 5px;
  @media (max-width: 1024px) {
  
  margin: auto;
  }
  
`


export const Options = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: column;

`
export const OptionBox = styled.div`
  display: flex;
  padding: 10px;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  background: #FFFFFF;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
  border-radius: 14px;
  width: 90%;
  min-height: 50px;
  height: max-content;
  margin-top: 1%; 
  
  @media (max-width: 1024px) {
  flex-wrap: nowrap;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
  height: max-content;
  width: 90%;
  margin-bottom: 10px;
  margin-top: 10px;
  padding: 50px;
    }
  @media (max-width: 412px) {
  flex-wrap: nowrap;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
  height: max-content;
  width: 90%;
  margin-bottom: 10px;
  margin-top: 10px;
  padding: 10px;
  }
  @media  (min-width: 1624px) {
  width: 1400px;
  }

`


export const OptionText = styled.div`
  display: flex ;
  justify-content: flex-start;
  align-items: center;
  flex-direction: row;
  margin-right: 20px;
 
  
  &:nth-child(2){   
    width: 170px;
  }
  &:nth-child(3){    
    width: 250px;
  }
  &:nth-child(4){  
    width: 200px;
  }
  &:nth-child(5){
    width: 350px;
  }
  &:nth-child(6){  
    width: 120px 
  }
  &:nth-child(7){ 
    width: 120px;
  }
  
  
  @media (max-width: 1024px) {
    margin-bottom: 15px;
    margin-left: 20%;
    &:nth-child(2){
      width: 50%;
    }
    &:nth-child(5){
      width: 90%;
    }
    &:nth-child(3){
     width: 70%;
    }
    &:nth-child(4){
      width: 70%;
    }
    &:nth-child(6){
      width: 70%;
    }
    &:nth-child(7){
      width: 70%;
    }
    
    
  }
 
  
`
export const OptionP = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  width: auto;
  color: rgba(0, 0, 0, 0.6);
  padding: 5px;
  margin: 0px;
`
export const OptionTitle = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 25px;
  color: black;
  padding: 5px;
`

export const Input = styled.input`
 -webkit-appearance: none;
	appearance: none;
	background: #ffffff;
	border: 1px solid #828282;
	border-radius: 6px;
	width: 16px;
	height: 16px;
  
	cursor: pointer;
  margin-right: 15px;
  

	&:checked {
		border: 5px solid #476ade;
	}
  @media (max-width: 1024px) {
  margin-right: 90%;
  margin-bottom: 15px;
 
  }


`

export const OptionImg = styled.img`
  margin-top: 10px;
   @media (max-width: 1024px) {
    margin-top: 20px;
    margin-left: 45%;
  }
`

