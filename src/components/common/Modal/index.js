import { Overlay, ContenedorModal, EncabezadoModal, BotonCerrar } from './styles';


const Modal = ({ children, estado }) => {
	return (
		<>

			<Overlay>
				<ContenedorModal>
					{children}
				</ContenedorModal>
			</Overlay>

		</>
	);
}
export default Modal;