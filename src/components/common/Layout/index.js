import Navbar from '../Navbar'
import { NavMobile } from '../NavMobile'
import { Container, Main } from './styles'
import { useState, useEffect } from 'react'
import React from 'react'
import { io } from 'socket.io-client'
import Notification from '../Notification'
import LandingSection from '../../../sections/LadingSection/LandingSection'


const Layout = ({ children, heightContainer }) => {
	const [mobile, setMobile] = useState(false)
	const [msg, setMsg] = useState();
	/* 	const [login, setLogin] = useState(null)
	
		useEffect(() => {
			setLogin(localStorage.getItem('loggedUser'))
		}, []) */

	const handleMobile = () => {
		setMobile(!mobile)
	}

	// Notificaciones
	const [token, setToken] = useState();
	const [socket, setSocket] = useState();
	useEffect(() => {
		setToken(localStorage.getItem('IdToken'))
	}, [])
	useEffect(() => {
		if (token) {
			const newSocket = io("http://localhost:3001", {
				auth: { token },
				autoConnect: true,
			});
			setSocket(newSocket)
			return () => newSocket.close()
		}
	}, [token])
	useEffect(() => {
		socket?.on("private message", (message) => {
			console.log("private message event + message", message)
			setMsg(message);
		})
	}, [socket])

	const childrenWithProps = React.Children.map(children, child => {
		// Checking isValidElement is the safe way and avoids a typescript
		// error too.
		if (React.isValidElement(child)) {
			return React.cloneElement(child, { socket });
		}
		return child;
	});

	const closeNotification = () => {
		setMsg(undefined);
	}



	return (<Container heightContainer={heightContainer}>
		<NavMobile open={mobile} handleMobile={handleMobile}></NavMobile>
		{token && <Navbar open={mobile} handleMobile={handleMobile} />}
		<Main>{childrenWithProps}</Main>
		<Notification msg={msg} closeNotification={closeNotification} />
	</Container>)
}

export default Layout
