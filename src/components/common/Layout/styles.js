import styled from 'styled-components'

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	height: ${({ heightContainer }) => heightContainer};
	position: relative;
	overflow-x: hidden;
`

export const Main = styled.main`
	padding-top: 77px;
	width: 100%;
	height: 100%;
	background: #f8f8f8;
`
