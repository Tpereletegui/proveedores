import styled from 'styled-components'

export const Container = styled.div`
	width: 100%;
	height: ${({ height }) => height};
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;
	gap: 18px;
	margin: 0 auto;
`

export const Welcome = styled.div`
	display: flex;
	flex-direction: column;
	gap: 10px;
	margin-left: 20px;
	margin-top: 20px;
`

export const Title = styled.p`
	font-weight: 500;
	font-size: 30px;
	color: #476ade;
	letter-spacing: -0.408px;
	text-align: center;
	
`

export const Description = styled.p`
	font-weight: 500;
	font-size: 15px;
	letter-spacing: -0.408px;
	color: #000000;
	text-align: center;
`

export const TableCategory = styled.table`
	width: min(90%, 900px);
	height: fit-content;
	font-size: 18px;
	display: flex;
	flex-direction: column;
	margin: 0;
	@media screen and (max-width: 1000px) {
		font-size: 14px;
	}
`

export const TheadCategory = styled.thead`
	width: 100%;
	height: 45px;
	font-size: 1em;
	display: flex;
	border-top: 2px solid #ccc;
	border-bottom: 2px solid #ccc;
`

export const ColHead = styled.td`
	height: 45px;
	display: flex;
	justify-content: center;
	align-items: center;
	font-weight: normal;
	font-size: 1em;
	color: #000000;
	letter-spacing: -0.408px;
	&:nth-child(1) {
		width: 40%;
	}
	&:nth-child(2) {
		width: 20%;
	}
	&:nth-child(3) {
		width: 40%;
	}
`

export const TbodyCategory = styled.tbody`
	width: 100%;
	height: max-content;
	font-size: 1em;
	display: flex;
	flex-direction: column;
	overflow-y: auto;
	margin-top: 12px;

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 3px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 20px;
	}
`

export const TrCategory = styled.tr`
	width: 100%;
	min-height: 43px;
	display: flex;
	font-weight: normal;
	font-size: 0.89em; /*16px*/
	letter-spacing: -0.408px;
	color: #828282;
`

export const TdCategory = styled.tbody`
	height: 100%;
	font-size: 1em;
	display: flex;
	justify-content: center;
	align-items: center;
	text-align: center;

	&:nth-child(1) {
		width: 40%;
	}
	&:nth-child(2) {
		width: 20%;
	}
	&:nth-child(3) {
		width: 40%;
	}
`
export const Form=styled.form`
	width: 100%;
	justify-content: center;
	align-items: center;
	display: flex;
	flex-direction: column;
`
export const OtherDiv=styled.div`
	font-weight: normal;
	font-size: 0.89em; /*16px*/
	letter-spacing: -0.408px;
	color: #828282;
	position: relative;
	left: 130px;
	width: max-content;
	display: flex;
	margin-bottom: 3px;
	@media (max-width:1024px){
		left: 30px;
	}
`
export const Text = styled.p``
export const Other = styled.input`
	height: 27px;
	background: #ffffff;
	border: 1px solid #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
	outline: none;
	text-align: center;
`
export const Radius = styled.input`
	font-size: 1em;
	-webkit-appearance: none;
	appearance: none;
	background: #ffffff;
	border: 1px solid #828282;
	border-radius: 6px;
	width: 1em;
	height: 1em;
	cursor: pointer;

	&:checked {
		border: 5px solid #476ade;
	}
`

export const Send = styled.button`
	width: 112px;
	height: 40px;
	background: #476ade;
	border: 1px solid #476ade;
	border-radius: 8px;
	font-weight: 500;
	font-size: 14px;
	letter-spacing: -0.408px;
	align-self: center;
	color: #ffffff;
	margin-top: 20px;
	cursor: pointer;
`