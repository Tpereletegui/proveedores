import axios from 'axios';
import { useState,useEffect,useRef } from 'react'

import { useForm } from "react-hook-form";
import useFetch from '../../../hooks/useFetch'
import {
	Container,
	Welcome,
	Title,
	Description,
	TableCategory,
	TheadCategory,
	TbodyCategory,
	ColHead,
	TdCategory,
	Text,
	TrCategory,
	Radius,
	Send,
	Form,
	Other,
	OtherDiv,
} from './styles'

const WelcomeCategory = ({ welcome }) => {
	const { register, handleSubmit, watch, formState: { errors } } = useForm({mode:"all"});
	const [provee, setProvee] = useState([])
	const [consume, setConsume] = useState([])
	const [userId, setUserId] =useState(undefined);
		// { nombre: 'Vechiculos', provee: false, consume: false },
		// { nombre: 'Inmuebles', provee: false, consume: false },
		// { nombre: 'Tecnologia', provee: false, consume: false },
		// { nombre: 'Hogar y Muebles', provee: false, consume: false },
		// { nombre: 'Electrodomesticos', provee: false, consume: false },
		// { nombre: 'Herramientas', provee: false, consume: false },
		// { nombre: 'Construccion', provee: false, consume: false },
		// { nombre: 'Deportes y Fitness', provee: false, consume: false },
		// { nombre: 'Accesorios para vehiculos', provee: false, consume: false },
		// { nombre: 'Moda', provee: false, consume: false },
		// { nombre: 'Juegos y Juguetes', provee: false, consume: false },

	/*
	const handleClick = (index, type) => {
		setCategorias(
			[...categorias],
			(categorias[index][type] = !categorias[index][type])
		)
	}
	*/
	useEffect(()=> {
		async function getId(){
		  let local = localStorage.getItem('loggedUser');
		  if(local){
		  console.log('local', local)
		  let localParse = JSON.parse(local)
		  console.log('localParse', localParse)
		  setUserId(localParse.id); 
		  }   
	  }
		getId();
	  },[])

	let { data,loading } = useFetch('http://localhost:3000/api/category', 'get')
	let length=data.length
	let handleClick = (value,type) => {
		// const copy = [...data]
		// console.log("copy",copy);
		// copy[index][type] = !copy[index][type]
		// setCategorias(copy)
		const categoryCheck=e => e.name ===value
		if(type=="provee"){
			let index= provee.findIndex(object =>{
				return object.name===value
			})
			index==-1?provee.push({name:value,type:"provee"}):provee.splice(index,1);
			console.log("value",value);
			console.log("index",index);
			console.log("provee",provee);
		}
		else if(type=="consume"){
			let index= consume.findIndex(object =>{
				return object.name=== value
			})
			index==-1?consume.push({name:value,type:"consume"}):consume.splice(index,1);
			console.log("consume",consume);
		}
	}

	
	let onSubmit=async(input)=>{
		let interestedCategories={interestedCategories:provee.concat(consume)}
		console.log("data",JSON.stringify(interestedCategories));
		const response = await axios.put(`http://localhost:3000/api/user?id=${userId}`,interestedCategories)

		console.log(response);

	}

	let input=watch("other")

	return (
		<Container height={welcome ? '100vh' : '100%'}>
			<Welcome>
				<Title>
					{welcome
						? 'Bienvenido a Compratodo'
						: 'Editar categorias preferidas'}
				</Title>
				<Description>
					Seleccione las categorias de productos que mejor se adapten
					al perfil de su empresa
				</Description>
			</Welcome>
			<Form onSubmit={handleSubmit(onSubmit)}>
			<TableCategory>
				<TheadCategory>
					{['Categoria', 'Provee', 'Consume'].map((col, key) => (
						<ColHead key={key}>{col}</ColHead>
					))}
				</TheadCategory>
				
					<TbodyCategory>
					{data.map((categoria, index) => (
						<TrCategory key={index}>
							<TdCategory>
								<Text>{categoria.name}</Text>
							</TdCategory>

							<TdCategory>
								<Radius
									checked={data[index]['provee']}
									type='checkbox'
									value={categoria.name}
									onClick={() => {
										handleClick(categoria.name, "provee")
									}}
								/>
							</TdCategory>
							<TdCategory>
								<Radius
									checked={data[index]['consume']}
									type='checkbox'
									value={categoria.name}
									onClick={() => {
										handleClick(categoria.name, 'consume')
									}}
								/>
							</TdCategory>
						</TrCategory>
					)
					)}
				</TbodyCategory>
				<TbodyCategory>
					<TrCategory>
						<TdCategory>
							<Other type="text" placeholder="Otra categoria"name="other" {...register("other")}/>
						</TdCategory>
						<TdCategory>
									<Radius
										//checked={data[length-1]['provee']}
										type='checkbox'
										value={input}
										onClick={() => {
											handleClick(input, "provee")
										}}
									/>
								</TdCategory>
								<TdCategory>
									<Radius
										//checked={data[length-1]['consume']}
										type='checkbox'
										value={input}
										onClick={() => {
											handleClick(input, 'consume')
										}}
									/>
							</TdCategory>
					</TrCategory>
				</TbodyCategory>					
			</TableCategory>
			<Send type='submit'>Enviar</Send>
			</Form>	
		</Container>
	)
}
export default WelcomeCategory