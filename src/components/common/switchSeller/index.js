import React, {useState} from 'react';
import styles from './switch.module.css';
import { useForm } from "react-hook-form";

const Switch = ({isToggled, onToggle, on, off}) => {
const [state, setState] =useState('Proveedor')
const {register} = useForm();
  return (
    
    <label className={styles.switch}>
      <input {...register("userType")} type='checkbox' id='switch' name='userType' checked={isToggled} onChange={onToggle}></input>
      <span className={styles.slider}></span>
      <p className={isToggled ? styles.pcomprar : styles.pcompraractive}>{on}</p>
      <p className={isToggled ? styles.pvenderactive : styles.pvender}>{off}</p>
    </label> 
   
  )
}

export default Switch;
