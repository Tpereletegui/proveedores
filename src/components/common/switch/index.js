
import React, {useState} from 'react';
import styles from './switch.module.css';


const Switch = ({isToggled, onToggle, on, off}) => {
const [state, setState] =useState('Comprador')

  return (
    
    <label className={styles.switch}>
      <input type='checkbox'  checked={isToggled} onChange={onToggle}></input>
      <span className={styles.slider}></span>
      <p className={isToggled ? styles.pcomprar : styles.pcompraractive}>{on}</p>
      <p className={isToggled ? styles.pvenderactive : styles.pvender}>{off}</p>
    </label> 
   
  )
}

export default Switch;