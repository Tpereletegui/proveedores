import styled from "styled-components";

export const TitleText = styled.h2`
  color: black;
  font-size: 18px;
  font-weight: 500;
  margin-top: 30px;
  margin-left: 30px;
`

export const Blob = styled.h3`
  background: #4182BD;
  padding: 5px 10px 5px 10px;
  border-radius: 10px;
  color: white;
  font-size: 16px;
  font-weight: 700px;
  width: fit-content;
  margin-top: 30px;
  margin-left: 20px;
`

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;

`