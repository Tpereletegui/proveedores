import React from 'react';
import { TitleContainer, TitleText, Blob } from './styles';

const Title = ({ title, blob }) => {
  return (
    <TitleContainer>
      <TitleText>{title}</TitleText>
      {blob ? <Blob>{blob}</Blob> : ''}
    </TitleContainer>
  )
}

export default Title;