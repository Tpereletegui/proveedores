import styled from 'styled-components'

import Link from 'next/link'
const Container = styled.nav`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	align-items: center;
	position: fixed;
	right: ${({ open }) => (open ? '0' : '-100%')};
	transition: 0.5s right ease;
	background-color: #1f1f1f;
	padding: 12px 18px;
	z-index: 9999;
`

const LinkMobile = styled(Link)`
	width: ${({ width }) => width};
	display: flex;
	align-items: center;
	font-size: 1em;
	font-weight: 500;
	letter-spacing: 1px;
	color: #f9f9f9;
	cursor: pointer;
	padding: 7.5px 16px;

	&:hover {
		background-color: #333333;
		border-radius: 4px;
	}
	border: 1px solid red;
`

const NavMobileHeader = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-between;
	align-items: center;
	margin-bottom: 20px;
`

const LinksContainer = styled.div`
	width: 100%;
	height: 700px;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	gap: 20px;
`

const Enlace = styled.a`
	font-size: 25px;
	color: #fff;
`

export const NavMobile = ({ open, handleMobile }) => {
	return (
		<Container open={open}>
			<NavMobileHeader>
				<svg
					style={{ marginLeft: 'auto' }}
					onClick={() => handleMobile(false)}
					width='48px'
					heigth='48px'
					
					className=''
					viewBox='0 0 48 48'
				>
					<title>burgerNavClose</title>
					<path
						d='M0 18A18 18 0 0118 0h12a18 18 0 0118 18v12a18 18 0 01-18 18H18A18 18 0 010 30z'
						fill='gray'
						fillOpacity='.2'
					></path>
					<path
						d='M31 18.41L29.59 17 24 22.59 18.4 17l-1.41 1.41L22.58 24l-5.59 5.59L18.4 31l5.59-5.59L29.58 31l1.41-1.41L25.4 24z'
						fill='#fcfcfc'
					></path>
				</svg>
			</NavMobileHeader>

			<LinksContainer>
				<LinkMobile width={'100%'} href='/'>
					<Enlace>Inicio</Enlace>
				</LinkMobile>

				<LinkMobile width={'100%'} href='/tooffer'>
					<Enlace>Ofertar</Enlace>
				</LinkMobile>
				<LinkMobile width={'100%'} href='/chat'>
					<Enlace>Mensajes</Enlace>
				</LinkMobile>
				<LinkMobile width={'100%'} href='/ayuda'>
					<Enlace>Ayuda</Enlace>
				</LinkMobile>
				<LinkMobile width={'100%'} href='/offers'>
					<Enlace>Ofertas Propias</Enlace>
				</LinkMobile>
				<LinkMobile width={'100%'} href='/admin'>
					<Enlace>Administrador</Enlace>
				</LinkMobile>
			</LinksContainer>
		</Container>
	)
}
