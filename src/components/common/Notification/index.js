import React, { useState } from 'react'
import { useEffect } from 'react'
import { Close, Image, TextContainer, TextMessage, TextTitle, Wrapper } from './styles'
import axios from 'axios'

const Notification = ({ msg, closeNotification }) => {
  
  const [user, setUser] = useState(undefined);

  useEffect(()=>{
    if(msg){
      console.log(msg.sender, 'sender')
      async function a(){
        const newUser = await axios.get(`http://localhost:3000/api/user/${msg.sender}`)
        setUser(newUser.data.data)
      }
      a()
    }
  }, [msg])

  useEffect(()=>{
    const timer = setTimeout(()=>{
        closeNotification()
        console.log('abcdf')
    }, 3000)
    return () => clearTimeout(timer);
  }, [msg])

  return (
    <>
    {(msg && msg.sender !== JSON.parse(localStorage.getItem('loggedUser')).id) && <Wrapper>
      <Image src={'img/user.png'} onClick={() => window.location = 'chat'} />
      <TextContainer onClick={() => window.location = 'chat'}>
        <TextTitle>{user && user.username}</TextTitle>
        <TextMessage>{msg.text}</TextMessage>
      </TextContainer>
      <Close onClick={closeNotification}>x</Close>
    </Wrapper>}
    </>
  )
}

export default Notification