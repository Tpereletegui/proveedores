import React from 'react';
import { Wrapper, TitleWrapper, TitleText, TitleIcon, Info } from './styles';

const Accordion = ({ title, info, active }) => {
  return (
    <Wrapper>
      <TitleWrapper>
        <TitleText style={active ? {color: '#4182BD'} : {}}>{title}</TitleText>
        <TitleIcon src={active ? "/img/arrowUpAccordion.png" : "/img/arrowDownAccordion.png"} />
      </TitleWrapper>
      {active && <Info>{info}</Info>}
    </Wrapper>
  )
}

export default Accordion;