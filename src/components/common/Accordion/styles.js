import styled from 'styled-components'

export const Wrapper = styled.div`
	width: 100%;
	flex-direction: column;
	margin-bottom: 12px;
`

export const TitleWrapper = styled.div`
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: space-between;
	margin: 0;
`

export const TitleText = styled.p`
	font-size: 17px;
	font-weight: 700;
	color: #656565;
	margin: 0;
`

export const TitleIcon = styled.img``

export const Info = styled.p`
	font-size: 15px;
	color: #828282;
	margin: 6px 16px 0 0;
`
