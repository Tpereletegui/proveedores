import { Wrapper, LeftContainer, Icon, Logo, Burger } from "./styles";
import Search from "./local/Search";
import Links from "./local/Links";
import ProfileOptions from "./local/ProfileOptions";
import { useState, useEffect } from "react";
import axios from "axios";
import Link from "next/link";

const Navbar = ({ mobile, handleMobile }) => {
  const [renderProfileOptions, setRenderProfileOptions] = useState(false);
  const [search, setSearch] = useState("");
  const [user, setUser] = useState(false);
  const [isSeller, setIsSeller] = useState(false);
  const [usuario, setUsuario] = useState({})

  useEffect(() => {
    async function getId() {
      try {
        let local = await localStorage.getItem("loggedUser");
        let localParse = local ? JSON.parse(local) : false;
        if (local) {
          setUser(true);
          const response = await axios.get(
            `${process.env.VERCEL_URL}/api/user/${localParse.id}`
          );
          setUsuario(response.data.data);
          let seller = await axios.get(
            `${process.env.VERCEL_URL}/api/user/getseller?_id=${localParse.id}`
          );
          if (seller) {
            setIsSeller(true);
          }
        }
      } catch (error) {
        console.log(error);
      }
    }
    getId();
  }, [user, isSeller]);

  return (
    <>
      {renderProfileOptions ? <ProfileOptions /> : ""}
      <Wrapper>
        <LeftContainer>
          <img width='100' src="/img/logo2.0.png" alt="logo milux"></img>
          <Search width={"300px"} setSearch={setSearch} />
        </LeftContainer>
        <Links user={user ? user : ""} isSeller={isSeller} />
        <Burger onClick={() => handleMobile()}></Burger>
        {user ? (
          <Icon
            src={
              usuario?.image
                ? usuario.image
                : "https://simg.nicepng.com/png/small/202-2022264_usuario-annimo-usuario-annimo-user-icon-png-transparent.png"
            }
            onClick={() => {
              setRenderProfileOptions(!renderProfileOptions);
            }}
          />
        ) : (
          <Link href="/login">
            <a style={{ color: "#000009" }}>Ingresar</a>
          </Link>
        )}
      </Wrapper>
    </>
  );
};

export default Navbar;
