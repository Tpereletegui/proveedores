import styled from 'styled-components'

export const Wrapper = styled.div`
	/*width: 300px;*/
	display: flex;
	gap: 10px;
	height: 36px;
	border-radius: 10px;
	border: 1px solid #828282;
	display: flex;
	align-items: center;
	background: #fafafa;
	padding-left: 10px;
`

export const Icon = styled.img`
	width: 20px;
	height: 20px;
`

export const SearchBar = styled.input`
	width: 256px;
	height: 22px;
	background: #fafafa;
	border: none;
	font-size: 17px;
	outline: none;

	@media screen and (max-width: 669px) {
		width: 150px;
	}
`
