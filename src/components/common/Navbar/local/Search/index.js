import { Wrapper, SearchBar, Icon } from './styles'

const Search = ({ setSearch, width }) => {
	return (
		<Wrapper width={width}>
			<Icon src='img/searchIcon.png' />
			<SearchBar
				placeholder='Buscar'
				onChange={e => setSearch(e.target.value)}
			/>
		</Wrapper>
	)
}

export default Search
