import { Enlace, Wrapper, Button } from './styles'
import Link from 'next/link'

const ProfileOptions = () => {

	function logOut(){
		if (typeof window !== 'undefined') {
			// Perform localStorage action
			localStorage.clear();
			window.location.reload();
		}
	}

	return (
		<Wrapper>
			<Link href={'/profile'}>
				<Enlace>Mi Perfil</Enlace>
			</Link>
			<Link href={'/settings'}>
				<Enlace>Ajustes</Enlace>
			</Link>
			<Link href={'/'}>
				<Button onClick={()=>logOut()}>Cerrar Sesion</Button>
			</Link>
		</Wrapper>
	)
}

export default ProfileOptions
