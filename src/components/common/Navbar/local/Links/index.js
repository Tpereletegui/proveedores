import { Wrapper, Categories, Category,LinkContainer, Arrow, Enlace, Read } from './styles'
import { useState } from 'react'
import useFetch from '../../../../../hooks/useFetch'
import ActiveLink from '../../../../ActiveLink'

import Link from 'next/link'
import React from 'react'
import { useRouter } from 'next/router'

import { useCategoriesContext } from '../../../../../context/categoriesContext'
const Links = ({ user, isSeller }) => {
	const [renderCategories, setRenderCategories] = useState(false)

	const { handleCategory, category } = useCategoriesContext()

	const { data } = useFetch(`${process.env.VERCEL_URL}/api/category`, 'get')
	const router = useRouter()

	return (
		<>
		{/* 	<Wrapper>

				<ActiveLink  href={'/'} children={'Inicio'}></ActiveLink>
		<div style={{width:"8%"}}>

		</div>
				<p style={{ display: 'flex', gap: '10px', alignItems: 'center',margin:0,padding:0 }} onClick={() => { setRenderCategories(!renderCategories) }}>
					Categorias
					{renderCategories ? (
						<Arrow src='/img/arrowUp.png' />
					) : (
						<Arrow src='/img/arrowDown.png' />
					)}
				</p>
				<LinkContainer>
					<ActiveLink href={isSeller ? `/tooffer` : '/login'} >ofertar</ActiveLink>
					<ActiveLink href='/chat' >mensajes</ActiveLink>
					<ActiveLink href='/ayuda' >ayuda</ActiveLink>
					<ActiveLink href='/offers' >ofertas propias</ActiveLink>
					<ActiveLink href='/admin' >administrador</ActiveLink>
				</LinkContainer>
				
			</Wrapper>

			<Categories renderCategories={renderCategories}>
				{data.map((ca, i) => (
					<div key={i}
						onClick={() => {
							handleCategory(ca.name, ca._id)
						}}
					>
						<Link href={'/'}>
							<Category color={category.name === ca.name ? '#476ADE' : '#000000'}>{ca.name}</Category>
						</Link>
					</div>
				))}
			</Categories>
 */}
		</>
	)
}

export default Links
