import styled from 'styled-components'

export const Wrapper = styled.div`
	font-size: 17px;
	display: flex;
	align-items: center;

	position: relative;
	justify-content: center;
	z-index: 2;
	width: 65%;
	@media screen and (max-width: 1275px) {
		display: none;
	}

`
export const LinkContainer = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-evenly;
`
export const Read = styled.div`
	position: relative;
	top: -2px;
	left: 5px;
	width: 10px;
	height: 10px;
	border-radius: 50%;
	background: #ff3636;
`

export const Link = styled.a`
	color: #000;
	transition: 0.5s;
	&:hover {
		transform: scale(1.1);
	}
`

export const Categories = styled.div`
	position: fixed;
	left: 50%;
	top: 77px;
	transform: translate(-50%,0);
	padding: ${({ renderCategories }) => renderCategories ? '20px 10px' : '0px'};
	opacity: ${({ renderCategories }) => renderCategories ? '1' : '0'};
	overflow: hidden;
	transition: 0.2s all ease-out;
	background: #ffffff;
	box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.25);
	border-radius: 0px 0px 8px 8px;
	display: flex;
	flex-wrap: nowrap;
	align-items: center;
	justify-content: center;
	gap: 20px;
`
export const Category = styled.a`
	display: block;
	color: ${({ color }) => color};
	font-size: 15px;
	transition: 0.3s all ease-out;

	&:hover {
		color: #476ADE;
		transform: scale(1.1);
	}

	cursor: pointer;
`

export const Arrow = styled.img`
	cursor: pointer;
`
