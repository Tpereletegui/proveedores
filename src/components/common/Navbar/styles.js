import styled from 'styled-components'
import { GiHamburgerMenu } from 'react-icons/gi'

export const Wrapper = styled.div`
	width: 100%;
	padding: 0 26px 0 26px;
	height: 77px;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.25);
	display: flex;
	align-items: center;
	justify-content: space-between;
	position: fixed;
	z-index: 99;
	background: white;
	overflow: hidden;
`

export const LeftContainer = styled.div`
	display: flex;
	align-items: center;
	gap: 22px;
`

export const Icon = styled.img`
	width: 44px;
	height: 44px;
	border-radius: 50%;
`

export const Burger = styled(GiHamburgerMenu)`
	width: 38px;
	height: 38px;
	margin-right: 20px;

	margin-left: auto;
	fill: #000000;
	display: none;

 	@media screen and (max-width: 1275px) {
		display: flex;
	} 
`

export const Logo = styled.div`
	width: 37px;
	height: 37px;
	background: #476ade;
	border-radius: 8px;
`
