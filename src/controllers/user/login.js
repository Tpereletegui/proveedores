const userLogin = ( body ) => {
  return fetch(`api/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body),
    credentials: "include"
  })
  .then(response => response.json())
  .then(response => response.token);
}

export default userLogin;