const deleteUser = ( id ) => {
  return fetch(`${process.env.apiURI}/user/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    },
    credentials: true
  })
  .then(response => response.json())
  .then(response => console.log(response));
}

export default deleteUser;