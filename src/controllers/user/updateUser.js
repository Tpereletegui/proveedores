const deleteUser = ( id, body ) => {
  return fetch(`${process.env.apiURI}/user/${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body,
    credentials: true
  })
  .then(response => response.json())
  .then(response => console.log(response));
}

export default deleteUser;