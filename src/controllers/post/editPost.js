const editPost = ( body, id ) => {
  console.log('body', body);
  console.log('id', id);
  console.log('process', process.env.apiURI)
  return fetch(`http://localhost:3000/api/post?_id=${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  })
  .then(response => response.json())
  .then(response => console.log(response));
}

export default editPost;