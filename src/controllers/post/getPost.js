const getPost = ( id ) => {
  return fetch(`${process.env.apiURI}/post/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    credentials: true
  })
  .then(response => response.json())
  .then(response => console.log(response));
}

export default getPost;