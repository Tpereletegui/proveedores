const deletePost = ( id ) => {
  console.log('id', id);
  console.log('process', process.env.apiURI)
  return fetch(`http://localhost:3000/api/post?_id=${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
    
  })
  .then(response => response.json())
  .then(response => console.log(response));
}

export default deletePost;