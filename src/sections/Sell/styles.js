import styled from "styled-components";

export const Container = styled.section`
  width: 100vw;
  overflow-y: scroll;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: flex-start;
  padding: 2% 5%;
`

export const ContentWrapper = styled.div`
  width: 100%;
  display: flex;
  height: 100%;
  justify-content: space-between;

  @media(max-width: 900px){
    flex-direction: column;
    align-items: center;
  }
`

export const MainTitle = styled.h1`
  font-size: 25px;
  color: #000;
  font-weight: 500;
  margin-bottom: 0;
`

export const Row = styled.div`
  width: 700px;
  max-width: 80%;
  margin-right: 20px;
`

export const Boxes = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`

export const Box = styled.div`
  width: 40%;
  display: flex;
  flex-direction: column;
`

export const Selector = styled.select`
  width: 100%;
  height: 40px;
  font-size: 17px;
  color: #828282;
  border: none;
  border-radius: 10px;
  position: relative;
  overflow: hidden;
  box-shadow: 0px 0px 4px rgba(0, 0, 0, .2);
  padding: 10px;
  background: #FFF;
`

export const Number = styled.input`
  width: 100%;
  height: 40px;
  font-size: 17px;
  color: #828282;
  border: none;
  border-radius: 10px;
  position: relative;
  overflow: hidden;
  box-shadow: 0px 0px 4px rgba(0, 0, 0, .2);
  padding: 10px;
`

export const TextInput = styled.textarea`
  width: 100%;
  height: 160px;
  border: none;
  border-radius: 14px;
  padding: 10px;
`

export const Division = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const Terms = styled.div`
  width: 100%;
  padding: 15px;
  border-radius: 16px;
  font-size: 14px;
  height: 160px;
  overflow-y: scroll;
  background: #FFF;
  box-shadow: 0px 0px 4px rgba(0, 0, 0, .2);
  color: #828282;
  line-height: 22px;
`

export const SubmitBtn = styled.button`
  color: #FFF;
  background: #4182BD;
  height: 54px;
  width: 100%;
  border: none;
  border-radius: 14px;
  font-size: 18px;
  margin-top: 40px;
`

export const AcceptTerms = styled.div`
  display: flex;
  width: 100%;
`

export const Accept = styled.div`
  font-size: 15px;
  color: #828282;
  font-weight: 500px;
  display: flex;
  align-items: center;
  margin-left: 30px;
`