import React from "react";
import Title from "../../components/common/Title";
import {
  Container,
  ContentWrapper,
  TextInput,
  Row,
  Boxes,
  Box,
  Selector,
  Number,
  Division,
  Terms,
  SubmitBtn,
  MainTitle,
  AcceptTerms,
  Accept
} from "./styles";
import { useForm } from "react-hook-form";

const Sell = () => {
  const { register, handleSubmit, watch } = useForm();
  const onSubmit = (data) => console.log(data);

  const accept = watch("accept");

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Container>
        <MainTitle>Crear nueva publicación</MainTitle>
        <hr style={{ width: "100%", borderColor: "2px solid #E1E1E1" }} />
        <ContentWrapper>
          <Row>
            <Title title="Descripción" />
            <TextInput {...register("description")} />
            <Boxes>
              <Box>
                <Title title="Categoría" />
                <Selector {...register("category")}>
                  <option>Tecnologia</option>
                  <option>Comida</option>
                  <option>Vestimenta</option>
                </Selector>
              </Box>
              <Box>
                <Title title="Opciones de Pago" />
                <Selector {...register("payment_method")}>
                  <option>Efectivo</option>
                  <option>Cheque</option>
                  <option>Mercado Pago</option>
                </Selector>
              </Box>
              <Box>
                <Title title="Precio" />
                <Number
                  type="number"
                  defaultValue={0}
                  step={100}
                  min={0}
                  {...register("price")}
                />
              </Box>
              <Box>
                <Title title="Cantidad Disponible" />
                <Number
                  type="number"
                  defaultValue={0}
                  step={1}
                  min={0}
                  {...register("stock")}
                />
              </Box>
            </Boxes>
          </Row>
          <Row>
            <Division>
              <Box>
                <Title title="Tiempo límite" />
                <Selector {...register("limit_time")}>
                  <option>1 mes</option>
                  <option>3 meses</option>
                  <option>6 meses</option>
                  <option>12 meses</option>
                </Selector>
              </Box>
            </Division>
            <AcceptTerms>
              <Title title="Condiciones de Publicación" />
              <Accept><input type="checkbox" style={{marginRight: '6px'}} {...register("accept")} />Aceptar</Accept>
            </AcceptTerms>
            <Terms>
              Es requisito necesario para la adquisición de los productos que se
              ofrecen en este sitio, que lea y acepte los siguientes Términos y
              Condiciones que a continuación se redactan. El uso de nuestros
              servicios así como la compra de nuestros productos implicará que
              usted ha leído y aceptado los Términos y Condiciones de Uso en el
              presente documento. Todas los productos que son ofrecidos por
              nuestro sitio web pudieran ser creadas, cobradas, enviadas o
              presentadas por una página web. Es requisito necesario para la
              adquisición de los productos que se ofrecen en este sitio, que lea
              y acepte los siguientes Términos y Condiciones que a continuación
              se redactan. El uso de nuestros servicios así como la compra de
              nuestros productos implicará que usted ha leído y aceptado los
              Términos y Condiciones de Uso en el presente documento. Todas los
              productos que son ofrecidos por nuestro sitio web pudieran ser
              creadas, cobradas, enviadas o presentadas por una página web.
            </Terms>
            <SubmitBtn type={accept ? "submit" : "button"} style={accept ? {} : {background: '#555'}}>Publicar anuncio de venta</SubmitBtn>
          </Row>
        </ContentWrapper>
      </Container>
    </form>
  );
};

export default Sell;
