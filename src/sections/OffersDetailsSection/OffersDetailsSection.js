import Layout from '../../components/common/Layout'
import useFetch from '../../hooks/useFetch'
import { Container } from './styles';
import PostOffer from '../../components/PostOffer';
import { useEffect, useState } from 'react';
import axios from 'axios';

const OffersDetailsSection = ({ id, socket }) => {
    const [post, setPost] = useState(null)
    console.log('POST', post)
    const { data } = useFetch(`http://localhost:3000/api/offer?_id=${id}`, 'get')

    useEffect(() => {
        if (data) {
            axios.get(`http://localhost:3000/api/post/${data?.post_id}`, 'get').then(response => setPost(response.data)).catch(error => console.log(error))
        }

    }, [data])


    console.log('DATAAAAAAA', data)
    /*
        useEffect(() => {
            axios.get(`http://localhost:3000/api/post/${data?.post_id}`, 'get').then(response => setPost(response.data)).catch(error => console.log(error))
        }, [data])
        */

    return (<Container>
        {post && <PostOffer socket={socket} datos={{
            pcd: post?.data?.quote.pdc,
            payment: post?.data?.quote.paymentMethod,
            conditions: post?.data?.quote.terms,
            observations1: post?.data?.quote.notes,
            billing: post?.data?.proposal.minimum,
            validation: post?.data?.proposal.endDate,
            includesTransportation: post?.data?.proposal.includesTransport,
            deliveryDate: post?.data?.proposal.shippingInterval,
            observations2: post?.data?.proposal.notes,
            deliveryAdress: post?.data?.contact.sellingAddress,
            billingAdress: post?.data?.contact.shippingAddress,
            variants: data?.variants,

        }}
            categories={post?.data?.categories}
            sellerId={data?.seller_id}
            buyerId={data?.buyer_id}
        ></PostOffer>}

    </Container>)


}
export default OffersDetailsSection