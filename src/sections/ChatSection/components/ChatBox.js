import { useEffect, useState } from 'react'
import styled from 'styled-components'
import axios from 'axios'

const Box = styled.div`
	width: 100%;
	min-height: 71px;
	display: flex;
	align-items: center;
	gap: 20px;
	position: relative;
	padding: 0 20px 0 20px;
	background: #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 10px;
	cursor: pointer;
	transition: 300ms background-color ease-out;
	border: ${({ focus }) => (focus ? '1px solid #476ADE' : 'none')};
	&:hover {
		background-color: rgba(208, 211, 213, 0.1);
	}
`

const Avatar = styled.img`
	width: 44px;
	height: 44px;
	border-radius: 50%;
`

const Read = styled.div`
	position: absolute;
	top: 10px;
	right: 10px;
	width: 10px;
	height: 10px;
	border-radius: 50%;
	background: #FF3636;
`

const NameAndMsg = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
`
const Name = styled.p`
	color: #000000;
	font-weight: 400;
	font-size: 17px;
	letter-spacing: -0.408px;
`

const Msg = styled.p`
	display: flex;
	align-items: center;
	width: 100%;
	font-weight: 400;
	font-size: 15px;
	letter-spacing: -0.408px;
	color: #828282;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
`

const ChatBox = ({ index, data, selected, setSelected, setChats, chats, execute, userId }) => {

	const [img, setImg] = useState(undefined);
	/* 	const handleChats = index => {
			if (!chats[index].read) {
				const copy = [...chats]
				copy[index].read = true
				setChats(copy)
			}
		} */
	useEffect(() => {
		const hisId = data?.members[0]?.id === userId ? data?.members[1]?.id : data?.members[0]?.id;
		const a = async () => {
			const response = await axios.get(`http://localhost:3000/api/user/${hisId}`);
			console.log(response)
			setImg(response.data.data?.image);
		}
		a();
	}, [])

	return (
		<Box
			onClick={() => {
				console.log(selected)
				setSelected(index)
				/* handleChats(index) */
				execute(index)
			}}
			focus={selected === index}
		>
			<Avatar src={img || '/img/user.png'}></Avatar>
			<NameAndMsg>
				{data && <>
					<Name>{data?.members[0]?.id === userId ? data?.members[1]?.name : data?.members[0]?.name}</Name>
					<Msg>{data?.messages[0].text}</Msg>
				</>}
			</NameAndMsg>
			{/* {!data?.read && <Read />} */}
		</Box>
	)
}
export default ChatBox
