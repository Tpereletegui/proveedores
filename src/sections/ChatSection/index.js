import { useState, useEffect, useRef, useContext } from 'react'
import { io } from "socket.io-client";
import AuthContext from '../../context/AuthContext'
import axios from 'axios';
import Modal from '../../components/common/Modal'

import { IoMdArrowBack, IoImagesOutline } from "react-icons/io";

import dynamic from 'next/dynamic'

const Picker = dynamic(import('emoji-picker-react'), { ssr: false })

import {
	Container,
	Chats,
	Chat,
	Header,
	Avatar,
	Name,
	NameAndOnline,
	Online,
	SendContainer,
	Sticker,
	Input,
	File,
	Conversation,
	General,
	Archivados,
	Back,
	GeneralOrArchivados,
	Box,
	CompraTodo,
	ModalOpen,
	InputFile,
	LabelFile
} from './styles'

import ChatBox from './components/ChatBox'

import Message from './components/Message'
import { set } from 'react-hook-form';

const ChatSection = ({ socket }) => {
	const [selected, setSelected] = useState(null)
	const [dimensions, setDimensions] = useState({})
	const [emoji, setEmoji] = useState(false)
	const [msg, setMsg] = useState('')
	const [mensajes, setMensajes] = useState([]);
	const [chats, setChats] = useState([])
	const [sellerData, setSellerData] = useState(undefined);
	const [file, setFile] = useState(false)
	const [body, setBody] = useState(null)

	const [modal, setModal] = useState(false)
	const [body2, setBody2] = useState(null)

	console.log('body2', body2)


	console.log('CHATS:', chats)

	const handleEmoji = () => {
		setEmoji(!emoji)
	}

	const handleChatSelect = async (index) => {
		if (index !== undefined) {
			setMensajes(chats[index]?.messages)
			const newSellerData = await axios.get(`http://localhost:3000/api/user/${chats[index].members[0].id === userId.id ? chats[index].members[1].id : chats[index].members[0].id}`)
			setSellerData(newSellerData);
		}
	}

	const [chosenEmoji, setChosenEmoji] = useState(null)
	const refInput = useRef(null)
	const [userId, setUserId] = useState(undefined);

	useEffect(() => {
		setUserId(JSON.parse(localStorage.getItem('loggedUser')));
	}, [])

	const onEmojiClick = (event, emojiObject) => {
		setChosenEmoji(emojiObject)
		refInput.current.focus()
	}

	const handleMsg = e => {
		setMsg(e.target.value)
	}

	useEffect(() => {
		if (chosenEmoji) {
			setMsg(prev => prev + chosenEmoji.emoji)
		}
	}, [chosenEmoji])

	useEffect(() => {
		if (body === '') return
		setMsg(body)
	}, [body])

/* 	useEffect(() => {
		if (body2 === '') return
		refInput?.current?.value = body2

	}, [body2]) */

	useEffect(() => {
		const handleResize = () => {
			const height = window.innerHeight
			const width = window.innerWidth
			setDimensions({
				height,
				width,
			})
		}

		window.addEventListener('resize', handleResize)

		return _ => window.removeEventListener('resize', handleResize)
	}, [dimensions])


	async function uploadImage(files) {
		console.log("files", files[0]);
		const formData = new FormData();
		formData.append("file", files[0]);
		formData.append("upload_preset", "azihkt5g");
		axios
			.post(
				"https://api.cloudinary.com/v1_1/proveedores/image/upload",
				formData
			)
			.then(async (response) => {
				console.log("response", response);
				setBody(response.data.url);
			});
	}

	async function uploadFile(files) {
		console.log("files", files[0]);
		const formData = new FormData();
		formData.append("file", files[0]);
		formData.append("upload_preset", "azihkt5g");
		axios
			.post(
				"https://api.cloudinary.com/v1_1/proveedores/file/upload",
				formData
			)
			.then(async (response) => {
				console.log("response", response);
				setBody2(response.data.url);
			});
	}

	console.log('BODY', body)

	useEffect(() => {

		socket?.on("chats", (chats) => {
			console.log("chats event + Chats", chats);
			setChats(chats.reverse());
			setMensajes(selected !== null ? chats[selected]?.messages : mensajes)
		})

		socket?.on("private message", (message) => {
			console.log("private message event + message", message.text)
			setMensajes(pre => [message, ...pre])


			/* 			let copy = [...chats]
						copy.forEach((elem) => { if (elem._id === message.chatId) { elem.messages.unshift(message) } })
						setChats(copy)
						console.log('COPY', copy)
						console.log('CHAT ID:', message.chatId)
						console.log('MESSAGE:', message) */
		})

		socket?.on("new chat", (chat) => {
			console.log("new chat", chat)
			setChats([...chats, chat])
		})

	}, [socket])

	const sendMessage = async () => {
		if (msg) {
			socket.emit("private message", { receiver: { id: sellerData.data.data._id, name: sellerData.data.data.username }, text: msg });
			setMsg('');
		}
	}

	return (
		<Container>

			{file && <Modal>
				<InputFile onChange={(e) => uploadImage(e.target.files)} name='file' id='file' type={'file'} />
				<LabelFile for='file'>Agregar nueva imagen</LabelFile>

				<p onClick={() => setFile(false)} style={{ position: 'absolute', top: '0', right: '10px', fontSize: '30px', cursor: 'pointer', color: '#000000' }}>x</p>
			</Modal>}

			{file && <ModalOpen></ModalOpen>}


			{modal && <Modal>
				<InputFile onChange={(e) => uploadFile(e.target.files)} name='file' id='file' type={'file'} />
				<LabelFile for='file'>Agregar archivo</LabelFile>
				<p onClick={() => setModal(false)} style={{ position: 'absolute', top: '0', right: '10px', fontSize: '30px', cursor: 'pointer', color: '#000000' }}>x</p>
			</Modal>}

			{modal && <ModalOpen></ModalOpen>}

			{selected === null ? (
				<Chats>

					<GeneralOrArchivados >

					</GeneralOrArchivados>
					{chats.map((chat, index) => {
						return <ChatBox
							data={chat}
							setChats={setChats}
							chats={chats}
							index={index}
							setSelected={setSelected}
							execute={handleChatSelect}
							selected={selected}
							userId={userId.id}
						/>
					})}
				</Chats>
			) : selected !== null && dimensions.width <= 980 ? (
				''
			) : (
				<Chats>
					<GeneralOrArchivados>
					</GeneralOrArchivados>
					{chats.map((chat, index) => {
						return <ChatBox
							data={chat}
							setChats={setChats}
							chats={chats}
							index={index}
							setSelected={setSelected}
							execute={handleChatSelect}
							selected={selected}
							userId={userId.id}
						/>
					})}
				</Chats>
			)}

			{selected !== null && (
				<Chat>

					{dimensions.width <= 980 && (

						<Back onClick={() => setSelected(null)} />

					)}
					<Header>
						<NameAndOnline>
							{chats[selected].online && <Online />}
							<Name>{chats[selected].members[0].id === userId.id ? chats[selected].members[0].name : chats[selected].members[1].name}</Name>
						</NameAndOnline>
						<Avatar src={'img/user.png'} />
					</Header>
					<Conversation>
						{mensajes.map((msg) => {
							const state = msg.sender !== userId.id;
							return <Message
								bool={state}
								direction={state ? 'flex-start' : 'flex-end'}
								msg={msg.text}
								id={msg.id}
							/>
						})}
					</Conversation>
					{emoji && (
						<Picker
							pickerStyle={{
								width: '100%',
								height: '670px'
							}}
							onEmojiClick={onEmojiClick}
						/>
					)}

					<SendContainer>
						<Sticker
							onClick={() => handleEmoji()}
							src='/img/sticker.png'
						/>
						<Input
							value={msg}
							onChange={e => handleMsg(e)}
							type='input'
							placeholder='Enviar mensaje...'
							ref={refInput}
							onKeyPress={(e) => {
								if (e.key === 'Enter') {
									sendMessage()
								}
							}
							}
						/>
						<button style={{ background: 'none', border: 'none' }} type="button" onClick={sendMessage}><File src='/img/send.svg' /></button>
						<File onClick={() => setFile(!file)} src='/img/file.png'></File>
						{/* 					<File onClick={() => setModal(!modal)} src='as'></File> */}


					</SendContainer>
				</Chat>
			)}

			{selected === null && (
				<Box>
					<CompraTodo>COMPRATODO</CompraTodo>
				</Box>
			)}
		</Container>
	)
}
export default ChatSection
