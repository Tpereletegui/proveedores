import styled from 'styled-components'

export const Container = styled.div`
	width: 100vw;
	height: 100vh;
	overflow: hidden;
	display: flex;
`

export const FormContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	height: 100%;
	width: 500px;
	max-width: 100vw;
	background: #fff;
	z-index: 99;
`

export const Slides = styled.div`
	width: calc(100vw - 500px);
	height: 100%;
	background: #476ade;
	overflow: hidden;
	display: flex;
	align-items: center;
	justify-content: center;
`

export const LogoContainer = styled.div`
	display: flex;
	gap: 20px;
	align-items: center;
	position: absolute;
	top: 0;
	left: 0;
	margin: 20px 0 0 20px;
`

export const Square = styled.div`
	width: 37px;
	height: 37px;
	background: #ffffff;
	border-radius: 8px;
`

export const Text = styled.p`
	font-weight: 700;
	font-size: 20px;
	color: #ffffff;
`

export const SlideButtons = styled.div`
	display: flex;
	width: 100px;
	justify-content: space-between;
	align-items: center;
	position: absolute;
	bottom: 10%;
`

export const SlideBtn = styled.button`
	background: #000;
	width: 24px;
	height: 24px;
	border: none;
	border-radius: 12px;
`
