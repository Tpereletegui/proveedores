import React, {useEffect, useState} from 'react';
import PostUpdate from '../../components/common/Posts/EditPost';
import { Wrapper, Head, Title, Delete, Save} from './styles.js';
import useFetch from '../../hooks/useFetch';
import { render } from 'react-dom';
import axios from 'axios';

const EditSection = ({id}) => {
  console.log('id', id)
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  // const { data, loading } = useFetch(`http://localhost:3000/api/post/${id}` , 'get');
  const categories = useFetch(`http://localhost:3000/api/category` , 'get');
  //

  useEffect(() => {
    
    async function getData(){
      if(id) {
        let datos = await axios.get(`http://localhost:3000/api/post/${id}`);
        console.log('datos', datos);
        setData(datos.data);
        setLoading(false);
      }
        return
      }
    getData()
  }, [id])
  
  // console.log('categories', categories.data)
  // console.log('dataaa', data)
  // const datos = {...data.data};
  // const postId = data.data?._id? data.data._id : '' ;
  // console.log('loading', datos)
  return (
   <Wrapper>
     <Head>
       <Title>Editar Publicación</Title>
     </Head>
     {
       loading === false  ? 
       <PostUpdate props={data} id={data._id} categories={categories.data} />
       : null
     }
     
   </Wrapper>
  )
}

export default EditSection;
