import styled from 'styled-components'

export const Container = styled.div`
	width: min(95%, 1700px);
	margin: 0 auto;
	min-height: calc(100vh - 77px);
	display: flex;
	flex-flow: row wrap;
	justify-content: center;
	align-items: center;
	padding-top: 75px;
	padding-bottom: 75px;
`

export const Text = styled.p`
	margin: 0;
	margin-left: 32px;
	font-size: 16px;
	color: #828282;
`

export const Row = styled.div`
	flex: 496px 1;
	display: flex;
	flex-direction: column;

	&:nth-child(1) {
		gap: 20px;
	}

	&:nth-child(2) {
		gap: 20px;
		align-items: center;
		align-self: flex-start;
	}
`
export const Small = styled.small`
	color:red;
`
export const AccordionContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	padding: 18px;
	background: #fff;
	box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.2);
	border-radius: 14px;
	margin-bottom: 20px;

	&:last-child {
		margin-bottom: 50px;
	}
`

/*FORMULARIO DE CONTACTO&*/

export const TitleContact = styled.p`
	width: min(90%, 600px);
	text-align: center;
	font-weight: 500;
	font-size: 18px;
	letter-spacing: -0.408px;
	color: #000000;
`

export const Description = styled.p`
	width: min(90%, 600px);
	text-align: center;
	font-weight: 500;
	font-size: 16px;
	letter-spacing: -0.408px;
	color: #828282;
`

export const Form = styled.form`
	width: min(90%, 600px);
	height: 100%;
	display: flex;
	gap: 20px;
	flex-direction: column;
`

export const InputContainer = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	gap: 11px;
`
export const Label = styled.label`
	font-weight: 500;
	font-size: 16px;
	letter-spacing: -0.408px;
	color: #000000;
`

export const Input = styled.input`
	width: 100%;
	height: 47px;
	background: #ffffff;
	border: 1px solid #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
	outline: none;
	padding-left: 10px;
`

export const Textarea = styled.textarea`
	width: 100%;
	min-height: 200px;
	background: #ffffff;
	border: 1px solid #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
	outline: none;
	resize: none;
	overflow-y: auto;
	padding: 12px;
`

export const Send = styled.button`
	align-self: flex-end;
	width: 97.78px;
	height: 35px;
	letter-spacing: -0.408px;
	font-weight: 500;
	font-size: 16px;
	color: #333333;
	background: #ffffff;
	border: 1px solid #476ade;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
	cursor: pointer;
`
