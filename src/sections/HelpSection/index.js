import {
	AccordionContainer,
	Container,
	Row,
	Text,
	TitleContact,
	Description,
	Form,
	InputContainer,
	Label,
	Input,
	Textarea,
	Send,
	Small,
} from './styles'
import Title from '../../components/common/Title'
import Accordion from '../../components/common/Accordion'
import Layout from '../../components/common/Layout'
import React from 'react'
import { useForm } from "react-hook-form";
import useFetch from '../../hooks/useFetch'
import axios from "axios"


const LoginSection = () => {
	const [accordionIndex, setAccordionIndex] = React.useState([])

	const accordionCompras = [
		{
			title: '¿Cómo hago para comprar?',
			info: 'If you experience any technical issues whilst playing one of our games, please try to reload the game - it should resume where you left off. If the issue persists, please contact our Live Chat Support',
		},
		{
			title: '¿Se puede mejorar la experiencia?',
			info: 'If you experience any technical issues whilst playing one of our games, please try to reload the game - it should resume where you left off. If the issue persists, please contact our Live Chat Support',
		},
		{
			title: '¿Qué pasa si mi vendedor no me entrega el pedido?',
			info: 'If you experience any technical issues whilst playing one of our games, please try to reload the game - it should resume where you left off. If the issue persists, please contact our Live Chat Support',
		},
		{
			title: '¿Cómo comunicarme con el vendedor?',
			info: 'If you experience any technical issues whilst playing one of our games, please try to reload the game - it should resume where you left off. If the issue persists, please contact our Live Chat Support',
		},
	]

	const accordionVentas = [
		{
			title: '¿Cómo hago para vender?',
			info: 'If you experience any technical issues whilst playing one of our games, please try to reload the game - it should resume where you left off. If the issue persists, please contact our Live Chat Support',
		},
		{
			title: '¿Se puede mejorar la experiencia?',
			info: 'If you experience any technical issues whilst playing one of our games, please try to reload the game - it should resume where you left off. If the issue persists, please contact our Live Chat Support',
		},
		{
			title: '¿Qué pasa si mi comprador no me entrega paga?',
			info: 'If you experience any technical issues whilst playing one of our games, please try to reload the game - it should resume where you left off. If the issue persists, please contact our Live Chat Support',
		},
		{
			title: '¿Cómo comunicarme con el comprador despues de la venta?',
			info: 'If you experience any technical issues whilst playing one of our games, please try to reload the game - it should resume where you left off. If the issue persists, please contact our Live Chat Support',
		},
	]

	const { register, handleSubmit, formState: { errors } } = useForm({ mode: "all" })

	async function onSubmitForm(values) {
		console.log(values);
		const response = await axios.post("http://localhost:3000/api/soporte", values)
	}
	return (
		<Container>
			<Row>
				<Title title='¿Tenés alguna duda?' blob='AYUDA' />
				<Text>Lee nuestras preguntas frecuentes.</Text>
				<Title title='Compras' />
				<AccordionContainer>
					{accordionCompras.map((accordion, index) => {
						const { title, info } = accordion
						return (
							<div
								onClick={() => {
									setAccordionIndex([0, index])
								}}
							>
								<Accordion
									title={title}
									info={info}
									active={
										index === accordionIndex[1] &&
										accordionIndex[0] === 0
									}
								/>
							</div>
						)
					})}
				</AccordionContainer>
				<Title title='Ventas' />
				<AccordionContainer>
					{accordionVentas.map((accordion, index) => {
						const { title, info } = accordion
						return (
							<div
								onClick={() => {
									setAccordionIndex([1, index])
								}}
							>
								<Accordion
									title={title}
									info={info}
									active={
										index === accordionIndex[1] &&
										accordionIndex[0] === 1
									}
								/>
							</div>
						)
					})}
				</AccordionContainer>
			</Row>
			<Row>
				<TitleContact>Contacta al soporte</TitleContact>
				<Description>
					Si tenes problemas con la plataforma no dudes en
					contactarnos. Luego de enviar tu mensaje, nos comunicaremos
					por mail con usted.
				</Description>
				<Form action="https://formsubmit.co/egancarry@gmail.com" method="POST" /*onSubmit={handleSubmit(onSubmitForm)}*/>
					<InputContainer>
						<Label htmlFor='name'>Nombre</Label>
						<Input name='name' type='text'
							{...register("name",
								{
									required: {
										value: true,
										message: "Ingresar un nombre es obligatorio"
									},
									minLength: {
										value: 2,
										message: "El nombre debe tener al menos 2 caracteres"
									},
									maxLength: {
										value: 60,
										message: "El nombre no puede superar los 60 caracteres"
									}
								})
							}
							id='name'
							style={errors.name ? { "border": " 1.5px solid red" } : null}
						></Input>
						<Small>
							{errors?.name?.message}
						</Small>
					</InputContainer>

					<InputContainer>
						<Label htmlFor='email'>Email</Label>
						<Input name="email" type='text' {...register("email", {
							required: {
								value: true,
								message: "Ingresar un email es obligatorio"
							},
							pattern: {
								value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
								message: "Ingrese un email valido"
							},

						}
						)} id='email'
							style={errors.email ? { "border": " 1.5px solid red" } : null}
						></Input>
						<Small>
							{errors?.email?.message}
						</Small>
					</InputContainer>

					<InputContainer>
						<Label htmlFor='msg'>Mensaje</Label>
						<Textarea name="msg"
							{...register("msg",
								{
									required: {
										value: true,
										message: "Ingresar un mensaje es obligatorio"
									},
									minLength: {
										value: 50,
										message: "El mensaje debe tener al menos 50 caracteres"
									},
									maxLength: {
										value: 1000,
										message: "El mensaje no puede superar los 1000 caracteres"
									}
								}
							)} type='input'
							style={errors.msg ? { "border": " 1.5px solid red" } : null}
						></Textarea>
						<Small>
							{errors?.msg?.message}
						</Small>
					</InputContainer>
					<Send type='submit'>Enviar</Send>
				</Form>
			</Row>
		</Container>
	)
}

export default LoginSection
