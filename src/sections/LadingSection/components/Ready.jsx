import { useRouter } from "next/router"
import styled from "styled-components"

const Container = styled.section`
	width: 100%;
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	gap: 40px;
	align-items: center;
	justify-content: center;
	scroll-snap-align: start;
	padding-top: 90px;
`

const Title = styled.p`
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: 48px;
`
const Button = styled.button`
	width: 255px;
	height: 55px;
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: ${({ bg }) => bg};
	color: ${({ color }) => color};
	border-radius: 30px;
	border: none;
	outline: none;
	border: 1px solid #476ade;
	cursor: pointer;
	font-weight: 700;
	font-size: 20px;
`

const Box = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	gap: 10px;
	align-items: center;
`

const Ready = () => {
	const router = useRouter()

	return (
		<Container>
			<Title data-aos="zoom-in" data-aos-duration="1000">
				¿Listo para arrancar?
			</Title>
			<Box data-aos="zoom-in" data-aos-duration="1000">
				<Button onClick={() => router.push("/signup")} bg="#476ADE" color="#FFFFFF">
					Registrarse
				</Button>
				<Button onClick={() => router.push("/ayuda")} bg="#FFFFFF" color="#476ADE">
					Necesitas ayuda?
				</Button>
			</Box>
		</Container>
	)
}

export default Ready
