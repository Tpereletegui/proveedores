import styled from "styled-components"

const Container = styled.section`
	width: 95%;
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	gap: 40px;
	margin: 0 auto;
	scroll-snap-align: start;
	padding-top: 90px;
`

const Top = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-items: center;
	gap: 10px;
`

const Title = styled.p`
	font-weight: 700;
	font-size: 48px;
	line-height: 29px;
	letter-spacing: -0.408px;
	color: #476ade;
	text-align: center;
`

const Img = styled.img`
	width: min(1000px, 95%);
`

const PricesAndDiscounts = () => {
	return (
		<Container id="precios">
			<Top data-aos="fade-up" data-aos-duration="1000">
				<Title>Precios</Title>
			</Top>
			<Img data-aos="zoom-in" data-aos-duration="1000" src="/img/precioss.png" alt="milux" />
		</Container>
	)
}

export default PricesAndDiscounts
