import styled from "styled-components"

const Container = styled.section`
	width: 95%;
	min-height: calc(100vh - 90px);
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	margin: 0 auto;
	scroll-snap-align: end;
`

const Title = styled.p`
	display: flex;
	align-items: center;
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: 48px;
`

const Description = styled.p`
	text-align: center;
	width: min(735px, 100%);
	text-align: center;
	font-weight: 400;
	font-size: 26px;
	letter-spacing: -0.408px;
	color: #000000;
`

const WhatIsMilux = () => {
	return (
		<Container>
			<Title data-aos="fade-up" data-aos-duration="1000">
				¿Que es <img style={{ marginLeft: "9px" }} width="120" src="/img/logo2.0.png" />?
			</Title>
			<Description data-aos="fade-up" data-aos-duration="1000">
				Pionera y líder del mercado,
				<span style={{ fontWeight: "700" }}>
					Milux posee soluciones digitales que permiten que compradores y vendedores interactúen de forma fácil, rápida
					e inteligente.
				</span>
				<br></br>Desde la compra hasta la venta de todas las familias de productos, tenemos soluciones adecuadas a su
				necesidad, logrando grandes beneficios para toda la cadena de suministros, aumentando la competitividad.
			</Description>
		</Container>
	)
}

export default WhatIsMilux
