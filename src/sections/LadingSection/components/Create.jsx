import styled from "styled-components"

const Container = styled.section`
	width: 100%;
	margin: 0 auto;
	min-height: 100vh;
	display: flex;
	align-items: center;
	justify-content: space-between;
	padding: 0 50px 0 100px;
	scroll-snap-align: start;
	padding-top: 90px;
	@media screen and (max-width: 1400px) {
		flex-direction: column-reverse;
		justify-content: space-evenly;
		padding: 0;
	}
`

const Title = styled.p`
	width: min(900px, 100%);
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: 48px;
`

const Create = () => {
	return (
		<Container>
			<p style={{ fontWeight: "700", fontSize: "36px", color: "#000000", textAlign: "center" }}>GIF</p>
			<Title data-aos="zoom-in" data-aos-duration="1000">
				Crea una publicacion cargando los diferentes productos que ofreces o que estas buscando
			</Title>
		</Container>
	)
}

export default Create
