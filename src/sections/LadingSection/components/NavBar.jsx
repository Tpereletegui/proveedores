import { useRouter } from "next/router"

import styled from "styled-components"
import Link from "next/link"

const Container = styled.header`
	width: 100%;
	min-height: 90px;
	position: fixed;
	top: 0;
	z-index: 1;
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 0 26px;
	background-color: #ffffff;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.25);
`

const Nav = styled.nav`
	height: 100%;
	display: flex;
	gap: 20px;
	justify-content: center;
	align-items: center;
	color: #000000;
	font-weight: 400;
	font-size: 17px;
`

const Sign = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	gap: 10px;
	@media screen and (max-width: 435px) {
		flex-direction: column;
	}
`

const Button = styled.button`
	width: 112px;
	height: 26px;
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: ${({ bg }) => bg};
	color: ${({ color }) => color};
	border-radius: 8px;
	border: none;
	outline: none;
	cursor: pointer;
	border: 1px solid #476ade;
`

const Enlace = styled(Link)``

const NavBar = () => {
	const router = useRouter()

	return (
		<Container>
			<Nav>
				<Enlace href="/ayuda">Contacto</Enlace>
				<Enlace href="#precios">Precios</Enlace>
			</Nav>
			<img width="130" src="/img/logo3.0.png" />
			<Sign>
				<Button onClick={() => router.push("/signup")} bg="#FFFFFF" color="#476ADE">
					Registrarse
				</Button>
				<Button onClick={() => router.push("/login")} bg="#476ADE" color="#FFFFFF">
					Iniciar sesion
				</Button>
			</Sign>
		</Container>
	)
}
export default NavBar
