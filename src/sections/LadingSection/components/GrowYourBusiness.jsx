import styled from "styled-components"
import { useRouter } from "next/router"

const Container = styled.section`
	width: 100%;
	min-height: 100vh;
	padding-top: 90px;
	display: flex;
	align-items: center;
	justify-content: center;
	gap: 10px;
	scroll-snap-align: end;
	@media screen and (max-width: 1372px) {
		flex-direction: column;
		scroll-snap-align: start;
	}
`
const Img = styled.img`
	position: absolute;
	bottom: -50px;
	right: 0;
	width: 20%;
`

const Img2 = styled.img`
	width: 100%;
`

const Right = styled.div`
	width: min(100%, 800px);
	display: flex;
	align-items: center;
	position: relative;
`

const Left = styled.div`
	width: min(620px, 100%);
	display: flex;
	flex-direction: column;
	align-items: center;
`
const Title = styled.p`
	font-weight: 400;
	font-size: 24px;
	line-height: 42px;
	text-align: center;
	letter-spacing: -0.408px;
	color: #000000;
`

const Description = styled.p`
	font-weight: 400;
	font-size: 20px;
	line-height: 28px;
	letter-spacing: -0.408px;
	color: #000000;
	text-align: center;
	width: min(560px, 100%);
`

const Button = styled.button`
	width: 196px;
	height: 40px;
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: ${({ bg }) => bg};
	color: ${({ color }) => color};
	border-radius: 30px;
	border: none;
	outline: none;
	border: 1px solid #3949ab;
	cursor: pointer;
	font-weight: 700;
	font-size: 14px;
`

const Buttons = styled.div`
	margin-top: 20px;
	display: flex;
	align-items: center;
	gap: 10px;
`

const GrowYourBusiness = () => {
	const router = useRouter()
	return (
		<Container>
			<Left data-aos="fade-right" data-aos-duration="1000">
				<Title>HACE CRECER TU COMERCIO CON</Title>
				<img width="150" src="/img/logo3.0.png" alt="milux" />
				<Description>
					Nunca fue tan facil comprar y vender como con <span style={{ fontWeight: "700" }}>Milux</span>.<br></br> Atrae
					tus clientes, impulsa tus ventas y lleva tu comercio al proximo nivel.
				</Description>
				<Buttons>
					<Button onClick={() => router.push("/signup")} color="#FFF" bg="#476ADE">
						Registrarse
					</Button>
					<Button as="a" href="#precios" color="#476ADE" bg="#FFF">
						Ver precios
					</Button>
				</Buttons>
			</Left>
			{/* <img src="/img/all.png" data-aos="fade-right" data-aos-duration="1000" /> */}
			<Right data-aos="fade-left" data-aos-duration="1000">
				<Img2 src="/img/mac.png" alt="milux" />
				<Img src="/img/iphone.png" alt="milux" />
			</Right>
		</Container>
	)
}

export default GrowYourBusiness
