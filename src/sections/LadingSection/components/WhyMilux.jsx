import styled from "styled-components"

const Container = styled.section`
	width: 95%;
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	margin: 0 auto;
	scroll-snap-align: start;
`

const Title = styled.p`
	display: flex;
	align-items: center;
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: 48px;
`

const Description = styled.p`
	width: min(735px, 100%);
	text-align: center;
	font-weight: 400;
	font-size: 26px;
	letter-spacing: -0.408px;
	color: #000000;
`

const WhyMilux = () => {
	return (
		<Container>
			<Title data-aos="fade-up" data-aos-duration="1000">
				¿Por que usar <img style={{ marginLeft: "9px" }} width="120" src="/img/logo2.0.png" />?
			</Title>
			<Description data-aos="fade-up" data-aos-duration="1000">
				Porque es una plataforma web en la que ambos usuarios pueden hacer publicaciones sobre los insumos que vendan o
				necesiten comprar, aumentando asi la competitividad, para que siempre puedas conseguir lo mejor, al mejor
				precio.
			</Description>
		</Container>
	)
}

export default WhyMilux
