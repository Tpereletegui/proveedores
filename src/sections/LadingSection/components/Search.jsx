import styled from "styled-components"

const Container = styled.section`
	width: 100%;
	min-height: 100vh;
	padding-top: 90px;
	display: flex;
	align-items: center;
	justify-content: center;
	gap: 30px;
	scroll-snap-align: start;
	@media screen and (max-width: 1372px) {
		flex-direction: column;
	}
`

const Title = styled.p`
	text-align: center;
	width: 30%;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: 48px;
	@media screen and (max-width: 1372px) {
		width: 100%;
	}
`

const Img = styled.img`
	width: min(1000px, 100%);
`

const Search = () => {
	return (
		<Container>
			<Title data-aos="fade-right" data-aos-duration="1000">
				Busca el producto que necesitas
			</Title>
			<Img data-aos="zoom-in" data-aos-duration="1000" src="/img/home.png" alt="milux" />
		</Container>
	)
}
export default Search
