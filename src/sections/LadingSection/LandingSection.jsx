import Head from "next/head"
import NavBar from "./components/NavBar"
import { Container, Help } from "./styles"
import WhatIsMilux from "./components/WhatIsMilux"
import GrowYourBusiness from "./components/GrowYourBusiness"
import WhyMilux from "./components/WhyMilux"
import Search from "./components/Search"
import PricesAndDiscounts from "./components/PricesAndDiscounts"
import Categories from "./components/Categories"
import Create from "./components/Create"
import All from "./components/All"
import Ready from "./components/Ready"

import { useRouter } from "next/router"

//AOS
import AOS from "aos"
import "aos/dist/aos.css"
import { useEffect, useRef } from "react"

const LandingSection = () => {
	const router = useRouter()
	const observer = useRef().current

	let callback = function (entries, observer) {
		entries.forEach(entry => {
			if (entry.target.nodeName === "IMG" && entry.isIntersecting) {
				let objetivo = entry.target
				objetivo.classList.add("lazy")
			}

			if (entry.target.nodeName === "DIV" && entry.isIntersecting) {
				let objetivo = entry.target
				objetivo.classList.add("lazy2")
			}
		})
	}
	let options = {
		root: null,
		threshold: 0.25,
	}

	useEffect(() => {
		observer = new IntersectionObserver(callback, options)
		observer.observe(document.getElementById("img"))
		observer.observe(document.getElementById("box"))
	}, [])

	useEffect(() => {
		AOS.init({
			// Global settings:
			disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
			startEvent: "DOMContentLoaded", // name of the event dispatched on the document, that AOS should initialize on
			initClassName: "aos-init", // class applied after initialization
			animatedClassName: "aos-animate", // class applied on animation
			useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
			disableMutationObserver: false, // disables automatic mutations' detections (advanced)
			debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
			throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)

			// Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
			offset: 300, // offset (in px) from the original trigger point
			delay: 0, // values from 0 to 3000, with step 50ms
			duration: 400, // values from 0 to 3000, with step 50ms
			easing: "ease", // default easing for AOS animations
			once: false, // whether animation should happen only once - while scrolling down
			mirror: true, // whether elements should animate out while scrolling past them
			anchorPlacement: "top-bottom", // defines which position of the element regarding to window should trigger the animation
		})
	}, [])

	return (
		<>
			<Head>
				<title>Bienvenido!</title>
			</Head>
			<Container>
				<Help onClick={() => router.push("/ayuda")}>?</Help>
				<NavBar />
				{<GrowYourBusiness />}
				<WhatIsMilux />
				<WhyMilux />
				<Categories />
				<Create />
				<Search />
				<All />
				<PricesAndDiscounts />
				<Ready />
			</Container>
		</>
	)
}
export default LandingSection
