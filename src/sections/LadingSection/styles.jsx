import styled from "styled-components"

export const Container = styled.div`
	width: 100%;
	min-height: 100vh;
	scroll-snap-type: y mandatory;
`

export const Help = styled.div`
	width: 70px;
	height: 62px;
	position: fixed;
	bottom: 50px;
	right: 50px;
	display: flex;
	justify-content: center;
	align-items: center;
	background: #476ade;
	box-shadow: -2px 3px 5px rgba(0, 0, 0, 0.25);
	border-radius: 50%;
	color: #ffffff;
	font-weight: 700;
	font-size: 36px;
	cursor: pointer;
	z-index: 20;
`
