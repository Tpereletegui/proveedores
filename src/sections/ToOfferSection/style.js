import styled from 'styled-components'

export const Container = styled.div`
	width: 100%;
	height: auto;
	min-height: calc(100vh - 77px);
	display: flex;
	flex-direction: column;
	align-items: center;
`
