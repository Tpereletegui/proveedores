import styled from 'styled-components'

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
  	height: calc(100vh - 77px);
`
export const PostsContainer = styled.div`
	/* display: flex; */
	/* flex-wrap: wrap; */
	width: 100%;
	height:calc(100vh - 77px);
	/* justify-content: space-between;
	align-items: center; */
	scroll-snap-type: y mandatory;
    overflow-y: auto;

	@media (max-width: 1075px) {
		justify-content: center;
	}
`


export const TitleAndCategories = styled.div`
	display: flex;
	flex-direction: column;
	gap: 20px;
	width: 100%;
	height:100%;
`

export const TitleAndSection = styled.div`
	display: flex;
	flex-direction: column;
`
