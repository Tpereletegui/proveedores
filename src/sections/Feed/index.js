import { useEffect, useState } from 'react'
import axios from 'axios'
import useFetch from '../../hooks/useFetch'
import { io } from 'socket.io-client'

import {
	Wrapper,
	PostsContainer,
	TitleAndCategories,
	TitleAndSection,
} from './styles'
import Title from '../../components/common/Title'
import CategorySelector from '../../components/Feed/CategorySelector'
import Post from '../../components/common/PostHome'
import { useCategoriesContext } from '../../context/categoriesContext'


const Feed = ({ socket }) => {
	const [isCategory, setIsCategory] = useState(false)
	const { category } = useCategoriesContext()

	const { data } = useFetch('http://localhost:3000/api/post', 'get')
	console.log('chekceandooo', data);
	const [postsByCategory, setpostsByCategory] = useState([])

	useEffect(() => {
		if (category.id !== null) {
			axios({
				method: 'post',
				url: 'http://localhost:3000/api/post/categories',
				data: {
					categories: [category.id],
				},
			})
				.then(res => {
					setpostsByCategory(res.data)
					setIsCategory(true)
				})
				.catch(error => console.log(error))
		}
	}, [category])

	return (

		<Wrapper>
			<TitleAndSection>
				{/* <Title
					title='Publicaciones Recientes'
					blob={isCategory ? category.name : 'NUEVO'}
				/> */}
				<PostsContainer>
					{function () {
						if (isCategory) {
							if (postsByCategory.length) {
								return postsByCategory.map((post, index) => (
									<Post
										key={index}
										socket={socket}
										datos={{
											id: post._id,
											/* Cotizacion*/
											pcd: post.quote.pdc,
											payment: post.quote.paymentMethod,

											observations1: post.quote.notes,
											/* Propuesta*/
											billing: post.proposal.minimum,
											validation: post.proposal.endDate,
											includesTransportation:
												post.proposal.includesTransport,
											deliveryDate:
												post.proposal.shippingInterval,
											/*paymentConditions: post.proposal.,*/
											observations2: post.proposal.notes,
											id: post._id,
											/*Contacto*/
											deliveryAdress:
												post.contact.sellingAddress,
											contact: post.contact.contactName,
											variants: post.variants,


										}}
										sellerId={post.seller_id}
										categories={post.categories}
										edit={false}

									></Post>
								))
							} else {
								return (<p>No hay publicaciones para esta categoria</p>)
							}
						} else {
							return data.map((post, index) => (
								<Post
									key={index}
									socket={socket}
									datos={{
										id: post._id,
										/* Cotizacion*/
										pcd: post.quote.pdc,
										payment: post.quote.paymentMethod,

										observations1: post.quote.notes,
										/* Propuesta*/
										billing: post.proposal.minimum,
										validation: post.proposal.endDate,
										includesTransportation:
											post.proposal.includesTransport,
										deliveryDate:
											post.proposal.shippingInterval,
										/*paymentConditions: post.proposal.,*/
										observations2: post.proposal.notes,

										/*Contacto*/
										deliveryAdress:
											post.contact.sellingAddress,
										contactName: post.contact.contactName,
										variants: post.variants,
										id: post._id,
										categories: post.categories
									}}
									sellerId={post.seller_id}
									categories={post.categories}
								></Post>
							))
						}
					}.call()}
				</PostsContainer>
			</TitleAndSection>
		</Wrapper>
	)
}

export default Feed
