import React, { useState, useEffect } from "react";
import Link from "next/link";
// import Posts from "../../components/common/Posts/index";
import PostsHome from '../../components/common/PostHome/index';
import Modal from "../../components/common/Modal";
import {
  Wrapper,
  ProfilePic,
  Name,
  Box,
  Info,
  InfoText,
  Rating,
  PRating,
  Hr,
  BoxText,
  Active,
  Category,
  Posts,
  PicContainer,
  Hover,
  ModalOpen,
  InputContainer,
  ButtonSave,
	DescriptionText
} from "./styles";

import Layout from "../../components/common/Layout";
import axios from "axios";


export const ProfileSection = ({ edit }) => {
  const [seller, setSeller] = useState(null);
  const [userId, setUser] = useState("");
  const [body, setBody] = useState(undefined);
  const [modal, setModal] = useState(false);
  const [usuario, setUsuario] = useState({});
  const [data, setData] = useState([]);

  console.log('checkeando', process.env.VERCEL_URL);

  useEffect(() => {
    if (JSON.parse(localStorage.getItem("loggedUser")) === null) {
      window.location = "/login";
    }
  }, []);

  useEffect(() => {
    const getSellerById = async () => {
      try {
        const user = localStorage.getItem("loggedUser");
        const userparsed = JSON.parse(user);
        setUser(userparsed.id);
        console.log("userid", userparsed);
        const response = await axios.get(
          `${process.env.VERCEL_URL}/api/user/getseller?_id=${userparsed.id}`
        );

        setSeller(response.data);
      } catch (error) {
        console.log(error);
      }
    };

    getSellerById();
  }, []);

  useEffect(() => {
    const getPostBySeller = async () => {
      try {
        const response = await axios.get(
          `${process.env.VERCEL_URL}/api/user/posts?seller_id=${seller._id}`
        );
        setData(response.data);
      } catch (error) {
        console.log(error);
      }
    };

    getPostBySeller();
  }, [seller]);
  useEffect(() => {
    const getUserInfo = async () => {
      try {
        if (!userId) return;
        const response = await axios.get(
          `${process.env.VERCEL_URL}/api/user/${userId}`
        );
        console.log("USUARIO", response);
        setUsuario(response.data.data);
      } catch (error) {
        console.log(error);
      }
    };

    getUserInfo();
  }, [userId]);

  async function uploadImage(files) {
    console.log("files", files[0]);
    const formData = new FormData();
    formData.append("file", files[0]);
    formData.append("upload_preset", "azihkt5g");
    axios
      .post(
        "https://api.cloudinary.com/v1_1/proveedores/image/upload",
        formData
      )
      .then(async (response) => {
        console.log("response", response);
        setBody(response.data.url);
      });
  }
  async function update() {
    let tomas = await axios.put(`${process.env.VERCEL_URL}/api/user?id=${userId}`, {
      image: body,
    });
    window.location.reload();
  }

 console.log('data', data)

  return (
    <>
      <Layout>
        <Wrapper>
          <PicContainer style={{ marginBottom: "30px" }}>
            <ProfilePic src={usuario?.image ? usuario.image : ""} />
            <Hover onClick={() => setModal(!modal)}> </Hover>
          </PicContainer>
          {modal && (
            <>
              <InputContainer
                onChange={(e) => uploadImage(e.target.files)}
                type={"file"}
              ></InputContainer>
              <ButtonSave onClick={() => update()}>Actualizar</ButtonSave>
            </>
          )}
          <Name style={{ marginBottom: "20px" }}>
            {seller?.businessName ? seller.businessName : usuario.username}
          </Name>
					{seller !== null ? 
					<DescriptionText>
						{seller.province ?? ''}, {seller.city ?? ''}
					</DescriptionText>
					:
					<>
          <DescriptionText>Se unió el: {usuario?.createdAt?.slice(0, 10)} </DescriptionText>
          
          </>	
				}
          {seller !== null ? (
            <Info>
              <InfoText>{data?.length} Publicaciones</InfoText>
              <InfoText>Vendedor</InfoText>
							<InfoText>{seller.interactions} Interacciones</InfoText>
            </Info>
          ) : (
            <Info>
              <InfoText> {usuario?.email ? usuario.email : ""}</InfoText>
              <InfoText style={{color: '#476ADE'}}><Link href='/signup'>Hazte Vendedor</Link></InfoText>
              <InfoText>
                Telefono: {usuario?.phone ? usuario.phone : ""}
              </InfoText>
            </Info>
          )}
        </Wrapper>
        {seller && seller ? (
          <Posts>
            <Active>
              <BoxText>Publicaciones Activas</BoxText>
              
            </Active>

            {data.length ? (
              data.map((post) => (
                <PostsHome
                  edit={true}
                  datos={{
                    /* Cotizacion*/
                    id: post._id,
                    pcd: post.quote.pdc,
                    payment: post.quote.paymentMethod,
                    conditions: post.quote.terms,
                    observations1: post.quote.notes,
                    /* Propuesta*/
                    billing: post.proposal.minimum,
                    validation: post.proposal.endDate,
                    includesTransportation: post.proposal.includesTransport,
                    deliveryDate: post.proposal.shippingInterval,
                    /*paymentConditions: post.proposal.,*/
                    observations2: post.proposal.notes,

                    /*Contacto*/
                    deliveryAdress: post.contact.sellingAddress,
                    contactName: post.contact.contactName,
                    variants: post.variants,
                  }}
                />
              ))
            ) : (
              <Name>No hay publicaciones activas</Name>
            )}
          </Posts>
        ) : (
          ""
        )}
      </Layout>
    </>
  );
};
