import styled from 'styled-components'

export const Wrapper = styled.div`
	position: relative;
	display: flex;
	justify-content: space-evenly;
	align-items: center;
	flex-direction: column;
	margin-top: 50px;
	height: 500px;
	z-index: 9;
	width: 100%;
	@media (max-width: 1024px) {
		width: 100%;
	}
`
export const ProfilePic = styled.img`
	width: 100%;
	height: 100%;
	border-radius: 50%;
	border: 0.5px solid #B2B1B2;
	position: absolute;
	
`
export const Name = styled.p`
	font-weight: 500;
	font-size: 25px;
	line-height: 22px;
	text-align: center;
	letter-spacing: -0.408px;
	color: #000000;
	border-bottom: 2px solid #476ade;
	padding-bottom: 15px;
`

export const Rating = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: row;
`

export const Info = styled.div`
	display: flex;
	justify-content: space-evenly;
	align-items: center;
	flex-direction: row;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
  border-radius: 14px;
	background-color: white;
	height: 150px;
	width: 80%;
	@media (max-width: 1024px) {
		flex-direction: column;
		width: 90%;
	}
`

export const InfoText = styled.p`
	color: #000000;
	font-weight: 500;
	font-size: 18px;
	line-height: 22px;
	letter-spacing: -0.408px;
`
export const DescriptionText =styled.p`
	color: #828282;
	font-weight: 500;
	font-size: 16px;
	line-height: 22px;
	letter-spacing: -0.408px;
`

export const Box = styled.div`
	width: 90%;
	background-color: white;
	height: 128px;
	display: flex;
	justify-content: space-evenly;
	align-items: center;
	flex-direction: row;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
	@media (max-width: 1024px) {
		flex-direction: column;
		width: 90%;
	}
`

export const BoxText = styled.p`
	font-weight: 500;
	font-size: 20px;
	line-height: 22px;
	letter-spacing: -0.408px;
	color: #000000;
`

export const Hr = styled.hr`
	height: 2px;
	width: 80%;
	background-color: #e1e1e1;
	border: none;
	position: absolute;
	top: 28%;
	left: 10%;
	z-index: 0;
	@media (max-width: 1024px) {
		display: none;
	}
`

export const PRating = styled.p`
	font-weight: normal;
	font-size: 15px;
	line-height: 22px;
	letter-spacing: -0.408px;
	color: #dfa819;
`

export const Active = styled.div`
	display: flex;
	width: 90%;
	flex-direction: row;
	align-items: center;
	margin: auto;
	margin-top: 40px;
	margin-bottom: 40px;
`

export const Category = styled.p`
	width: max-content;
	font-size: 20px;
	background: #476ade;
	border-radius: 8px;
	padding: 8px;
	color: white;
	margin-left: 15px;
`

export const Posts = styled.div`
	width: 90%;
	display: flex;
	flex-direction: column;
	align-items: center;
	gap: 30px;
	margin: 0 auto;
	margin-bottom: 50px;
`

export const PicContainer = styled.div`
	width: 120px;
	height: 120px;
	border-radius: 50%;
	position: relative;
	
`


export const Hover = styled.button`
	color: transparent;
	position: absolute;
	width: 120px;
	height: 120px;
	border-radius: 50%;
	margin: auto;
	background-color: transparent;
	
	border: none;
	cursor: pointer;
	&:hover{
		background-color: rgba(236, 236, 236, 0.7);
		background-image: url('/img/pencilModal.png');
		background-repeat: no-repeat;
		background-position: 50% 70%;
		color: black;
	}
`

export const ModalOpen = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: rgba(220,220,220,0.9);
`

export const InputContainer = styled.input`
	
`

export const ButtonSave =styled.button `
  background: #FFFFFF;
  border: 1px solid #476ADE;
  box-sizing: border-box;
  border-radius: 8px;
  width: 100px;
  height: 30px;
	margin-top: 20px;
	margin-bottom: 20px;
  
  color: #476ADE;
  cursor: pointer;
  &:hover{
    background-color: #476ADE;
    color: #FFFFFF;
  }

`
