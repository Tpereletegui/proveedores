import styled from "styled-components"

export const Container = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	/*justify-content: center;*/
	align-items: center;
	padding: 70px 0;
	gap: 20px;
`

export const Title = styled.p`
	width: 80%;
	font-weight: 500;
	font-size: 18px;
	letter-spacing: -0.408px;
	color: #000000;
`

export const Table = styled.table`
	width: 80%;
	height: 700px;
	display: flex;
	flex-direction: column;
	font-size: 17px;
	@media screen and (max-width: 1050px) {
		font-size: 12px;
	}

	@media screen and (max-width: 625px) {
		display: none;
	}
`

export const Thead = styled.tbody`
	width: 100%;
	height: 60px;
	display: flex;
`

export const ColHead = styled.td`
	width: calc(100% / 6);
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	gap: 10px;
	font-weight: 500;
	font-size: 1em;
	letter-spacing: -0.408px;
	color: #454545;
`

export const Arrow = styled.img`
	cursor: pointer;
`

export const Tbody = styled.tbody`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	gap: 20px;
	overflow-y: auto;

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 3px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 20px;
	}
`

export const Row = styled.tr`
	width: 100%;
	min-height: 75px;
	display: flex;
	background: #ffffff;
	border-radius: 14px;
`

export const Col = styled.td`
	width: calc(100% / 6);
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	text-align: center;
	font-weight: 500;
	font-size: 1em;
`

export const MsgContainer = styled.div`
	width: 3.25em;
	height: 2.4375em;
	display: flex;
	justify-content: center;
	align-items: center;
	background: #e8e8e8;
	border-radius: 14px;
	cursor: pointer;
`
export const Msg = styled.img`
	width: 1.6em;
	height: 1.6em;
`

export const TableMobileContainer = styled.div`
	width: 100%;
	height: 100%;
	font-size: 16px;
	display: none;
	flex-direction: column;
	align-items: center;
	gap: 20px;
	overflow-y: scroll;

	@media screen and (max-width: 625px) {
		display: flex;
	}

	@media screen and (max-width: 365px) {
		font-size: 14px;
	}

	@media screen and (max-width: 260px) {
		font-size: 13px;
	}
`

export const TableMobile = styled.div`
	width: 90%;
	height: auto;
	font-size: 1em;
	background: #ffffff;
	border-radius: 14px;
	padding: 0px 15px 0px 15px;
`

export const RowMobile = styled.div`
	width: 100%;
	height: 50px;
	font-size: 1em;
	display: flex;
	&:nth-child(6) {
		justify-content: flex-end;

		@media screen and (max-width: 260px) {
			justify-content: center;
		}
	}
`

export const ColMobile = styled.div`
	width: 50%;
	height: 100%;
	font-size: 1em;
	display: flex;
	justify-content: flex-start;
	align-items: center;
`

export const Table2 = styled.table`
	width: 80%;
	display: flex;
	flex-direction: column;
	font-size: 17px;
	margin: 0 auto;
	margin-bottom: 20px;
`

export const ModalOpen = styled.div`
	position: absolute;
	width: 100%;
	height: 100%;
	background-color: rgba(220, 220, 220, 0.7);
	z-index: 8;
`

export const Head2 = styled.head`
	width: 100%;
	height: 60px;
	display: flex;
`

export const ColHead2 = styled.td`
	width: calc(100% / 3);
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	gap: 10px;
	font-weight: 500;
	font-size: 1em;
	letter-spacing: -0.408px;
	color: #454545;
`

export const Tbody2 = styled.tbody`
	width: 100%;
	max-height: 400px;
	display: flex;
	flex-direction: column;
	gap: 20px;
	overflow-y: auto;

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 3px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 20px;
	}
`

export const Row2 = styled.tr`
	width: 100%;
	height: 40px;
	display: flex;
	background: #ffffff;
	border-radius: 14px;
`

export const Col2 = styled.td`
	width: calc(100% / 3);
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	text-align: center;
	font-weight: 500;
	font-size: 1em;
`
