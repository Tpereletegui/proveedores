import axios from "axios"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import Layout from "../../components/common/Layout"

import Modal from "../../components/common/Modal"

import {
	Container,
	Table,
	Thead,
	ColHead,
	Tbody,
	Row,
	Col,
	Msg,
	MsgContainer,
	TableMobile,
	ColMobile,
	RowMobile,
	TableMobileContainer,
	Title,
	Table2,
	Head2,
	ColHead2,
	Tbody2,
	Row2,
	Col2,
	ModalOpen,

} from "./styles"

const OffersSection = () => {
	const [offers, setOffers] = useState(undefined)
	const [users, setUsers] = useState([])
	const [selected, setSelected] = useState(null)
	const [offer, setOffer] = useState(null)
	const [modal, setModal] = useState(false)

	const openModal = row => {
		setOffer(row)
		setModal(true)
	}

	const closeModal = () => {
		setModal(false)
	}

	let router = useRouter()
	console.log("oferta concreta", offer)
	/*onClick={() => router.push('/post/abc')}*/

	useEffect(() => {
		if (JSON.parse(localStorage.getItem("loggedUser")) === null) {
			window.location = "/login"
		}
	}, [])

	useEffect(() => {
		async function a() {
			try {
				const seller = await axios.get(
					`http://localhost:3000/api/user/getseller?_id=${JSON.parse(localStorage.getItem("loggedUser")).id}`
				)
				const newOffers = await axios.get(`http://localhost:3000/api/offer/own?id=${seller.data._id}`)
				setOffers(newOffers.data)
				console.log("NUEVAS OFERTAS", newOffers.data)
				const newUsers = await axios.get(`http://localhost:3000/api/user`)
				setUsers(newUsers.data)
			} catch (error) {
				console.log(error)
			}
		}
		a()
	}, [])

	return (
		<Layout heightContainer={"100vh"}>
			{modal && <ModalOpen></ModalOpen>}
			<Container>
				{modal && <Modal>
					<Table2>
						<Head2>
							<ColHead2>Precios</ColHead2>
							<ColHead2>Unidades</ColHead2>
							<ColHead2>Modelos</ColHead2>
						</Head2>
						<Tbody2>
							{offer?.variants.map((variant, index) => (
								<Row2 key={index}>
									<Col2>{variant.price}</Col2>
									<Col2>{variant.quantity}</Col2>
									<Col2>{variant.variantType}</Col2>
								</Row2>
							))}
						</Tbody2>
					</Table2>
					<p onClick={() => closeModal()} style={{ position: 'absolute', top: '0', right: '10px', fontSize: '30px', cursor: 'pointer', color: '#000000' }}>x</p>
				</Modal>}


				<Title>Actividad de la Publicación</Title>
				{offers ? (
					<>
						<Table>
							<Thead>
								{["Tipo", "Modelo", "Cantidad", "Precio", "Comprador"].map((col, index) => (
									<ColHead key={index}>
										{col}
									</ColHead>
								))}
							</Thead>
							<Tbody>
								{offers &&
									offers.map((row, index) => {
										let precios = []
										let modelos = []
										let cantidades = []
										row.variants.map((v, i) => {
											if (i !== row.variants.length - 1) {
												precios.push(`${v.price}`)
												cantidades.push(v.quantity)
												return modelos.push(v.variantType)
											} else {
												precios.push(`${v.price}`)
												cantidades.push(v.quantity)
												return modelos.push(v.variantType)
											}
										})
										let username

										if (users) {
											username =
												users.filter(user => user && user._id === row.buyer_id).length &&
												users.filter(user => user && user._id === row.buyer_id)[0].username
										}

										return (
											<Row key={index}>
												<Col>{row.type}</Col>
												<Col>
													{modelos.length > 1 ? (
														''

													) : (
														modelos
													)}
												</Col>
												<Col>
													{cantidades.length > 1 ? (
														<p style={{ fontWeight: '700' }}>Multiples productos</p>

													) : (
														cantidades
													)}
												</Col>
												<Col>
													{precios.length > 1 ? (
														''
													) : (
														precios
													)}
												</Col>
												<Col>{username}</Col>
												{/* <Col>{row.createdAt.slice(0, 10)}</Col> */}
												<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', gap: '8px', width: '120px' }}>
													{row.variants.length > 1 && <MsgContainer onClick={() => openModal(row)}>
														<Msg src="/img/formatlist.svg"></Msg>
													</MsgContainer>}
													<MsgContainer onClick={() => router.push(`/offer/${row._id}`)}>
														<Msg src="/img/visibility.svg"></Msg>
													</MsgContainer>

												</div>

											</Row>
										)
									})}
							</Tbody>
						</Table>

						<TableMobileContainer>
							{offers &&
								offers.map(row => {
									let precios = ""
									let modelos = ""
									let cantidades = ""
									row.variants.map((v, i) => {
										if (i !== row.variants.length - 1) {
											precios += "$" + v.price + " / "
											cantidades += v.quantity + " / "
											return (modelos += v.variantType + " / ")
										} else {
											precios += "$" + v.price
											cantidades += v.quantity
											return (modelos += v.variantType)
										}
									})
									let username

									if (users) {
										username =
											users.filter(user => user && user._id === row.buyer_id).length &&
											users.filter(user => user && user._id === row.buyer_id)[0].username
									}

									return (
										<TableMobile>
											<RowMobile>
												<ColMobile>Tipo</ColMobile>
												<ColMobile>{row.type}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Modelos</ColMobile>
												<ColMobile>{modelos.length > 1 ? '' : modelos}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Unidades</ColMobile>
												<ColMobile>{cantidades.length > 1 ? (
													<p style={{ fontWeight: '700' }}>Multiples productos</p>

												) : (
													cantidades
												)}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Precios</ColMobile>
												<ColMobile>{precios.length > 1 ? '' : precios}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Comprador</ColMobile>
												<ColMobile>{username}</ColMobile>
											</RowMobile>
											<RowMobile>
												<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', gap: '8px', width: '120px' }}>
													{row.variants.length > 1 && <MsgContainer onClick={() => openModal(row)}>
														<Msg src="/img/formatlist.svg"></Msg>
													</MsgContainer>}
													<MsgContainer onClick={() => router.push(`/offer/${row._id}`)}>
														<Msg src="/img/visibility.svg"></Msg>
													</MsgContainer>

												</div>
											</RowMobile>
										</TableMobile>
									)
								})}
						</TableMobileContainer>
					</>
				) : (
					<p>No hay contra ofertas</p>
				)}
			</Container>
		</Layout>
	)
}

export default OffersSection
