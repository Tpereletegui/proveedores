import styled from 'styled-components'
import { AiOutlineSend } from 'react-icons/ai'
import { TiDeleteOutline } from 'react-icons/ti'
export const Container = styled.div`
	width: 100%;
	min-height: calc(100vh - 77px);
	display: flex;
	flex-direction: column;
	align-items: center;
	gap: 39px;
	background: #f8f8f8;
	padding: 78px 0 78px 0;
`
export const Growth = styled.p`
	font-weight: 500;
	font-size: 18px;
	letter-spacing: -0.408px;
	color: #000000;
	margin-right: auto;
	display: flex;
`
export const GrowthText = styled.p`
	font-weight: 500;
	font-size: 18px;
	letter-spacing: -0.408px;
	color: #5fb17b;
	margin-right: 5px;
`
export const ArrowUpward = styled.i`
	font-weight: 500;
	font-size: 18px;
	letter-spacing: -0.408px;
	color: #5fb17b;
	margin-right: auto;
	position: relative;
	top: 3px;
	padding-right: 5px;
`

export const Title = styled.p`
	font-weight: 500;
	font-size: 18px;
	letter-spacing: -0.408px;
	color: #000000;
	margin-right: auto;
`
export const MainTitle = styled.p`
	font-weight: 500;
	font-size: 18px;
	letter-spacing: -0.408px;
	color: #000000;
	margin-right: auto;
	text-align: center;
	width: 100%;
`

export const Section = styled.section`
	display: flex;
	width: min(1500px, 95%);
	flex-wrap: wrap;
	justify-content: center;
	gap: 30px;
`
export const StatisticsContainer = styled.div`
	width: 100%;
	display: flex;
	flex-wrap: wrap;
	justify-content: flex-start;
	gap: 24px;
`

export const Left = styled.div`
	flex: 1 389px;
	height: 371.82px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	background: #ffffff;
	border: 1px solid #ffffff;
	border-radius: 14px;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
`

export const Quantity = styled.p`
	font-weight: 500;
	color: #232d42;
	font-size: ${({ fontSize }) => fontSize};
`

export const LeftTop = styled.div`
	padding: 21px 21px 0 21px;
	width: 100%;
	display: flex;
	justify-content: space-between;
`

export const LeftMid = styled.div`
	text-align: center;
`

export const Mid = styled.div`
	flex: 1 389px;
	display: flex;
	flex-wrap: wrap;
	gap: 10px;
`

export const Box = styled.div`
	flex: 1 160px;
	padding: 12px 0px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	gap: 15px;
	background: #ffffff;
	border: 1px solid #ffffff;
	border-radius: 14px;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
`

export const Right = styled.div`
	flex: 1 389px;
	height: 371.82px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
`

export const RightBox = styled.div`
	width: 100%;
	height: 179px;
	display: flex;
	justify-content: center;
	align-items: center;
	gap: 20px;
	background: #ffffff;
	border: 1px solid #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
`

export const QuantityAndSubTitle = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	gap: 10px;
`

export const StadicstImg = styled.img`
	width: ${({ width }) => width};
`

export const TextStatistics = styled.p`
	color: #8a92a6;
	font-style: normal;
	font-weight: 400;
	font-size: 16px;
`

export const Text = styled.p`
	font-weight: 500;
	font-size: 15px;
	color: #828282;
	letter-spacing: -0.408px;
	font-weight: normal;
	font-size: 17px;
`

export const Index = styled.span`
	color: #476ade;
	font-weight: 500;
	font-size: 15px;
	letter-spacing: -0.408px;
`

export const Row = styled.div`
	min-height: 30px;
	display: flex;
	justify-content: space-between;
	padding-left: 10px;
	padding-right: 10px;
	align-items: center;
	flex: ${({ width }) => `1 ${width}`};
`

export const ArticleContainer = styled.div`
	flex: 1 370px;
	display: flex;
	flex-direction: column;
	gap: 26px;
`

export const Article = styled.div`
	background: #ffffff;
	border: 1px solid #ffffff;
	border-radius: 14px;
	padding: 12px 18px;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
`

export const SearchContainer = styled.div`
	max-width: 400px;
	min-width: 0;
	display: flex;
	flex-grow: 1;
	flex-direction: column;
	gap: 26px;
	margin-right: auto;
`

export const UserContainer =styled.div`
	width: 100%;
	display: flex;
	flex-grow: 1;
	flex-direction: column;
	gap: 26px;
	margin-right: auto;
`

export const Results = styled.div`
	width: 100%;
	max-height: 250px;
	min-height: 0;
	overflow-y: scroll;
	padding: 0px 5px 0 5px;
	overflow-x: hidden;

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 5px;
	}

	&::-webkit-scrollbar:horizontal {
		height: 10px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 20px;
	}
	&::-webkit-scrollbar-track {
	}
`

export const BlockReport = styled.div`
	display: flex;
	gap: 10px;
`

export const Img = styled.img`
	width: 15px;
	height: 15px;
	cursor: pointer;
`

export const RestrictedContainer = styled.div`
	display: flex;
	flex-direction: column;
	flex-grow: 1;
	gap: 26px;
	height: 400px;
	max-width: 600px;
`

export const Restricted = styled.div`
	max-height: 400px;
	min-height: 0;
	display: flex;
	flex-wrap: wrap;
	padding: 12px 18px;
	background: #ffffff;
	border: 1px solid #ffffff;
	border-radius: 14px;
	overflow-y: scroll;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	overflow-x: hidden;

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 5px;
	}

	&::-webkit-scrollbar:horizontal {
		height: 10px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 20px;
	}
`

export const SliderContainer = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	gap: 26px;
`

export const Slider = styled.section`
	width: 100%;
	display: flex;
	flex-direction: column;
	gap: 30px;
	align-self: flex-start;
`

export const SliderHeader = styled.header`
	display: flex;
	gap: 20px;
	margin-right: auto;
`
export const SliderImgAndBtn = styled.div`
	display: flex;
	flex-wrap: wrap;
	gap: 20px;
	align-items: center;

	@media screen and (max-width: 699px) {
		gap: 0px;
	}
`
export const SliderImg = styled.img`
	width: min(500px, 100%);
	height: 150px;
`

export const Button = styled.button`
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 6px 15px;
	height: 30px;
	background: #ffffff;
	border: 1px solid #476ade;
	border-radius: 14px;
	color: #476ade;
	font-weight: normal;
	font-size: 15px;
`

export const AddCategoryContainer = styled.div`
	display: flex;
	flex-direction: column;
	gap: 26px;
	margin-right: auto;
	width: min(389px, 98%);
`

export const AddCategoryHeader = styled.header`
	width: 100%;
	display: flex;
	gap: 10px;
	align-items: center;
	justify-content: center;
`

export const ButtonAdd = styled.button`
	width: 50px;
	height: 20px;
	display: flex;
	justify-content: center;
	align-items: center;
`

export const Send = styled(AiOutlineSend)`
	width: 20px;
	height: 20px;
	cursor: pointer;
`

export const CategoriesContainer = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	gap: 26px;
	background: #ffffff;
	border: 1px solid #ffffff;
	border-radius: 14px;
	padding: 23px 18px 23px 18px;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
`

export const Categories = styled.ul`
	width: 100%;
	max-height: 200px;
	display: flex;
	flex-direction: column;
	gap: 5px;
	overflow-y: auto;
	&::-webkit-scrollbar {
		appearance: none;
		-webkit-appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 6px;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 10px;
	}
`

export const ButtonDelete = styled(TiDeleteOutline)`
	width: 18px;
	height: 18px;
	fill: #db2c2c;
	cursor: pointer;
	margin-right: 5px;
`

export const Category = styled.li`
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: space-between;
	list-style: none;
`

export const InputAddCategory = styled.input`
	display: flex;
	width: 100%;
	height: 36px;
	background: rgba(250, 250, 250, 0.93);
	padding-left: 10px;
	border: none;
	outline: none;
	font-size: 16px;
	border: 1px solid #828282;
	border-radius: 10px;
`
export const InputFile = styled.input`
	width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
	position: absolute;
	z-index: -1;
	font-size: 1.25em;
    font-weight: 700;
    color: white;
    background-color: black;
    display: inline-block;
		&:focus{
			background-color: red;
		}

`
export const LabelFile =styled.label`
  display: flex;
	justify-content: center;
	align-items: center;
	padding: 4px 15px;
	height: 30px;
	background: #ffffff;
	border: 1px solid #476ade;
	border-radius: 14px;
	color: #476ade;
	font-weight: normal;
	font-size: 15px;
    display: inline-block;
		cursor: pointer;
		&:hover {
			background-color: #476ade;
			color: white;
		}

`