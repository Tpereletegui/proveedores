import {
  Container,
  Section,
  Title,
  MainTitle,
  ArticleContainer,
  Article,
  Text,
  Index,
  Results,
  RestrictedContainer,
  Restricted,
  Row,
  BlockReport,
  Img,
  Slider,
  SliderHeader,
  Button,
  SearchContainer,
  SliderImgAndBtn,
  SliderImg,
  SliderContainer,
  StadicstImg,
  TextStatistics,
  StatisticsContainer,
  Left,
  Mid,
  Right,
  LeftTop,
  LeftMid,
  Quantity,
  Box,
  RightBox,
  QuantityAndSubTitle,
  Growth,
  ArrowUpward,
  GrowthText,
  AddCategoryContainer,
  AddCategoryHeader,
  Send,
  CategoriesContainer,
  Categories,
  ButtonDelete,
  Category,
  InputAddCategory,
  UserContainer,
	InputFile,
	LabelFile
} from "./styles";

import Layout from "../../components/common/Layout";
import Search from "../../components/common/Navbar/local/Search";
import Users from "../../components/Admin/users";
import { useState, useEffect, useMemo } from "react";
import axios from "axios";

const AdminSection = () => {
  const [categories, setCategories] = useState([]);
  const [category, setCategory] = useState("");
  const [reload, setReload] = useState(false);

  const createCategory = () => {
    if (category === "") return;
    axios({
      method: "post",
      url: "http://localhost:3000/api/category",
      data: {
        name: category,
      },
    });

    setReload(!reload);
    setCategory("");
  };

  const deleteCategory = (index) => {
    axios.delete(
      `http://localhost:3000/api/category?_id=${categories[index]._id}`
    );
    setReload(!reload);
  };

  useEffect(
    function () {
      async function getCategories() {
        const response = await axios.get("http://localhost:3000/api/category");
        setCategories(response.data);
      }

      getCategories();
    },
    [reload]
  );

  const categoriasDestacadas = useMemo(() => [
    "Tecnologia",
    "Electrodomésticos",
    "Moda",
    "Construcción",
    "Salud y Equipamiento Médico",
  ]);
  const resultadosMasBuscados = useMemo(() => [
    "iPad Pro 10.9",
    "Celular Samsung S21",
    "Tesla Model Y",
    "Aire acondicionado LG doble inverter",
    "Bicicleta Raleigh rodado 26",
  ]);

  const usuariosInteracciones = useMemo(() => [
    "Ignacio Prados",
    "Angulo Israel",
    "Marcos Navarro",
    "Agustin Prado",
    "Julia Diaz",
  ]);

  let ImageSliders = useMemo(() => ["/img/slider.png", "/img/slider.png"]);
  const [images, setimages] = useState(["/img/slider.png", "/img/slider.png"]);
  console.log("image", ImageSliders);
  const restringidos = [];

  const [search, setSearch] = useState("");
	const [add, setAdd] = useState(false);


	function deleteImage(image){
		let filter = images.filter( x => x !== image);
		setimages([...filter])
	}
  

  function uploadImage(files) {
    console.log("files", files[0]);
    const formData = new FormData();
    formData.append("file", files[0]);
    formData.append("upload_preset", "azihkt5g");
    axios
      .post(
        "https://api.cloudinary.com/v1_1/proveedores/image/upload",
        formData
      )
      .then((response) => {
        setimages([...images, response.data.url]);
				setAdd(false)
      });
  }

  console.log("images, ", images);

  return (
    <Layout heightContainer={"auto"}>
      <Container>
        <Section>
          <MainTitle>Estadísticas de la web</MainTitle>
          <StatisticsContainer>
            <Left>
              <LeftTop>
                <StadicstImg src="/img/user.png"></StadicstImg>
                <TextStatistics>Nuevos usuarios</TextStatistics>
              </LeftTop>
              <LeftMid>
                <Quantity fontSize={"33px"}>5.326</Quantity>
                <Growth>
                  <GrowthText>
                    <ArrowUpward className="material-icons">
                      arrow_upward
                    </ArrowUpward>
                    30%
                  </GrowthText>{" "}
                  de crecimiento
                </Growth>
              </LeftMid>
              <StadicstImg width={"100%"} src="/img/wave1.png"></StadicstImg>
            </Left>
            <Mid>
              <Box>
                <StadicstImg
                  width={"75px"}
                  src="/img/interactions.png"
                ></StadicstImg>
                <Quantity fontSize={"25px"}>3.452</Quantity>
                <TextStatistics>Nuevas interacciones</TextStatistics>
              </Box>
              <Box>
                <StadicstImg width={"75px"} src="/img/posts.png"></StadicstImg>
                <Quantity fontSize={"25px"}>10.375</Quantity>
                <TextStatistics>Nuevas publicaciones</TextStatistics>
              </Box>
              <Box>
                <StadicstImg width={"75px"} src="/img/visits.png"></StadicstImg>
                <Quantity fontSize={"25px"}>6.863</Quantity>
                <TextStatistics>Visitas</TextStatistics>
              </Box>
              <Box>
                <StadicstImg width={"75px"} src="/img/money.png"></StadicstImg>
                <Quantity fontSize={"25px"}>$ 3.485 us</Quantity>
                <TextStatistics>Ganancias</TextStatistics>
              </Box>
            </Mid>
            <Right>
              <RightBox>
                <QuantityAndSubTitle>
                  <Quantity fontSize={"25px"}>10.375</Quantity>
                  <TextStatistics>Usuarios</TextStatistics>
                </QuantityAndSubTitle>
                <StadicstImg src="/img/wave1.png" width={"60%"}></StadicstImg>
              </RightBox>
              <RightBox>
                <QuantityAndSubTitle>
                  <Quantity fontSize={"25px"}>106.3k</Quantity>
                  <TextStatistics>Interacciones</TextStatistics>
                </QuantityAndSubTitle>
                <StadicstImg src="/img/wave1.png" width={"60%"}></StadicstImg>
              </RightBox>
            </Right>
          </StatisticsContainer>
        </Section>
        <Section>
          <StatisticsContainer>
            <ArticleContainer>
              <Title>Categorías más destacadas</Title>
              <Article>
                {categoriasDestacadas.map((categoria, index) => (
                  <Text key={index}>
                    <Index>{index + 1}. </Index>
                    {categoria}
                  </Text>
                ))}
              </Article>
            </ArticleContainer>

            <ArticleContainer>
              <Title>Resultados más buscados</Title>
              <Article>
                {resultadosMasBuscados.map((resultados, index) => (
                  <Text key={index}>
                    <Index>{index + 1}. </Index>
                    {resultados}
                  </Text>
                ))}
              </Article>
            </ArticleContainer>

            <ArticleContainer>
              <Title>Usuarios con más interacciones</Title>
              <Article>
                {usuariosInteracciones.map((interaccion, index) => (
                  <Text key={index}>
                    <Index>{index + 1}. </Index>
                    {interaccion}
                  </Text>
                ))}
              </Article>
            </ArticleContainer>
          </StatisticsContainer>
        </Section>

        <Section>
          <AddCategoryContainer>
            <Title>Agregar categoria</Title>

            <CategoriesContainer>
              <AddCategoryHeader>
                <InputAddCategory
                  placeholder="Ingrese categoria"
                  value={category}
                  onChange={(e) => setCategory(e.target.value)}
                  type="text"
                ></InputAddCategory>

                <Send onClick={() => createCategory()}></Send>
              </AddCategoryHeader>
              <Categories>
                {categories.map((category, index) => (
                  <Category>
                    {category.name}
                    <ButtonDelete onClick={() => deleteCategory(index)}>
                      Delete
                    </ButtonDelete>
                  </Category>
                ))}
              </Categories>
            </CategoriesContainer>
          </AddCategoryContainer>
        </Section>

        <Section>
          {/* <SearchContainer style={{marginRight: '5%'}}>
						<Title>Restringir usuarios </Title>
						<Article>
							<Search width={'100%'} setSearch={setSearch} />
							<Results>
								{search &&
									usuariosInteracciones
										.filter(user => user.includes(search))
										.map(res => (
											<Row width={'100%'}>
												<Text>{res}</Text>
												<BlockReport>
													<Img src='/img/warning.png'></Img>
													<Img src='/img/block.png'></Img>
												</BlockReport>
											</Row>
										))}
							</Results>
						</Article>
					</SearchContainer>
					{restringidos.length > 0 && (
						<RestrictedContainer>
							<Title> Usuarios restringidos </Title>
							<Restricted>
								{restringidos.map(user => (
									<Row width={'50%'}>
										<Text>{user}</Text>
										<BlockReport>
											<Img src='/img/warning.png'></Img>
											<Img src='/img/block.png'></Img>
										</BlockReport>
									</Row>
								))}
							</Restricted>
						</RestrictedContainer>
					)} */}
          <UserContainer>
            <Title>Usuarios</Title>
            <Users />
          </UserContainer>
        </Section>

        <Section>
          <SliderContainer>
            <SliderHeader>
              <Title>Slider en el Inicio</Title>
              {/* <Button onClick={()=>setAdd(!add)}></Button> */}
							<InputFile onChange={(e)=>uploadImage(e.target.files)} name='file' id='file' type={'file'} />
							<LabelFile for='file'>Agregar nueva imagen</LabelFile>
            </SliderHeader>
            <Slider>
              {images &&
                images.map((image, index) => (
                  <SliderImgAndBtn key={index}>
                    <SliderImg src={image}></SliderImg>
                    <Button onClick={()=>deleteImage(image)}>Eliminar Imagen</Button>
                  </SliderImgAndBtn>
                ))}
              
            </Slider>
          </SliderContainer>
        </Section>
      </Container>
    </Layout>
  );
};

export default AdminSection;
