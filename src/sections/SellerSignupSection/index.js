import React, { useMemo } from 'react'
import Form from '../../components/Login/SellerForm'
import Info from '../../components/Login/Info'
import Switch from '../../components/Login/Switch'
import {
	Container,
	FormContainer,
	Slides,
	SlideButtons,
	SlideBtn,
	Square,
	LogoContainer,
	Text,
} from './styles'

import Slide from '../../components/Login/Slide'


const SignupSection = () => {
	const slides = useMemo(() => [
		{
			title: 'Compra y Vende Gratis',
			subtitle: 'Solo con un par de clicks',
			imgUrl: '/img/slide1.png',
		},
		{
			title: 'Contribuye con la comunidad',
			subtitle: 'Encontrá personas que venden productos',
			imgUrl: '/img/slide2.png',
		},
		{
			title: 'Compra y Vende Gratis',
			subtitle: 'Solo con un par de clicks',
			imgUrl: '/img/slide1.png',
		},
	], [])
	const [slideIndex, setSlideIndex] = React.useState(0)
	return (
		<Container>
			<Slides>
				<LogoContainer>
					<Square></Square>
					<Text>MILUX</Text>
				</LogoContainer>
				{slides.map((slide, index) => {
					const { title, subtitle, imgUrl } = slide
					if (index === slideIndex) {
						return (
							<Slide
								key={index}
								title={title}
								subtitle={subtitle}
								imgUrl={imgUrl}
							/>
						)
					} else {
						return ''
					}
				})}
				<SlideButtons>
					{slides.map((slide, index) => {
						return (
							<SlideBtn
								style={
									index === slideIndex
										? { background: '#FFF' }
										: {}
								}
								key={index}
								onClick={() => {
									setSlideIndex(index)
								}}
							></SlideBtn>
						)
					})}
				</SlideButtons>
			</Slides>
			<FormContainer>
				<Info
					title='Empieza ahora'
					subtitle='Crea tu cuenta para ingresar'
				/>
				<Form mode='signup' />
				<Switch mode='signup' />
			</FormContainer>
		</Container>
	)
}

export default SignupSection
