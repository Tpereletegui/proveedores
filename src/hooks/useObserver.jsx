import { useEffect, useRef, useState } from "react"

const useOberserver = options => {
	const [elements, setElements] = useState([]) //Elementos a ser observados
	const [entries, setEntries] = useState([])

	const observer = useRef(
		new IntersectionObserver(observedEntries => {
			setEntries(observedEntries)
			console.log(observedEntries)
		}, options)
	)
	/*IntersectionObserver es una api de navegador. Cada vez que ejecutamos este hook siempre obtendremos el mismo valor, para eso es useRef*/
	/*Cada vez que se produzca un cambio, se ejecuta la funcion de callback y esa misma funcion devuelve entries, las cuales las vamos a almacenar en un estado*/

	useEffect(() => {
		const currenObserver = observer.current
		currenObserver.disconnect() //Dejo de observar lo viejo
		if (elements.length > 0) {
			elements.forEach(element => currenObserver.observe(element))
		}

		return () => {
			//Funcion de cleanUp, se ejecuta antes de ejecutar el siguiente efecto
			if (currenObserver) {
				currenObserver.disconnect()
			}
		}
	}, [elements])

	return [observer.current, setElements, entries]
}
export default useOberserver
