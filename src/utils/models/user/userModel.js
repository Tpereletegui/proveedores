import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  image : {
    type: String,
    default : 'https://www.pngitem.com/pimgs/m/30-307416_profile-icon-png-image-free-download-searchpng-employee.png'
  },

  password: {
    type: String,
    required: true
  },
  interestedCategories:[Object],
  picture: String,
  seller_id: mongoose.ObjectId,
  code: {
    type: String,
    required: false
  }
}, {timestamps: {createdAt: "createdAt", updatedAt: "updatedAt"}});

module.exports = mongoose.models.User || mongoose.model("User", UserSchema, "Users");