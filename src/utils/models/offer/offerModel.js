const mongoose = require("mongoose");

const VariantsSchema = new mongoose.Schema({
  variantType: {
    type: String,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
  },
  brand: {
    type: String,
  },
  color: {
    type: String,
  },
  image: {
    type: String
  },
})

const OfferSchema = new mongoose.Schema(
  {
    type: {
      type: String,
      required: true
    },
    post_id: {
      type: mongoose.ObjectId,
      required: true
    },
    buyer_id: {
      type: mongoose.ObjectId,
      required: true
    },
    seller_id: {
      type: mongoose.ObjectId,
      required: true
    },
    variants: [VariantsSchema],
  },
  { timestamps: true }
);

module.exports = mongoose.models.Offer || mongoose.model("Offer", OfferSchema);
