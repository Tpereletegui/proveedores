import OffersDetailsSection from '../../sections/OffersDetailsSection/OffersDetailsSection';
import { useRouter } from 'next/router';
import Head from 'next/head'
import Layout from '../../components/common/Layout';
export default function Offer() {
    const router = useRouter();
    const { id } = router.query;

    return (<>
        <Head><title>Offer</title></Head>
        <Layout heightContainer={'100vh'}>
            <OffersDetailsSection id={id} />
        </Layout>

    </>)
}
