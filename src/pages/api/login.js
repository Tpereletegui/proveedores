import User from '../../utils/models/user/userModel';
import bcrypt from 'bcrypt';
import connectDB from '../../utils/database';
import jwt from 'jsonwebtoken';


const login = async (req, res) => {
  if (req.method === 'POST') {
    const { email, password } = req.body
    if (!email || !password) return res.status(400).json({ error: 'Missing credentials' });

    // console.log(email);
    const user = await User.findOne({ email: email });
    // console.log("Usuario: ", user);
    if (!user) return res.status(400).json({ error: 'Usuario no encontrado' });
    const validPassword = await bcrypt.compare(password, user.password);
    if (!validPassword) return res.status(400).json({ error: 'contraseña no válida' })

    // create token
    const token = jwt.sign({
      name: user.username,
      id: user._id,
      picture: user?.picture
    }, process.env.TOKEN_SECRET)

    return res.status(200).json({
      token
    })
  }

}

export default connectDB(login);