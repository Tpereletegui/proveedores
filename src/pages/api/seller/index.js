import connectDB from "../../../utils/database";
import Seller from "../../../utils/models/seller/sellerModel";

const seller = async (req, res) => {
  if (req.method === "GET") {
    try {
      const sellers = await Seller.find({}).sort({createdAt: -1});
      res.status(200).send(sellers);
    } catch (err) {
      res.status(400).send(err);
    }
  } else if (req.method === "POST") {
    console.log(req.body);
    const {
      username,
      email,
      cuit,
      fantasyName,
      postalCode,
      city,
      phone,
      province,
      category,
      user_id,
      socialReason,
      address
    } = req.body;
    if (
      username 
      &&
      email &&
      cuit &&
      postalCode &&
      fantasyName &&
      city &&
      phone &&
      province &&
      category &&
      user_id &&
      socialReason &&
      address
      ) {
        let seller = new Seller(req.body);
        
        try {
        const newSeller = await seller.save();
        res.status(200).send(newSeller);
      } catch (err) {
        res.status(400).json({ error: err });
      }
      } else {
        return res.status(400).json({ error: "Bad request" });
      }
  } else if (req.method === "DELETE") {
    const { _id } = req.query;
    Seller.findOneAndDelete({ _id }, function (err, docs) {
      if (err) {
        res.status(404).json({"error": err});
      } else {
        res.status(200).json({ "Deleted Seller": docs });
      }
    });
  } else if (req.method === "PUT") {
    const { _id } = req.query;
    const updateData = req.body;
    try {
      if (_id && updateData) {
        const updated = await Seller.findOneAndUpdate({ _id }, updateData);
        return res.status(200).send(updated);
      }
      return res.send("bad request");
    } catch (err) {
      return res.status(400).send(err.message);
    }
  }
};

export default connectDB(seller);
