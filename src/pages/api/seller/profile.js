import Seller from '../../../utils/models/seller/sellerModel';
import connectDB from '../../../utils/database';
import Post from '../../../utils/models/post/postModel';

const getSellerProfileStats = async (req, res) => {
  const { _id } = req.query;
  let seller = await Seller.findOne({ _id: _id }).lean().exec();
  let posts = await Post.find({ seller_id: _id });
  let data = await seller
  data.postCount = posts.length;
  res.send(data);
}

export default connectDB(getSellerProfileStats);