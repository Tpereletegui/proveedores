import Seller from '../../../utils/models/seller/sellerModel'
import connectDB from '../../../utils/database';

const getSellerById = async (req, res) => {
  const {id} = req.query;
  if(!id) return res.status(400).json({error: "Bad Request"});
  try{
    const seller = await Seller.findById(id);
    res.json({data: seller})
  }catch(err){
    res.status(404).json({error: err.message})
  }
}


export default connectDB(getSellerById);