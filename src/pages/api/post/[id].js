import Post from '../../../utils/models/post/postModel';
import connectDB from '../../../utils/database';

const getPostById = async (req, res) => {
  const {id} = req.query;
  if(!id) return res.status(400).json({error: "Bad Request"});
  try{
    const post = await Post.findById(id);
    res.json({data: post})
  }catch(err){
    res.status(404).json({error: err.message})
  }
}


export default connectDB(getPostById);