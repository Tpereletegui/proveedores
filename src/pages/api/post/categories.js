import connectDB from "../../../utils/database";
import Post from "../../../utils/models/post/postModel.js";

const postByCategory = async (req, res) => {
  const {categories} = req.body; 
  const posts = await Post.find({categories: {$in: categories}});
  console.log();
  res.send(posts);
}


export default connectDB(postByCategory);