import connectDB from "../../../utils/database";
import Post from "../../../utils/models/post/postModel.js";

const post = async (req, res) => {
  const { _id } = req.query;
  switch (req.method) {
    case "GET":
      try {
        const posts = await Post.find({}).sort({createdAt: -1});
        res.status(200).send(posts);
      } catch (err) {
        res.json({ error: err });
      }
      break;
    case "POST":
      const { seller_id, quote, proposal, contact, variants } = req.body;
      try {
        if (seller_id && quote && proposal && contact && variants) {
          let post = new Post(req.body);
          const newPost = await post.save();
          res.status(200).send(newPost);
        } else {
          return res.status(400).json({ error: "Bad request" });
        }
      } catch (err) {
        res.status(400).json({ error: err.message });
      }
      break;
    case "PUT":
      const updateData = req.body;
      try {
        if (_id && updateData) {
          const updated = await Post.findOneAndUpdate({ _id }, updateData);
          return res.status(200).json({ status: "updated" });
        }
        return res.status(400).json({ error: "bad request" });
      } catch (err) {
        return res.status(400).json({ error: err.message });
      }
      break;
    case "DELETE":
      Post.findOneAndDelete({ _id }, function (err, docs) {
        if (err) {
          res.status(404).json({"error": err});
        } else {
          res.status(200).json({ "Deleted Post": docs });
        }
      });
      break;

    default:
      break;
  }
};

export default connectDB(post);
