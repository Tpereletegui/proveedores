import Post from '../../../utils/models/post/postModel';
import connectDB from '../../../utils/database';

const addViewToPost = async (req, res) => {
  const { _id } = req.query;
  if (!_id) return res.status(400).send("Bad request");
  const post = await Post.findOne({ _id });
  let views = !post.views ? 1 : post.views + 1;
  const updated = await Post.findOneAndUpdate({ _id }, { views: views });
  if (!updated) return res.status(500).send("bad request");
  return res.send("Updated"); 
}

export default connectDB(addViewToPost);
