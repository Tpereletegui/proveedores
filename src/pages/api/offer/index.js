import connectDB from "../../../utils/database";
import Offer from "../../../utils/models/offer/offerModel";

const offer = async (req, res) => {
  const { _id } = req.query;
  switch (req.method) {
    case "GET":
      if(_id){
        try{
          const offers = await Offer.findById(_id);
          res.status(200).send(offers);
        }catch(err){
          res.json({ error: err });
        }
      }else{
        try {
          const offers = await Offer.find({}).sort({createdAt: -1});
          res.status(200).send(offers);
        } catch (err) {
          res.json({ error: err });
        }
      }
      break;
    case "POST":
      const { post_id, seller_id, buyer_id, variants, type } = req.body;
      console.log(req.body)
      try {
        if (post_id && seller_id && buyer_id && variants.length && type) {
          let o = new Offer(req.body);
          const newOffer = await o.save();
          res.status(200).send(newOffer);
        } else {
          return res.status(400).json({ error: req.body });
        }
      } catch (err) {
        res.status(400).json({ error: err.message });
      }
      break;
    case "DELETE":
      Offer.findOneAndDelete({ _id }, function (err, docs) {
        if (err) {
          res.status(404).json({"error": err});
        } else {
          res.status(200).json({ "Deleted Post": docs });
        }
      });
      break;

    default:
      break;
  }
};

export default connectDB(offer);
