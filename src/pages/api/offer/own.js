import connectDB from "../../../utils/database";
import Offer from "../../../utils/models/offer/offerModel";

const bySellerId = async (req, res) => {
  const { id } = req.query;
  switch (req.method) {
    case "GET":
      try{
        const offers = await Offer.find({ seller_id: id }).sort({createdAt: -1});
        res.status(200).send(offers);
      }catch(err){
        res.json({error: err})
      }
    break;
    
    default:
    break;
  }
}

export default connectDB(bySellerId);