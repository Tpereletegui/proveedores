import { NextApiRequest, NextApiResponse } from "next";
import User from '../../../utils/models/user/userModel'
import connectDB from "../../../utils/database";

const getUserById = async (req, res) => {
  const { id } = req.query;
  if (!id) return res.status(400).json({ error: "Bad Request" });

  try {
    const post = await User.findById(id);
    res.json({ data: post })
  } catch (err) {
    res.status(404).json({ error: err.message })
  }
}
export default connectDB(getUserById);