import connectDB from '../../../utils/database';
import User from '../../../utils/models/user/userModel';
import bcrypt from 'bcrypt';

const user = async (req, res) => {
  
  switch(req.method){
    
    case "GET":
      const users = await User.find({});
      return res.status(200).json(users);

    case "POST": 
      const { username, email, password, phone, type } = req.body;
      if (username && email && password && phone && type) {
        let usersLength;
        try {
          usersLength = await User.find({});
        }catch(err){
          return res.status(400).json(err)
        }
        let code = type[0] + usersLength.length;
        for(let i = 0; i < 6 - String(usersLength.length).length; i++){
          code += '0';
        }
        try {
          const passwordhash = await bcrypt.hash(password, 10);
          let user = new User({
            username,
            phone,
            email,
            password: passwordhash,
            code
          });
          let usercreated = await user.save();
          return res.status(200).json(usercreated);

        } catch (error) {
          return res.status(400).json(error);
        }
      } else {
        return res.status(400).send("Bad request")
      }
      
    case "DELETE":
      const { _id } = req.query;
      User.findOneAndDelete({ _id }, function (err, docs) {
        if (err) {
          return res.status(404).json({err});
        }
        else {
          return res.status(200).json(docs);
        }
      })

    case "PUT":
      const { id } = req.query
      console.log(req.body)
      try {
        console.log(req.body);
        if (id) {
          const user = await User.findOne({ _id: id })
          user.interestedCategories=req.body.interestedCategories
          user.markModified("interestedCategories")
          await user.save()
          return res.status(200).json(user);
        } else {
          return res.send(id);
        }
      } catch (err) {
        return res.status(400).json(err)
      }

    default:
      return res.status(404).json({error: "REQ METHOD NOT VALID."})
  
  }

}

export default connectDB(user);