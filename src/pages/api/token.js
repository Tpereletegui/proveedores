import jwt from 'jsonwebtoken';
import connectDB from '../../utils/database';


const decodeToken = async (req, res) => {
  const {token} = req.body;
  const decoded = await jwt.verify(token, process.env.TOKEN_SECRET)
  res.send(decoded);
}

export default decodeToken;