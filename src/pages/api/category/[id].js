import connectDB from '../../../utils/database';
import Category from "../../../utils/models/categoryModel";

const getCategoryById = async (req, res) => {
  const {id} = req.query;
  if(!id) return res.status(400).json({error: "Bad Request"});
  try{
    const category = await Category.findById(id);
    res.json({data: category})
  }catch(err){
    res.status(404).json({error: err.message})
  }
}


export default connectDB(getCategoryById);