import '../styles/globals.css'
import { SessionProvider } from 'next-auth/react'
import CategoriesContextProvider from '../context/categoriesContext'
import { AuthProvider } from '../context/AuthContext'

function MyApp({ Component, pageProps: {  ...pageProps } }) {
	return (
	
			<CategoriesContextProvider>
			
					<Component {...pageProps} />
			
			</CategoriesContextProvider>
	
	)
}

export default MyApp