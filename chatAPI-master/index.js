const express = require('express');
const app = express();

const server = require("http").createServer(app);
const cors = require("cors");
const bodyParser = require("body-parser");
// const jwt = require("_helpers/jwt");
// const errorHandler = require("_helpers/error-handler");
// import socket events code
const Sockets = require("./sockets");
// import socket io
const io = require("socket.io");
const { deleteChats, getChats } = require('./chats/chat.service');

//Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
// app.use(jwt());

// api routes
// app.use("/api/users", require("./users/users.controller"));

// global error handler
// app.use(errorHandler);
app.get('/chats/delete', deleteChats)

// start server
const port = 3001;

const httpServer = server.listen(port, function () {
  console.log("Server running on port " + port);
});

// create a new instance of a socket server using the http server
const io_server = io(httpServer, { cors: { origin: "*" } });

// pass the socket server instance to the imported socket function with the events and middlewares
Sockets(io_server);
