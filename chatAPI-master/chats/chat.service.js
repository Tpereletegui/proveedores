const db = require("../_helpers/db");
const Chat = db.Chat;
const User = db.User;
const Message = db.Message;

module.exports = {
  newChat,
  getChats,
  getChat,
  deleteChats
};

//Create a new chat

async function newChat(sender, receiver) {
  console.log(receiver, sender)
  // check if it already exists
  const exists = await Chat.findOne({
    members: { $all: [sender, receiver] },
  });
  // if it exists returns an object with the existing chat and a property describing its not a new chat
  if (exists) return { new: false, chat: exists };
  // if it doesnt exists creates a new chat and returns an object with the newly created chat and
  // a property describing its a new chat
  const chat = new Chat({ members: [sender, receiver] });
  const savedChat = await chat.save();
  return { new: true, chat: savedChat };
}

// Get all user chats

async function getChats(userId) {
  const allChats = await Chat.find({ members: { $in: [userId] } })
    .select("members updatedAt")
    .populate({ path: "members", model: User, select: "firstName lastName" })
    .populate({
      path: "messages",
      model: Message,
      select: "sender text createdAt -_id",
      options: { limit: 100, sort: { createdAt: -1 } },
    })
    .lean();
  return allChats;
}

// Get chat with specific user

async function getChat(userId, secondUserId) {
  const chat = await Chat.findOne({
    members: { $all: [userId, secondUserId] },
  })
    .select("members updatedAt")
    .populate({ path: "members", model: User, select: "firstName lastName" })
    .populate({
      path: "messages",
      model: Message,
      select: "sender text createdAt -_id",
      options: { limit: 100, sort: { createdAt: -1 } },
    })
    .lean();
  return chat;
}

async function deleteChats(){
  const chats = await Chat.deleteMany({});
  return chats;
}
