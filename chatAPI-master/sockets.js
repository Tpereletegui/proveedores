const chatService = require("./chats/chat.service");
const messageService = require("./messages/message.service");
const sessionStore = require("./sessionStorage");
const jwt = require("jsonwebtoken");
// const config = require("config.json");
const crypto = require("crypto");
const randomId = () => crypto.randomBytes(8).toString("hex");

const Sockets = (io) => {
  
  // middleware jwt
  io.use((socket, next) => {
    // check if a sessionID comes with the connection
    const sessionID = socket.handshake.auth.sessionID;

    if (sessionID) {
      // find existing session
      const session = sessionStore.findSession(sessionID);
      if (session) {
        socket.sessionID = sessionID;
        socket.userID = session.userID;
        return next();
      }
    }
    // check if the jwt token comes with the connection
    const token = socket.handshake.auth.token;

    // get the user id from the decoded jwt
    // const user_id = jwt.verify(token, config.secret, (err, decoded) => {
    //   if (err) return null;
    //   return decoded.sub;
    // });

    const user_id = jwt.decode(token)

    // refuse the connection if there was no jwt
    if (!user_id) {
      return next(new Error("Invalid user"));
    }

    // create new session
    socket.sessionID = randomId();
    socket.userID = user_id;
    next();
  });

  // from here starts the connection
  

  io.on("connection", async (socket) => {
    console.log("An user has connected");
    // persist session
    sessionStore.saveSession(socket.sessionID, {
      userID: socket.userID,
      connected: true,
      socketID: socket.id,
    });

    // emit session details
    socket.emit("session", {
      sessionID: socket.sessionID,
      userID: socket.userID,
    });

    // join the "userID" room
    socket.join(socket.userID);

    // fetch existing users
    const users = [];

    sessionStore.findAllSessions().forEach((session) => {
      users.push({
        userID: session.userID,
        connected: session.connected,
        socketID: session.socketID,
      });
    });

    const user = {id: socket.userID.id, name: socket.userID.name}
    // fetch chats of the user
    const chats = await chatService.getChats(user);

    // emit the list of online/offline sessions
    socket.emit("users", users);

    // emit the list of chats for the connected socket
    socket.emit("chats", chats);

    // notify existing users a user has connected
    socket.broadcast.emit("user connected", {
      userID: socket.userID,
      connected: true,
      socketID: socket.id,
    });

    // receives a private message from front, creates a new chat and message in db if needed, and then
    // notifies the users with the corresponding message (or chat if it had to be created,
    // with a different event)
    socket.on("private message", async ({ receiver, text }) => {
      console.log(receiver)
      // search for an existing chat
      const foundChat = await chatService.newChat(user, receiver);

      if (foundChat.chat) {
        // if chat exists creates a new message
        const newMessage = await messageService.newMessage(
          foundChat.chat._id,
          user.id,
          text
        );

        if (newMessage) {
          // if the message is created with successfully check if the receiver session is online

          const isOnline = sessionStore
            .findAllSessions()
            .filter((item) => (item.userID.id === receiver.id && item.connected));

          // add the new message to the chat and save
          foundChat.chat.messages = [
            ...foundChat.chat.messages,
            newMessage._id,
          ];
          await foundChat.chat.save();

          // if receiver session exists and is online check if the chat
          // existed before or was created with this message
          console.log(isOnline)
          if (isOnline.length > 0) {
            // console.log("Its online!!", isOnline[0]);

            // if chat was created for this message (first message in a conversation)
            if (foundChat.new) {
              //find the updated chat with the correct info (populated/selected fields) to emit
              const updatedChat = await chatService.getChat(
                user,
                receiver
              );
              console.log(updatedChat)

              // emit the new chat event with the updated chat to receiver and also the sender
              // supposedly should be able to emit both events at once, but its not working for me
              // so i send to sender and receiver separately
              socket
                .to(isOnline[0].socketID)
                // .to(socket.id)
                .emit("new chat", updatedChat);
            } else {
              // if chat already existed before this message
              // filter only the fields that are needed to be sent of the message
              const auxMsg = {
                createdAt: newMessage.createdAt,
                sender: newMessage.sender,
                text: newMessage.text,
                chatId: newMessage.chatId,
              };
              // emit the filtered message to receiver and sender (separately for the reason explained above)
              socket
                .to(isOnline[0].socketID)
                // .to(socket.id)
                .emit("private message", auxMsg);
              socket.emit("private message", auxMsg);
            }
          } else {
            console.log("Not online");
            // same as above but since the receiver isnt online we only emit events to the sender
            if (foundChat.new) {
              //find the updated chat with the correct info (populated/selected fields) to emit
              const updatedChat = await chatService.getChat(
                user,
                receiver
              );

              socket.emit("new chat", updatedChat);
            } else {
              const auxMsg = {
                createdAt: newMessage.createdAt,
                sender: newMessage.sender,
                text: newMessage.text,
                chatId: newMessage.chatId,
              };
              socket.emit("private message", auxMsg);
            }
          }
        }
      } else return new Error("An error has occurred");
    });

    // notify users upon disconnection
    socket.on("disconnect", async () => {
      console.log("An user has disconnected");
      // search for the socket in the list of all connected sockets
      const matchingSockets = await io.in(socket.userID).fetchSockets();
      // if array is empty means its not on the list, so its disconnected
      const isDisconnected = matchingSockets.length === 0;
      if (isDisconnected) {
        // notify other users
        socket.broadcast.emit("user disconnected", socket.userID);
        // update the connection status of the session
        sessionStore.saveSession(socket.sessionID, {
          userID: socket.userID,
          connected: false,
        });
      }
    });
  });
};

module.exports = Sockets;
